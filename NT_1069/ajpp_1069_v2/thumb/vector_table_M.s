/**************************************************
 *
 * Part one of the system initialization code, contains low-level
 * initialization, plain thumb variant.
 *
 * Copyright 2008 IAR Systems. All rights reserved.
 *
 * $Revision: 52685 $
 *
 **************************************************/

;
; The modules in this file are included in the libraries, and may be replaced
; by any user-defined modules that define the PUBLIC symbol __iar_program_start or
; a user defined start symbol.
; To override the cstartup defined in the library, simply add your modified
; version to the workbench project.
;
; The vector table is normally located at address 0.
; When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
; The name "__vector_table" has special meaning for C-SPY, which
; is where to find the SP start value.
; If vector table is not located at address 0, the user has to initialize the  NVIC vector
; table register (VTOR) before using interrupts.
;
; Cortex-M version
;

        MODULE  ?vector_table

        AAPCS INTERWORK, VFP_COMPATIBLE, RWPI_COMPATIBLE
        PRESERVE8


        ;; Forward declaration of sections.
        SECTION CSTACK:DATA:NOROOT(3)

        SECTION .intvec:CODE:NOROOT(2)

        EXTERN  __iar_program_start
        PUBLIC  __vector_table

        DATA

__iar_init$$done:               ; The vector table is not needed
                                ; until after copy initialization is done

__vector_table
        DCD     sfe(CSTACK)
        DCD     __iar_program_start

        DCD     NMI_Handler
        DCD     HardFault_Handler
        DCD     MemManage_Handler
        DCD     BusFault_Handler
        DCD     UsageFault_Handler
        DCD     0
        DCD     0
        DCD     0
        DCD     0
        DCD     SVC_Handler
        DCD     DebugMon_Handler
        DCD     0
        DCD     PendSV_Handler
        DCD     SysTick_Handler
         ; External Interrupts
        DCD     DMA1_Stream0_IRQHandler           ; DMA1 Stream 0                                   
        DCD     DMA1_Stream1_IRQHandler           ; DMA1 Stream 1         
        DCD     TIM1_IRQHandler                   ; TIM1
        DCD     TIM2_IRQHandler                   ; TIM2
        DCD     USART1_IRQHandler                 ; USART1                                          
        DCD     USART2_IRQHandler                 ; USART2
        DCD     SPI1_IRQHandler                   ; SPI1                                            
        DCD     USART3_IRQHandler                 ; USART2SPI2_IRQHandler                   ; SPI2
        
        DCD     EXTI0_IRQHandler                  ; EXTI Line0                                             
        DCD     EXTI1_IRQHandler                  ; EXTI Line1                                             
        DCD     EXTI2_IRQHandler                  ; EXTI Line2                                             
        DCD     EXTI3_IRQHandler                  ; EXTI Line3                                             
        DCD     EXTI4_IRQHandler                  ; EXTI Line4
        DCD     EXTI5_IRQHandler                  ; EXTI Line2                                             
        DCD     EXTI6_IRQHandler                  ; EXTI Line3                                             
        DCD     EXTI7_IRQHandler                  ; EXTI Line4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Default interrupt handlers.
;;

;        PUBWEAK NMI_Handler
;        PUBWEAK HardFault_Handler
;        PUBWEAK MemManage_Handler
;        PUBWEAK BusFault_Handler
;        PUBWEAK UsageFault_Handler
;        PUBWEAK SVC_Handler
;        PUBWEAK DebugMon_Handler
;        PUBWEAK PendSV_Handler
;        PUBWEAK SysTick_Handler
;        PUBWEAK DMA1_Stream0_IRQHandler
;        PUBWEAK DMA1_Stream1_IRQHandler
;        PUBWEAK TIM1_IRQHandler
;        PUBWEAK TIM2_IRQHandler
;        PUBWEAK USART1_IRQHandler
;        PUBWEAK USART2_IRQHandler
;        PUBWEAK SPI1_IRQHandler
;        PUBWEAK SPI2_IRQHandler
;        PUBWEAK EXTI0_IRQHandler
;        PUBWEAK EXTI1_IRQHandler
;        PUBWEAK EXTI2_IRQHandler
;        PUBWEAK EXTI3_IRQHandler
;        PUBWEAK EXTI4_IRQHandler
;        PUBWEAK EXTI5_IRQHandler
;        PUBWEAK EXTI6_IRQHandler
;        PUBWEAK EXTI7_IRQHandler

        
        SECTION .text:CODE:REORDER(1)
        THUMB

        PUBWEAK NMI_Handler
        SECTION .text:CODE:REORDER(1)
NMI_Handler
        B NMI_Handler

        PUBWEAK HardFault_Handler
        SECTION .text:CODE:REORDER(1)
HardFault_Handler
        B HardFault_Handler

        PUBWEAK MemManage_Handler
        SECTION .text:CODE:REORDER(1)
MemManage_Handler
        B MemManage_Handler

        PUBWEAK BusFault_Handler
        SECTION .text:CODE:REORDER(1)
BusFault_Handler
        B BusFault_Handler

        PUBWEAK UsageFault_Handler
        SECTION .text:CODE:REORDER(1)
UsageFault_Handler
        B UsageFault_Handler

        PUBWEAK SVC_Handler
        SECTION .text:CODE:REORDER(1)
SVC_Handler
        B SVC_Handler

        PUBWEAK DebugMon_Handler
        SECTION .text:CODE:REORDER(1)
DebugMon_Handler
        B DebugMon_Handler

        PUBWEAK PendSV_Handler
        SECTION .text:CODE:REORDER(1)
PendSV_Handler
        B PendSV_Handler

        PUBWEAK SysTick_Handler
        SECTION .text:CODE:REORDER(1)
SysTick_Handler
        B SysTick_Handler

; External Interrupts
        PUBWEAK DMA1_Stream0_IRQHandler
        SECTION .text:CODE:REORDER(1)    
DMA1_Stream0_IRQHandler  
        B DMA1_Stream0_IRQHandler

        PUBWEAK DMA1_Stream1_IRQHandler
        SECTION .text:CODE:REORDER(1)    
DMA1_Stream1_IRQHandler  
        B DMA1_Stream1_IRQHandler

        PUBWEAK TIM1_IRQHandler
        SECTION .text:CODE:REORDER(1)
TIM1_IRQHandler  
        B TIM1_IRQHandler

        PUBWEAK TIM2_IRQHandler
        SECTION .text:CODE:REORDER(1)
TIM2_IRQHandler  
        B TIM2_IRQHandler

        PUBWEAK USART1_IRQHandler
        SECTION .text:CODE:REORDER(1)
USART1_IRQHandler  
        B USART1_IRQHandler

        PUBWEAK USART2_IRQHandler
        SECTION .text:CODE:REORDER(1)
USART2_IRQHandler  
        B USART2_IRQHandler        
        
        PUBWEAK USART3_IRQHandler
        SECTION .text:CODE:REORDER(1)
USART3_IRQHandler  
        B USART3_IRQHandler        

        PUBWEAK SPI1_IRQHandler
        SECTION .text:CODE:REORDER(1)
SPI1_IRQHandler  
        B SPI1_IRQHandler

        PUBWEAK SPI2_IRQHandler
        SECTION .text:CODE:REORDER(1)
SPI2_IRQHandler  
        B SPI2_IRQHandler

        PUBWEAK EXTI0_IRQHandler
        SECTION .text:CODE:REORDER(1)
EXTI0_IRQHandler  
        B EXTI0_IRQHandler

        PUBWEAK EXTI1_IRQHandler
        SECTION .text:CODE:REORDER(1)
EXTI1_IRQHandler  
        B EXTI1_IRQHandler

        PUBWEAK EXTI2_IRQHandler
        SECTION .text:CODE:REORDER(1)
EXTI2_IRQHandler  
        B EXTI2_IRQHandler

        PUBWEAK EXTI3_IRQHandler
        SECTION .text:CODE:REORDER(1)
EXTI3_IRQHandler
        B EXTI3_IRQHandler

        PUBWEAK EXTI4_IRQHandler
        SECTION .text:CODE:REORDER(1)    
EXTI4_IRQHandler  
        B EXTI4_IRQHandler
        
        PUBWEAK EXTI5_IRQHandler
        SECTION .text:CODE:REORDER(1)
EXTI5_IRQHandler  
        B EXTI5_IRQHandler

        PUBWEAK EXTI6_IRQHandler
        SECTION .text:CODE:REORDER(1)
EXTI6_IRQHandler
        B EXTI6_IRQHandler

        PUBWEAK EXTI7_IRQHandler
        SECTION .text:CODE:REORDER(1)    
EXTI7_IRQHandler  
        B EXTI7_IRQHandler        

;NMI_Handler
;HardFault_Handler
;MemManage_Handler
;BusFault_Handler
;UsageFault_Handler
;SVC_Handler
;DebugMon_Handler
;PendSV_Handler
;SysTick_Handler
;Default_Handler
;__default_handler
;        CALL_GRAPH_ROOT __default_handler, "interrupt"
;        NOCALL __default_handler
;        B __default_handler

        END
