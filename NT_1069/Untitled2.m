clear all; 
clc; 
close all;

WIDTH       = 14;     % ����������� ���
max_log      = 20*log10(2^(WIDTH-1));
WIDTH_LOG   = double(int32(log2(max_log))+1);     % ����������� ����� �����
WIDTH_FR    = 3;                                  % ����������� ������� �����
WIDTH_OUT   = WIDTH_LOG + WIDTH_FR;
coeff       = zeros(WIDTH_LOG,1);
for i=1:WIDTH_OUT
    coeff(i) = 10^((2^(i-WIDTH_FR))/20);
    disp(coeff(i));
end     


len = 8192;
x = (1:1:len)';
y2 = 20*log10(x);

y  = zeros(len,1);
for i=1:len
    y(i) = adc2db (x(i), WIDTH_OUT, WIDTH_LOG, coeff);
end     

mism = y2-y;

figure_log = figure('Name', 'dB'); 
subplot(2, 1, 1);
plot(x, y, 'r');
hold on;
plot(x, y2, 'g');
subplot(2, 1, 2);
plot(x, mism, 'm');



% WIDTH_OUT - ����������� ��������� �����
% WIDTH_LOG - ����� �������� ����� �����
% x - ����
function y = adc2db (x, WIDTH_OUT, WIDTH_LOG, coeff)
    y = 0;
    stage  = zeros(WIDTH_OUT+1,1);
    stage(1)  = 1;
    for i=1:WIDTH_OUT
        temp = stage(i)*coeff(WIDTH_OUT+1-i);
        if temp <= x
            y = y + 2^(WIDTH_LOG+1-i);
            stage(i+1) = temp;
        else
            stage(i+1) = stage(i);
        end   
    end     
end