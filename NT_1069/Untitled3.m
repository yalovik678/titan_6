clear all; 
clc; 
close all;


LNA = [4.6 8.1 12.6 15.1] ;     % ������� �������� ��� ���
IFA.min  = 1.8  ;               % ����������� �������� ���
IFA.step = 0.73 ;               % ��� �������� ���
ADC = [0 10] ;                  % ������� �������� ��� ���

resolution = 0.25;             % ���������� ����������� �������
len = 300;                     % ������ �������

ref = (0:resolution:resolution*(len-1))';

contol.gain = zeros(len, 1);
contol.rf   = zeros(len, 1);
contol.ifa  = zeros(len, 1);
contol.adc  = zeros(len, 1);


for i=1:len
  % ref(i) - �������� �������
  mism = zeros(4,64,2);  
  abs_min.summ      = 0;
  abs_min.value     = 100;
  abs_min.idx_rf    = 1;
  abs_min.idx_ifa   = 1;
  abs_min.idx_adc   = 1;
  
  for idx_rf=1:4
     gain.rf = LNA(idx_rf); 
     for idx_ifa=1:64
        gain.ifa = ifa_code2dB(idx_ifa-1, IFA);
        for idx_adc = 1:2
            gain.adc = ADC(idx_adc);
            gain.summ = gain.rf + gain.ifa + gain.adc;
            mism(idx_rf, idx_ifa, idx_adc) = abs(gain.summ-ref(i));
            
            if (mism(idx_rf, idx_ifa, idx_adc) < abs_min.value) || (mism(idx_rf, idx_ifa, idx_adc) < 0.5)
                abs_min.summ      = gain.summ;
                abs_min.value     = mism(idx_rf, idx_ifa, idx_adc);
                abs_min.idx_rf    = idx_rf  ;
                abs_min.idx_ifa   = idx_ifa ;
                abs_min.idx_adc   = idx_adc ;
            end
            
        end    
     end 
  end    


    contol.gain(i) = abs_min.summ     ;
    contol.rf(i)   = abs_min.idx_rf   ;
    contol.ifa(i)  = abs_min.idx_ifa  ;
    contol.adc(i)  = abs_min.idx_adc  ;

  
 % [M, [midx_rf, midx_ifa, midx_adc]] = min(mism);
 % [M, I] = min(mism);
    
end    


figure_log = figure('Name', 'dB'); 
subplot(4, 1, 1);
plot(contol.gain);
title('contol.gain - full gain of receiver')
subplot(4, 1, 2);
plot(contol.rf);
title('contol.rf - LNA')
subplot(4, 1, 3);
plot(contol.ifa);
title('contol.ifa - IFA')
subplot(4, 1, 4);
plot(contol.adc);
title('contol.adc - FILTER HMC900 stage 10dB')


figure_2 = figure('Name', 'mism'); 
plot(ref);
hold on
plot(contol.gain);
hold off

save_to_file(uint16(contol.gain * 256), 'contol_gain.txt');
save_to_file(contol.rf -1,   'contol_rf.txt');
save_to_file(contol.ifa-1,  'contol_ifa.txt');
save_to_file(contol.adc-1,  'contol_adc.txt');

% ������� � ����

function save_to_file(data, file) 
    sw = sprintf('%10d\n', data');
    outfile = fopen(file, 'wt');
    fprintf(outfile, '%s', sw');
    fclose(outfile);
end

function dB = ifa_code2dB(code, IFA)
    dB = IFA.min + code * IFA.step;
end