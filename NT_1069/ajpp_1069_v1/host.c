#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"


int Host(uint16_t data, int temp){
  uint16_t whole = (data>>3) & 0x1FF;
  uint16_t ref = 360;        //90dB * 4 = 360 
  uint16_t det =(uint16_t) (unsigned) data;
  uint16_t mism =(uint16_t) ((signed) ref - (signed) det);
  uint16_t mism_magn =(uint16_t) abs((signed) mism)>>2;
  uint16_t mism_sing = (mism>>9) & 0x1;
  
  uint16_t mism_dz, mism_dz_limit;
  uint16_t dz= 60;
  int max_step = 20;     //10dB * 4       

//////STAGE 1
  
  if (mism_magn > dz)
    mism_dz = mism_magn - dz;
  else
    mism_dz = 0;
  
  if(mism_dz > max_step)
    mism_dz_limit = max_step;
  else
    mism_dz_limit = mism_dz;
  
//////STAGE 2
  uint16_t step;
  
  if(mism_sing == 0x1)
    step = - mism_dz_limit;
  else
    step = mism_dz_limit;

//////STAGE 3  
 
  int gain_control, gain_control_bk ;
  int max_control =300;

  temp = temp + step;
  
  if(temp<0)
    temp = 0;
  if(temp>max_control-1)
    temp = max_control-1;
  
  gain_control = (int)temp;
  gain_control_bk = gain_control;
  
  return gain_control;
 }


