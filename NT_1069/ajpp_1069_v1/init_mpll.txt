uint8_t dmix[9] = {0x4d, 0x80, 0x80, 0x80, 0x80, 16, 0x50, 0x06, 0x00};  //1506-1590 MHz

int init_MPLL(void) 
{
volatile int* SPI = (volatile int *)SPI1_BASE;
volatile int* GPIO = (volatile int *)GPIO_BASE;
uint32_t din, wc;
/*ADDRESS DEFAULTVALUE SETTING REGISTER FUNCTION
0x00    0x3E    2.56GHz         LO Frequency Tuning
0x01    0x84    DG = �4         Gain
0x02    0x80    0mV     Offset I-Channel
0x03    0x80    0mV     Offset Q-Channel
0x04    0x80    0dB     I/Q Gain Ratio
0x05    0x10    0�      I/Q Phase Balance
0x06    0x50    OFF     LO Port Matching Override
0x07    0x06    OFF     Temperature Correction Override
0x08    0x00    NORMAL  Operating Mode */

while (SPI[SSSR]&0x8); // BSY  
SPI[SSCR0] = 0x091F; // 32-bits, CPU_CLK / 20
SPI[SSCR1] = 0x2488;     // write only, CS2, CS aktiv high
GPIO[GPIO_ALT_1]  |= 0x27;        // spi1 (cs2)
GPIO[GPIO_DIR_1] &= (~0x24);   // set "in" direction for scs2_2 & sclk_2 pins  
 
//while(1)
din = ReadFromMPLL(0); // chip_ID A7975
if (din != 0xA7975) 
  return 1;
WriteToMPLL(0, 0x000020);  // soft reset

WriteToMPLL(2, 0x000001);  // Reference Divider = 1
//WriteToMPLL(7, 0x00014d);  // PFD default
WriteToMPLL(9, 0x547FFF);  // Charge Pump CP = 2.54mA, CP DN Offset = 420uA, Fractional Mode
WriteToMPLL(0xB, 0x0FC0E1);  // PD UP and DN enabled, CSP On, Force CP disabled

WriteToMPLL(5, 0x0f88);  // all subsystem enable
//WriteToMPLL(5, 0xE090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 11 = Max Gain, Divider Output Stage Gain Control = 1
//WriteToMPLL(5, 0x8090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 00 = Min Gain, Divider Output Stage Gain Control = 1
WriteToMPLL(5, 0xA090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 01 = 0 db, Divider Output Stage Gain Control = 1
WriteToMPLL(5, 0x2A98);  // RF Buffer = SE, Manual RFO Mode = 1, RF Buffer Bias = 10, Spare Don�t Cares = 0010
WriteToMPLL(5, 0x60A0);  // default
WriteToMPLL(5, 0x1628);  // default
WriteToMPLL(5, 0);  // close reg_5 write

WriteToMPLL(0xA, 0x2046);  // AutoCal enabled, VSPI trigger enabled
WriteToMPLL(0x6, 0x200B4A);  // Fractional B Mode
WriteToMPLL(3, 159);
WriteToMPLL(4, 0);

din = ReadFromMPLL(3);
if (din != 159) 
  return 1;
wc = 1000;
do {
din = ReadFromMPLL(0x12);
wc--; 
} while (((din & 0x2) == 0) && (wc > 0) );   // wait Lock
if ((din & 0x2) == 0)  
  return 1;

GPIO[GPIO_DIR_1] |= 0x20;   // set "out" direction for scs2_2 pin
GPIO[GPIO_ALT_1] &= (~0x20);   // off spi2 (cs2)

// --------------------------------------------------------- Mixer ---
SPI[SSCR0] = 0x090F; // 16-bits, CPU_CLK / 20
GPIO[GPIO_ALT_1] |= 0x18;        // spi2 (cs0, cs1)

WriteToMIX(0x80+8, 0x8);   // sreset
din = ReadFromMIX(0x80);
if (din != 0x3e) 
  return 1;

for (int i = 0; i < 9; i++) {
  WriteToMIX(0x80+i, dmix[i]);  
}

for (int i = 0; i < 9; i++) {
  if (ReadFromMIX(0x80+i) != dmix[i]) 
    return 1;
}
return 0;
}  