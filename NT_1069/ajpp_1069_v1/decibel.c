#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"

//uint8_t DWIDTH = 16;
// uint8_t size = 32;
  int DWIDTH_IN = 14;
  int DWIDTH_WH = 7;
  int DWIDTH_FR = 3;

  int CWIDTH_WH = 24;
  int CWIDTH_FR = 8; 
  int CWIDTH = 32;
  
 
/*int* Init_coeff_vector(void){
   double temp;
   int result[10] ;
   for(int i=0; i<10; i++){
     temp =  pow(2, (i - DWIDTH_FR));
     temp = pow(10,(temp/20)); 
     temp = temp * pow(2, CWIDTH_FR);
     result[i]= (int) temp ;
   }
  
   return result;
}*/

 

uint16_t Init1(void){
   double temp;
   int result;
   temp = 1 * pow(2, CWIDTH_FR);
   result = (uint16_t)(signed)(int) temp ;
  
   return result;
   
}
 
uint16_t Machine(uint16_t data){ 
  
   double temp;
   uint16_t result[10] ;

   for(int i=0; i<10; i++){
     temp =  pow(2, (i - DWIDTH_FR));
     temp = pow(10,(temp/20)); 
     temp = temp * pow(2, CWIDTH_FR);
     result[i]= (uint16_t)(signed)(int) temp ;
   }
  
  int state = Init1();
  uint32_t   level; 
  uint16_t out_data = 0;
  //int* h = Init_coeff_vector();
  
  for (int i=9; i>=0; i--){
    level = (unsigned) state * (unsigned)(result[i]);
    level = (level>>8) & 0xFFFFFFFF ; 
   
    if ((unsigned) level<(unsigned) data){
      state = level;
      out_data = out_data | (0x1 << i) ;       
    }  
  }
   return  out_data;
} 




