#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"
#include "rom.h"


int ADC_rom(int data){
  if (data>254)
    return 1;
  else 
    return 0;
}


uint8_t rf_ifa_rom(int data){
  int* coff_ifa;
  int* coff_rf;
  coff_rf = coff_rf_rom;
  coff_ifa = coff_ifa_rom; 
  
  return (uint8_t)coff_ifa[data]<<2 & coff_rf[data];

}


/*int rf_rom(int data){
  int* coff_rf;
  coff_rf = coff_rf_rom;
  return coff_rf[data];

}*/



