#include <string.h>
#include "MCU32.h"
#include "wake.h"
#include "ak.h"
/************************************************************************************
*                                CONSTANTS
*************************************************************************************/

/************************************************************************************
*                                VARIABLES
*************************************************************************************/
extern WakePackageT wake_UART_1_rx;
int RxWakeTimer;

//int Tics_2kHz = 0;  // ���� ���������� 2 ���
int SysTickFlg = 0;  // ���� ������� �� ���������� �������
//int Cnt10ms = 0;    // ������� 0..19 ���������� 2 ���
//int Cnt40ms = 0;    // ������� 0..3 ����� �������� 25 ��

//extern int AGC_on;

unsigned int sys_tics = 0;             // ����� �� ������ �������� ������ � SysTickFlg
uint32_t prog_status, prog_cntrl;
uint32_t AK_off_timer = 0;


extern uint32_t trimming_step;
//extern uint32_t trimming_err;
extern uint16_t trimming_pause;
extern uint8_t ifa_agc_lev[];

void WriteToMax14662(uint8_t chan,  uint8_t dat) ;
/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/

extern void HW_Init(void);
extern int init_RFIC1(void);
extern int init_RFIC(uint8_t rfic);
extern uint8_t rd_RFIC(uint8_t rfic, uint8_t reg);
extern int init_CLOCK_PLL(void);
extern int init_ADC(int adc);
extern void change_main_clock(int);
//void CPU_InitTick (void);
extern void sppStartDataProcessing(int chan);
//int soft_SPI(void);
extern void WakeTxStart(WakePackageT * package);
//extern void CheckRxWake(void);
extern void WakeReadInit(WakePackageT * package);
//extern void  USART1_Send (void);
//extern void  USART2_Rcv (void);
//extern void init_host_rx(void); 
//extern void init_DAC(void);
extern void initAK(void);
//extern void init_ds_rx(void);
extern void AGC(void);
extern void asc_host(WakePackageT * package);
//extern void send_to_host(uint8_t bt);
//extern void init_board_rx(void);
extern void trimming(void);
//extern void send_registrator_data(void);
extern void read_flash_id(void);
extern void write_flash_image(void);
int trim_ADC_1056(int num);
void RF_AGC(void);

void CheckRegistratorData();

void SET_RF_AGC(void);
extern void CalibrTonOff(void);
extern void RESTART(void);
extern int init_MPLL(void);
extern int trim_ADC(int num);
extern void HSI_Init(void);

extern int* Init_data_vector(void);  
/*************************************************************************************
*                                  main()
* Arguments   : none
**************************************************************************************/

int main()
{

int init_error = 0x7, init_try = 5;
volatile int *GPIO = (volatile int*) GPIO_BASE;
//int i;
  //  setlocale (LC_ALL, "C"); // ������������� stdio �������
volatile int *portNVIC_ISER = (volatile int *)NVIC_ISER; //0xe000e100;  
volatile int *portNVIC_IPR1 = (volatile int *)NVIC_IPR1;
volatile int* UART1 = (volatile int *)UART1_BASE;

  sys_tics = 0;
  SysTickFlg = 0;
  prog_status = TRIMMING_ADC1_FAIL | TRIMMING_ADC0_FAIL; // status word
//  prog_cntrl = RF_AGC_ON; // | RF_AGC_MANUAL;
  prog_cntrl = ATT_ENABLE;
//  AGC_on = 0; 
  trimming_step = 0;
  trimming_pause = 0;

  HW_Init();  // �������������, ���������� ����������
 /* do { 
    if (init_error & 0x1) {
      if (init_RFIC(0) == 0) 
        init_error &= ~(0x1);   // RFIC 0 initialization Ok, VCO lock Ok
    } else {
      if (init_error & 0x2) {
//#ifdef IDT5P49V5901A        
//        if (init_CLOCK_PLL() == 0) 
//#endif          
          init_error &= ~(0x2);   // config write Ok, Lock Ok
      } else {  
        if (init_ADC(0) == 0) 
          init_error &= ~(0x4);   // config write Ok  
      } 
    }    
    init_try--;
  } while (init_error && init_try);  
  
  if (init_error & 0x4) 
    while(1);*/

  HSI_Init(); 

  change_main_clock(SRC_HF_CLK);  
  
  initAK(); 
  
/*#if ADC_NUM == 2
    init_try = 5;
    while (init_RFIC(1) && init_try) init_try--;
    if(init_try == 0) 
     prog_status |= INIT_RFIC_1_FAIL;
    init_try = 5;
    while (init_ADC(1) && init_try) init_try--;
    if(init_try == 0) 
     prog_status |= INIT_ADC_1_FAIL;
#endif  
//  initAK();  
  
  if(trim_ADC(0)) 
    prog_status &= ~TRIMMING_ADC0_FAIL;
#if ADC_NUM == 2  
  if(trim_ADC(1)) 
    prog_status &= ~TRIMMING_ADC1_FAIL; 
#else  
  prog_status &= ~TRIMMING_ADC1_FAIL;  
#endif*/
  
  WriteToMax14662(0x2,0xff); //CS2, MOD_B
  WriteToMax14662(0x3,0xff); //CS3, MOD_A
  
  WriteToMax14662(0x1, 0x77); //CS1 SW_CS_GAIN_A(ABCD)
  WriteToMax14662(0x0, 0x77); //CS0 SW_CS_GAIN_B(EFGH)

  WakeReadInit(&wake_UART_1_rx);
//  *portNVIC_IPR1 = 0x00000100;                // UART_2_priority
  *portNVIC_IPR1 = 0x00000001;                // UART_1_priority
  *portNVIC_ISER = USART1_IRQ_Enable;         // set UART_1_INT 
//  *portNVIC_ISER = USART2_IRQ_Enable;         // set UART_2_INT
//  *portNVIC_ISER = USART3_IRQ_Enable;         // set UART_3_INT
//    AGC_on = 1;

while (!SysTickFlg); 
SysTickFlg = 0;  
  
/*  init_try = 5;
  while (init_MPLL() && init_try) {
    init_try--;
    while (!SysTickFlg); 
    SysTickFlg = 0;
  } 
  if(init_try == 0) 
    prog_status |= INIT_MODUL_FAIL; */
    
//  GPIO[GPIO_OUT_1] |= 0x00000004;  // set sclk_1 pin  // HMS830 open mode   for debug
    
  
while (!SysTickFlg); 
SysTickFlg = 0;
//1058 handshake
GPIO[GPIO_ALT_1] |= 0x01000000;    // uart1 (tx_only)
GPIO[GPIO_DIR_1] &= ~(0x01000000);   // clear "out" direction for txd_1
  
while (1) {
    if (SysTickFlg) {
      SysTickFlg = 0;
      sys_tics++;
      if (AK_off_timer) AK_off_timer--;
      //if ((sys_tics&0x3f) == 0) RF_AGC();
 
      if (prog_cntrl & RESTART_ALL)      // RESTART ALL  
        RESTART();
      
//      if ((sys_tics&0x3) == 0) {  // 100 Hz
      if ((sys_tics&0xf) == 0) {  // 400 Hz
        if (prog_cntrl & WR_FLASH_IMAGE) 
          write_flash_image();
      }
      if (RxWakeTimer) {
        RxWakeTimer--;
        if (RxWakeTimer <= 0) {
          wake_UART_1_rx._status |= WAKE_READ_TIMEOUT;
          UART1[UARTINTMASK] &= ~0x2;    // clear RXINT
          RxWakeTimer = 0;
        }    
      }  // (RxWakeTimer) {
    }  // if (SysTickFlg) {
    
    if ((wake_UART_1_rx._status & WAKE_RESV_PACKET) || (wake_UART_1_rx._status & WAKE_READ_TIMEOUT)) {  // received HOST command
      asc_host(&wake_UART_1_rx);
      WakeReadInit(&wake_UART_1_rx);
    }  
  }  // while (1) {
    return 0;
}
/*********************************************************************
*                            End Of File
**********************************************************************/
