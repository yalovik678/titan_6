--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------
--		формирование статусного пакета


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity agc_status_drv0 is
	port
	(
		clk				: in	std_logic;			-- dsp clock
		reset	 		: in	std_logic;			-- active reset
		start			: out	std_logic;			
		stop			: in	std_logic;			
		sel				: out	natural range 0 to 7;			
		save			: out	std_logic
	);
end entity agc_status_drv0;

		

architecture rtl of agc_status_drv0 is

	
--	+----------------------------------------------------------------------------
-- 	|	selector
--	|		
--	+----------------------------------------------------------------------------
	signal 	selector			: natural range 0 to 7; 
	signal 	selector_reset		: std_logic;
	signal 	selector_inc		: std_logic;
	signal 	selector_stop		: std_logic;
	
--	+----------------------------------------------------------------------------
-- 	|	Machine 
--	|		
--	+----------------------------------------------------------------------------
	type t_machine 	is  (
							s_idle, 
							s_prepare,
							s_start,
							s_work,
							s_save,
							s_new
						);
								
	attribute syn_encoding 	: string;
	attribute syn_encoding of t_machine 	: type is "safe";      
	signal host_d   		: t_machine;
	signal host_q   		: t_machine;


begin

--	+----------------------------------------------------------------------------
-- 	|	Selector 
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			selector <= 0;
		elsif (rising_edge(clk)) then
			if selector_reset = '1' then
				selector <= 0;
			elsif selector_inc = '1' then
				selector <= (selector + 1) mod 8;
			end if;	
		end if;
	end process;
	
	sel <= selector;

	selector_stop 	<= '1' when (selector = 7) else '0';

--	+----------------------------------------------------------------------------
-- 	|	Machine 
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if reset = '1' then
			host_q <= s_idle;
		elsif (rising_edge(clk)) then
			host_q <= host_d;
		end if;	
	end process;

	process (
			host_q, 
				stop,
				selector_stop
			)

	begin
		host_d <= host_q;

		start				<= '0';	
		save				<= '0';	
	 	selector_reset		<= '0';
	 	selector_inc		<= '0';

		case host_q is
			when s_idle =>
				selector_reset	<= '1';
				host_d <= s_prepare; 


			when s_prepare =>
				host_d <= s_start; 

			when s_start =>
				start	<= '1';	
				host_d <= s_work; 

				
			when s_work =>
				if stop = '1' then
					host_d 	<= s_save; 
				end if;

			when s_save =>
				host_d 	<= s_new; 
					save	<= '1';

			when s_new =>
				host_d 	<= s_prepare; 
					selector_inc <= '1';

			
			when others => 
				host_d 	<= s_idle; 
				
		end case;
	end process;


				
end rtl;
					

