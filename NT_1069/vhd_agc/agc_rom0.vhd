--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity agc_rom0 is
	port 
	(	
		clk			: in	std_logic;
		control		: in	natural range 0 to 299;				-- control (dB * 4)
		gain		: out	std_logic_vector(15 downto 0);		-- 8.8 (dB) actual gain	
		rf			: out	std_logic_vector(1 downto 0);		-- 0-3
		ifa			: out	std_logic_vector(5 downto 0);		-- 0-63
		adc			: out	std_logic
	);
end agc_rom0;

architecture syn of agc_rom0 is

--	+----------------------------------------------------------------------------
-- 	|	ПЗУ фактического усиления
--	|		
--	+----------------------------------------------------------------------------
	component agc_gain_rom0 is
		port 
		(	
			clk			: in	std_logic;
			addr		: in	natural range 0 to 299;
			data		: out	std_logic_vector(15 downto 0)		-- 8.8
		);
	end component;

--	+----------------------------------------------------------------------------
-- 	|	ПЗУ управления RF
--	|		
--	+----------------------------------------------------------------------------
	component agc_rf_rom0 is
		port 
		(	
			clk			: in	std_logic;
			addr		: in	natural range 0 to 299;
			data		: out	std_logic_vector(1 downto 0)		-- 0-63
		);
	end component;

--	+----------------------------------------------------------------------------
-- 	|	ПЗУ управления IFA
--	|		
--	+----------------------------------------------------------------------------
	component agc_ifa_rom0 is
		port 
		(	
			clk			: in	std_logic;
			addr		: in	natural range 0 to 299;
			data		: out	std_logic_vector(5 downto 0)		-- 0-63
		);
	end component;
	
--	+----------------------------------------------------------------------------
-- 	|	ПЗУ управления ADC
--	|		
--	+----------------------------------------------------------------------------
	component agc_adc_rom0 is
		port 
		(	
			clk			: in	std_logic;
			addr		: in	natural range 0 to 299;
			data		: out	std_logic
		);
	end component;



		
begin

--	+----------------------------------------------------------------------------
-- 	|	ПЗУ фактического усиления
--	|		
--	+----------------------------------------------------------------------------
	gain_rom : agc_gain_rom0 
		port map
		(	
			clk			=>	clk		,	
			addr		=>	control	,	
			data		=>	gain
		);
	
--	+----------------------------------------------------------------------------
-- 	|	ПЗУ управления RF
--	|		
--	+----------------------------------------------------------------------------
	rf_rom : agc_rf_rom0 
		port map
		(	
			clk			=>	clk		,	
			addr		=>	control	,	
			data		=>	rf	
		);

--	+----------------------------------------------------------------------------
-- 	|	ПЗУ управления IFA
--	|		
--	+----------------------------------------------------------------------------
	ifa_rom : agc_ifa_rom0 
		port map
		(	
			clk			=>	clk		,	
			addr		=>	control	,	
			data		=>	ifa
		);

--	+----------------------------------------------------------------------------
-- 	|	ПЗУ управления ADC
--	|		
--	+----------------------------------------------------------------------------
	adc_rom : agc_adc_rom0 
		port map
		(	
			clk			=>	clk		,	
			addr		=>	control	,	
			data		=>	adc
		);





end syn;


