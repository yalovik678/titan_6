--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.numeric_bit.all;
use ieee.math_real.all;
use ieee.math_complex.all;

entity detector0 is
	generic
	(
		DWIDTH      	: natural	:= 16	  -- Разрядность входных данных
	);
	port
	(
		clk				: in	std_logic;			-- dsp clock
		reset	 		: in	std_logic;			-- active reset
		data			: in	std_logic_vector(DWIDTH-1 downto 0); 			
		result			: out	std_logic_vector(DWIDTH-1 downto 0)
	);
end entity detector0;

		

architecture rtl of detector0 is

--	+----------------------------------------------------------------------------
-- 	|	Функция расчета коэффициентов
--	|		
--	+----------------------------------------------------------------------------
	constant P_SHIFT			: natural	:= 5;
	constant I_SHIFT			: natural	:= 10;
	
--	constant EDWIDTH			: natural	:= DWIDTH + maximum(P_SHIFT, I_SHIFT); -- maximum only VHDL 2008
	constant EDWIDTH			: natural	:= DWIDTH + I_SHIFT;

	subtype  t_data is std_logic_vector(DWIDTH-1 downto 0); 
	subtype  t_edata is std_logic_vector(EDWIDTH-1 downto 0); 

	function expand_high(arg : t_data) return t_edata is
		variable	temp : t_edata;
	begin
		temp := (others => '0');
		temp(EDWIDTH-1 downto EDWIDTH-DWIDTH) := arg;
		return temp;
	end function expand_high;
	
	function crop(arg : t_edata) return t_data is
		variable	temp : t_data;
	begin
		if arg(EDWIDTH-1) = '1' then
			temp := (others => '0');
		else
			temp := arg(EDWIDTH-1 downto EDWIDTH-DWIDTH);
		end if;	
		return temp;
	end function crop;
--	+----------------------------------------------------------------------------
-- 	|	Входной регистр
--	|		
--	+----------------------------------------------------------------------------
	signal	rg_data				: t_data;	
	signal	abs_data			: t_data;	
	signal	abs_edata			: t_edata; 
	
--	+----------------------------------------------------------------------------
-- 	|	Фильтр
--	|		
--	+----------------------------------------------------------------------------
	signal	mism				: t_edata; 
	signal	mism_p				: t_edata; 
	signal	mism_i				: t_edata; 
	signal	acc_i				: t_edata; 
	signal	acc					: t_edata; 



begin

--	+----------------------------------------------------------------------------
-- 	|	Входной регистр
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			rg_data		<= (others => '0');
			abs_data	<= (others => '0');
		elsif (rising_edge(clk)) then
			rg_data		<= data;
			abs_data	<= std_logic_vector(abs(signed(rg_data)));
		end if;
	end process;

	abs_edata	<= expand_high(abs_data);


--	+----------------------------------------------------------------------------
-- 	|	Фильтр
--	|		
--	+----------------------------------------------------------------------------
	mism	<= std_logic_vector(signed(abs_edata) - signed(acc));
	mism_p	<= std_logic_vector(shift_right(signed(mism), P_SHIFT));
	mism_i	<= std_logic_vector(shift_right(signed(mism), I_SHIFT));

	process (clk, reset)
	begin
		if (reset = '1') then
			acc_i		<= (others => '0');
			acc			<= (others => '0');
		elsif (rising_edge(clk)) then
			acc_i	<= std_logic_vector(signed(acc_i) + signed(mism_i));
			acc		<= std_logic_vector(signed(acc) + signed(acc_i) + signed(mism_p));
		end if;
	end process;

	result	<= crop(acc);
	


				
end rtl;
