library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

entity agc_adc_rom0 is
	port 
	(	
		clk			: in	std_logic;
		addr		: in	natural range 0 to 299;
		data		: out	std_logic
	);
end agc_adc_rom0;

architecture syn of agc_adc_rom0 is

begin

	process (clk)
	begin
		if (clk'event and clk = '1') then
			if addr >= 254 then
				data <= '1';
			else
				data <= '0';
			end if;	
		end if;
	end process;

end syn;


