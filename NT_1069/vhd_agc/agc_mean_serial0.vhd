--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

--	последовательный усреднитель

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;


-- library work;
-- use work.agc_package.all;


entity agc_mean_serial0 is
	generic
	(
		DWIDTH	 	 	: natural	:= 16	; 
		STAGES			: natural	:= 10	;	
		STAGES_LOG2		: natural	:= 4	
	);
	port
	(
		clk				: in	std_logic;			-- dsp clock
		reset	 		: in	std_logic;			-- active reset
		div				: in	std_logic_vector(STAGES_LOG2-1 downto 0); 	
		max_n_mean		: in	std_logic;
		valid_i			: in	std_logic;			
		data_i 			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		valid_o			: out	std_logic;			
		data_o 			: out	std_logic_vector(DWIDTH-1 downto 0)
	);
end entity agc_mean_serial0;

		

architecture rtl of agc_mean_serial0 is


--	+----------------------------------------------------------------------------
-- 	|	BASE TYPE
--	|		
--	+----------------------------------------------------------------------------
	subtype t_data is std_logic_vector(DWIDTH-1 downto 0);		

	type t_port is				
		record
			valid			: std_logic;
			data			: t_data;			
		end record; 

	function default_port return  t_port is
	begin
		return t_port'('0', (others => '0'));
	end function default_port;
	

	type t_port_vector is array (natural range <>) of t_port; 
	
	subtype t_port_vector2 is t_port_vector(0 to 1);		
	
--	+----------------------------------------------------------------------------
-- 	|	MODE TYPE
--	|		
--	+----------------------------------------------------------------------------
	type t_pos 		is (DOUBLE, LAST);
	type t_mode 	is (MEAN2, MAX2);

	constant MAX_STAGES : natural := 2**STAGES_LOG2;	
	
	type t_pos_vector is array (0 to STAGES-1) of t_pos; 				-- управляющий вектор
	type t_pos_matrix is array (0 to MAX_STAGES-1) of t_pos_vector; 	-- управляющие вектора для всех вариантов управления
	
	function init_pos_matrix return t_pos_matrix is
		variable temp : t_pos_matrix;
	begin
		for i in 0 to MAX_STAGES-1 loop
		-- i - управление 
			for j in 0 to STAGES-1 loop
			-- j - стадия конвейера
				temp(i)(j) := DOUBLE;
			end loop;
		--	if (i > 0) AND (i <= STAGES) then
		--		temp(i)(i-1) := LAST;
		--	end if;

			if (i > 0) then
				if (i <= STAGES) then
					temp(i)(i-1) := LAST;
				else
					temp(i)(STAGES-1) := LAST;
				end if;
			end if;

		end loop;
		return temp;
	end function init_pos_matrix;
	
	constant pos_matrix		: t_pos_matrix := init_pos_matrix;
	
	signal	pos_vector		: t_pos_vector;
	signal	mode			: t_mode;
--	+----------------------------------------------------------------------------
-- 	|	ACC2 CONTEXT
--	|		
--	+----------------------------------------------------------------------------
	type t_context is				
		record
			phase			: boolean;
			data			: t_port_vector2;			
		end record; 

	function default_context return  t_context is
	begin
		return t_context'(false, (others => default_port));
	end function default_context;

	type t_context_vector is array (0 to STAGES-1) of t_context;


	-- 	расчет нового значения в зависимости от позиции и режима работы
	--	на входе phase игнорируется
	function acc2(input, output : t_context; position : t_pos; mode : t_mode) return t_context is
		subtype t_edata is std_logic_vector(DWIDTH downto 0);		
		alias 	arg_a is  input.data(0).data;
		alias 	arg_b is  output.data(0).data;

		variable data_a : t_edata;
		variable data_b : t_edata;
		variable summa 	: t_edata;
		variable result : t_context;
	begin
	
		-- управление первым выходом (и контекстом)
		
		if input.data(0).valid = '1' then
			if output.phase then
			-- выполняем целевую операцию 
				result.phase 			:= false;
				result.data(0).valid 	:= input.data(0).valid;
				if mode = MEAN2 then
				-- вычисляем среднее из двух
					data_a 	:= std_logic_vector(resize(signed(arg_a), DWIDTH+1)); 
					data_b 	:= std_logic_vector(resize(signed(arg_b), DWIDTH+1)); 
					summa 	:= std_logic_vector(signed(data_a) + signed(data_b));
					result.data(0).data := summa(DWIDTH downto 1);
				else
				-- вычисляем максимальное из двух
					if signed(input.data(0).data) > signed(output.data(0).data) then
						result.data(0).data := input.data(0).data;
					else
						result.data(0).data := output.data(0).data;
					end if;
				end if;
			else
			-- записываем вход 
				result.phase 			:= true;
				result.data(0).data 	:= input.data(0).data;
				result.data(0).valid 	:= '0';
			end if;
		else
		-- храним прошлое значение (кроме валида)
			result := output;
			result.data(0).valid 	:= '0';
		end if;	
	
		-- управление вторым выходом
		if position = DOUBLE then
		-- 1->1
			result.data(1) := input.data(1);
		else
		-- LAST
		-- 0->1
			result.data(1) := result.data(0);
		end if;
	
	
		return result;
	end function acc2;

--	+----------------------------------------------------------------------------
-- 	|	Каскады
--	|		
--	+----------------------------------------------------------------------------
	signal input 				: t_port ;
	signal output 				: t_port ;

	signal input_context		: t_context ;
	signal context_vector		: t_context_vector := (others => default_context);
	


begin

--	+----------------------------------------------------------------------------
-- 	|	Управление и вход
--	|		
--	+----------------------------------------------------------------------------

	pos_vector		<= pos_matrix(to_integer(unsigned(div)));
	mode			<= MAX2 when max_n_mean = '1' else MEAN2;

	input.valid		<= valid_i	; 
	input.data		<= data_i 	; 

	input_context.phase		<= false;
	input_context.data(0)	<= input;
	input_context.data(1)	<= input;

--	+----------------------------------------------------------------------------
-- 	|	Каскады
--	|		
--	+----------------------------------------------------------------------------

	process (clk, reset)
	--	variable	
	begin
		if (reset = '1') then
			context_vector(0)	<= default_context;
		elsif (rising_edge(clk)) then
			context_vector(0)	<= acc2(input_context, context_vector(0), pos_vector(0), mode);
		end if;
	end process;

	
	for_cascade:
	for i in 1 to STAGES-1 generate
	
		process (clk, reset)
		begin
			if (reset = '1') then
				context_vector(i)	<= default_context;
			elsif (rising_edge(clk)) then
				context_vector(i)	<= acc2(context_vector(i-1), context_vector(i), pos_vector(i), mode);
			end if;
		end process;
	end generate;

--	+----------------------------------------------------------------------------
-- 	|	Выход
--	|		
--	+----------------------------------------------------------------------------

	output	<= context_vector(STAGES-1).data(1);

	valid_o		<= output.valid; 
	data_o 		<= output.data; 


end rtl;
					

