--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------
--		формирование старта принятия решения контроллера АРУ


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity agc_timer_drv0 is
	port
	(
		clk				: in	std_logic;			-- dsp clock
		reset	 		: in	std_logic;			-- active reset
		enable			: in	std_logic;
		div				: in	std_logic_vector(3 downto 0);
		acc_control		: out	std_logic_vector(4 downto 0);
		start			: out	std_logic
	);
end entity agc_timer_drv0;

		

architecture rtl of agc_timer_drv0 is

	attribute mark_debug		: string;
--	+----------------------------------------------------------------------------
-- 	|	interval
--	|		
--	+----------------------------------------------------------------------------
	
	type t_real_vector is array (0 to 15) of real; 
	type t_natural_vector is array (0 to 15) of natural; 
	
	constant FCLK 		: real := 100.0e6; 
	
	function init_real_intevals return t_real_vector is
		variable temp : t_real_vector;
	begin
		temp( 0) := 5.0e-6;
		temp( 1) := 10.0e-6;
		temp( 2) := 20.0e-6;
		temp( 3) := 50.0e-6;
		temp( 4) := 100.0e-6;
		temp( 5) := 200.0e-6;
		temp( 6) := 500.0e-6;
		temp( 7) := 1.0e-3;
		temp( 8) := 2.0e-3;
		temp( 9) := 5.0e-3;
		temp(10) := 10.0e-3;
		temp(11) := 20.0e-3;
		temp(12) := 50.0e-3;
		temp(13) := 100.0e-3;
		temp(14) := 200.0e-3;
		temp(15) := 500.0e-3;
		return temp;
	end function init_real_intevals;

	function init_natural_vector return t_natural_vector is
		variable interv  : t_real_vector := init_real_intevals;
		variable temp 	 : t_natural_vector;
	begin
		for i in 0 to 15 loop
			temp(i)	:= integer(interv(i)*FCLK) - 1;
		end loop;
		return temp;
	end function init_natural_vector;
	
	constant STOP_VALUES		: t_natural_vector := init_natural_vector;
	
	function log2(N : in integer) return natural is
		variable size			: natural;
	begin	
		size := 1;
		while 2**size < N loop
			size := size + 1;
		end loop;
		return size;
	end log2;
	
	function init_acc_vector return t_natural_vector is
		variable interv  : t_real_vector := init_real_intevals;
		variable temp 	 : t_natural_vector;
	begin
		for i in 0 to 15 loop
			temp(i)	:= log2(integer(interv(i)*FCLK * 0.25)) - 1;
		end loop;
		return temp;
	end function init_acc_vector;
	
	constant ACC_VALUES		: t_natural_vector := init_acc_vector;
--	+----------------------------------------------------------------------------
-- 	|	timer
--	|		
--	+----------------------------------------------------------------------------
	signal 	load_val		: natural; 
	signal 	timer			: natural; 
	signal 	timer_stop		: std_logic;
	
	attribute mark_debug of	timer					: signal is "true";
	attribute mark_debug of	start					: signal is "true";
	attribute mark_debug of	timer_stop				: signal is "true";



begin

	load_val	<= STOP_VALUES(to_integer(unsigned(div)));

	acc_control	<= std_logic_vector(to_unsigned(ACC_VALUES(to_integer(unsigned(div))), 5));

--	+----------------------------------------------------------------------------
-- 	|	timer 
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			timer <= 0;
		elsif (rising_edge(clk)) then
			if enable = '1' then
				if timer_stop = '1' then
					timer <= load_val;
				else
					timer <= timer - 1;
				end if;	
			else
				timer <= load_val;
			end if;
		end if;
	end process;
	
	timer_stop 	<= '1' when (timer = 0) else '0';

	
	process (clk, reset)
	begin
		if (reset = '1') then
			start <= '0';
		elsif (rising_edge(clk)) then
			start <= timer_stop;
		end if;
	end process;
	
	
	
end rtl;
					

