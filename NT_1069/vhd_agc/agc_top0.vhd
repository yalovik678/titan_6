--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

entity agc_top0 is
	generic
	(
		DWIDTH      	: natural	:= 16	;	-- Разрядность входных данных
		MAX_CONTROL		: natural 	:= 300 	 	-- 299
	);
	port
	(
		clk				: in	std_logic;								-- dsp clock
		reset	 		: in	std_logic;								-- active reset
		data			: in	std_logic_vector(DWIDTH-1 downto 0); 	-- ADC			
		control			: out	natural range 0 to MAX_CONTROL-1;		-- control (dB * 4)
		no_change		: out	std_logic;								-- устанавливается на время до слекдущего цикла.
		no_jammer		: out	std_logic;								-- признак того, что нет помехи...
		gain			: out	std_logic_vector(15 downto 0);			-- 8.8 (dB) actual gain	
		rf				: out	std_logic_vector(1 downto 0);			-- 0-3
		ifa				: out	std_logic_vector(5 downto 0);			-- 0-63
		adc				: out	std_logic								-- 10dB additional gain in ADC
	);
end entity agc_top0;

		

architecture rtl of agc_top0 is

--	+----------------------------------------------------------------------------
-- 	|	Константы 
--	|		
--	+----------------------------------------------------------------------------
	constant	max_log			: real := 20.0 * log10 (2.0 ** real(DWIDTH - 1));
	constant	DWIDTH_WH 	 	: natural := integer (log2(max_log)) + 1;     -- Разрядность целой части результата
	constant	DWIDTH_FR   	: natural := 2   ; 	  						-- Разрядность дробной части результата
	
	constant	CYCLE_LOG2		: natural := 4	;  -- число тактов valid 

--	+----------------------------------------------------------------------------
-- 	|	Пик-детектор
--	|		
--	+----------------------------------------------------------------------------
	component pick_detector0 is
		generic
		(
			DWIDTH      	: natural	:= 16	  -- Разрядность входных данных
		);
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			data			: in	std_logic_vector(DWIDTH-1 downto 0); 			
			result			: out	std_logic_vector(DWIDTH-1 downto 0)
		);
	end component;
		
	signal	pick				: std_logic_vector(DWIDTH-1 downto 0);

--	+----------------------------------------------------------------------------
-- 	|	Пересчет в дБ
--	|		
--	+----------------------------------------------------------------------------
	component decibel0 is
		generic
		(
			DWIDTH_IN      	: natural	:= 14	;     -- Разрядность входных данных
			DWIDTH_WH 	 	: natural	:= 7	;     -- Разрядность целой части результата
			DWIDTH_FR   	: natural	:= 3     	  -- Разрядность дробной части результата
		);
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			start			: in	std_logic;			-- включение
			stop			: out	std_logic;			-- выходной импульс
			ready			: out	std_logic;			-- готовность
			data			: in	std_logic_vector(DWIDTH_IN - 1 downto 0); 			
			result			: out	std_logic_vector(DWIDTH_WH + DWIDTH_FR - 1 downto 0) 	
		);
	end component;

	signal	start				: std_logic := '1';		-- включение
	signal	stop				: std_logic;			-- выходной импульс
	signal	ready				: std_logic;			-- готовность
	signal	level_dB			: std_logic_vector(DWIDTH_WH+DWIDTH_FR-1 downto 0);

--	+----------------------------------------------------------------------------
-- 	|	Управление
--	|		
--	+----------------------------------------------------------------------------

	component agc_host0 is
		generic
		(
			DWIDTH_WH 	 	: natural	:= 7	;     -- Разрядность целой части результата
			DWIDTH_FR   	: natural	:= 3    ; 	  -- Разрядность дробной части результата
			MAX_CONTROL		: natural	:= 300	;	  -- 299
			CYCLE_LOG2		: natural	:= 4		  -- число тактов valid 
		);
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			valid			: in	std_logic;			-- active reset
			data			: in	std_logic_vector(DWIDTH_WH+DWIDTH_FR-1 downto 0); 	
			control			: out	natural range 0 to MAX_CONTROL-1;		-- control (dB * 4)
			no_change		: out	std_logic;			-- устанавливается на время до слекдущего цикла.
			no_jammer		: out	std_logic			-- признак того, что нет помехи...
		);
	end component;
	
	signal	int_control			: natural range 0 to MAX_CONTROL-1;		-- control (dB * 4)
	signal	int_no_change		: std_logic;			-- устанавливается на время до слекдущего цикла.
	signal	int_no_jammer		: std_logic;		-- признак того, что нет помехи...
	
--	+----------------------------------------------------------------------------
-- 	|	Преобразование управления в управление усилителями
--	|	через таблицы	
--	+----------------------------------------------------------------------------
	
	component agc_rom0 is
		port 
		(	
			clk			: in	std_logic;
			control		: in	natural range 0 to 299;				-- control (dB * 4)
			gain		: out	std_logic_vector(15 downto 0);		-- 8.8 (dB) actual gain	
			rf			: out	std_logic_vector(1 downto 0);		-- 0-3
			ifa			: out	std_logic_vector(5 downto 0);		-- 0-63
			adc			: out	std_logic
		);
	end component;


begin


--	+----------------------------------------------------------------------------
-- 	|	Пик-детектор
--	|		
--	+----------------------------------------------------------------------------
	pick_det : pick_detector0 
		generic map
		(
			DWIDTH  	=>   DWIDTH
		)
		port map
		(
			clk			=>	clk		,	
			reset	 	=>	reset	,	
			data		=>	data	,			
			result		=>	pick		
		);
	

--	+----------------------------------------------------------------------------
-- 	|	Пересчет в дБ
--	|		
--	+----------------------------------------------------------------------------
	to_decibel : decibel0 
		generic map
		(
			DWIDTH_IN      	=>	DWIDTH      	,	     -- Разрядность входных данных
			DWIDTH_WH 	 	=>	DWIDTH_WH 	 	,	     -- Разрядность целой части результата
			DWIDTH_FR   	=>	DWIDTH_FR   			  -- Разрядность дробной части результата
		)
		port map
		(
			clk				=>	clk		,	
			reset	 		=>	reset	,	
			start			=>	start	,	
			stop			=>	stop	,	
			ready			=>	ready	,	
			data			=>	pick	,	
			result			=>	level_dB 	
		);

--	+----------------------------------------------------------------------------
-- 	|	Управление
--	|		
--	+----------------------------------------------------------------------------

	host : agc_host0 
		generic map
		(
			DWIDTH_WH 	 	=>	DWIDTH_WH 	,	  -- Разрядность целой части результата
			DWIDTH_FR   	=>	DWIDTH_FR   ,	  -- Разрядность дробной части результата
			MAX_CONTROL		=>	MAX_CONTROL	,	  -- 299
			CYCLE_LOG2		=>	CYCLE_LOG2		  -- число тактов valid 
		)
		port map
		(
			clk				=>	clk				,	
			reset	 		=>	reset	 		,	
			valid			=>	stop			,	
			data			=>	level_dB		,	
			control			=>	int_control		,	
			no_change		=>	int_no_change	,	
			no_jammer		=>	int_no_jammer	
		);

	control		<=	int_control		;	
	no_change	<=	int_no_change	;	
	no_jammer	<=	int_no_jammer	;


--	+----------------------------------------------------------------------------
-- 	|	Преобразование управления в управление усилителями
--	|	через таблицы	
--	+----------------------------------------------------------------------------
	
	roms : agc_rom0 
		port map
		(	
			clk			=>	clk			,	
			control		=>	int_control	,	-- control (dB * 4)
			gain		=>	gain		,	-- 8.8 (dB) actual gain	
			rf			=>	rf			,	-- 0-3
			ifa			=>	ifa			,	-- 0-63
			adc			=>	adc			
		);





				
end rtl;
					

