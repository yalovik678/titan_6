--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

library work;
use work.cmd_package.all;
use work.sts_package.all;
use work.lvds_package.all;

entity agc_meter_top2 is
	port
	(
		clk				: in	std_logic;								-- dsp clock
		reset	 		: in	std_logic;								-- active reset
		control			: in	work.cmd_package.t_agc_control;			-- 
		data			: in	work.lvds_package.t_adc_data_vector; 	-- ADC			
		start_spi		: out	std_logic;								-- импульс обновления данных!!!
		result			: out 	work.sts_package.t_agc_status
	);
end entity agc_meter_top2;

		
architecture rtl of agc_meter_top2 is

--	+----------------------------------------------------------------------------
-- 	|	Константы 
--	|		
--	+----------------------------------------------------------------------------
	constant DWIDTH      	: natural	:= 16	;							-- Разрядность входных данных
	
	constant MAX_LOG		: real 		:= 20.0 * log10 (2.0 ** real(DWIDTH - 1));
	constant DWIDTH_WH 	 	: natural 	:= integer (log2(MAX_LOG)) + 1;     -- Разрядность целой части результата
	-- constant DWIDTH_WH 	 	: natural	:= 7	;    					
	constant DWIDTH_FR   	: natural 	:= 2   ; 	  						-- Разрядность дробной части результата

	constant MAX_CONTROL	: natural 	:= 300	;	  -- 299
	
	constant STAGES			: natural 	:= 20	;	
	constant STAGES_LOG2	: natural 	:= 5	;

	subtype t_data 		is std_logic_vector(DWIDTH-1 downto 0);
	type t_data_vector 	is array (natural range <>) of t_data;

--	+----------------------------------------------------------------------------
-- 	|	Общие функции ....... 
--	|					расчет децибел
--	+----------------------------------------------------------------------------
	
	component decibel0 is
		generic
		(
			DWIDTH_IN      	: natural	:= 14	;     -- Разрядность входных данных
			DWIDTH_WH 	 	: natural	:= 7	;     -- Разрядность целой части результата
			DWIDTH_FR   	: natural	:= 3     	  -- Разрядность дробной части результата
		);
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			start			: in	std_logic;			-- включение
			stop			: out	std_logic;			-- выходной импульс
			ready			: out	std_logic;			-- готовность
			data			: in	std_logic_vector(DWIDTH_IN - 1 downto 0); 			
			result			: out	std_logic_vector(DWIDTH_WH + DWIDTH_FR - 1 downto 0) 	
		);
	end component;


--	+----------------------------------------------------------------------------
-- 	|	Пик-детектор
--	|		
--	+----------------------------------------------------------------------------
	component pick_detector0 is
		generic
		(
			DWIDTH      	: natural	:= 16	  -- Разрядность входных данных
		);
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			data			: in	std_logic_vector(DWIDTH-1 downto 0); 			
			result			: out	std_logic_vector(DWIDTH-1 downto 0)
		);
	end component;
		
	signal	pick				: t_data_vector(0 to 7);

--	+----------------------------------------------------------------------------
-- 	|	Блок расчета dB для тракта статуса
--	|		
--	+----------------------------------------------------------------------------
	signal	asd_data			: t_data ;		
	signal	asd_stop			: std_logic;			
	signal	asd_result			: std_logic_vector(DWIDTH_WH + DWIDTH_FR - 1 downto 0) ;	

--	+----------------------------------------------------------------------------
-- 	|	Контроллер формирования статусного расчета 
--	|		
--	+----------------------------------------------------------------------------

	component agc_status_drv0 is
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			start			: out	std_logic;			
			stop			: in	std_logic;			
			sel				: out	natural range 0 to 7;			
			save			: out	std_logic
		);
	end component;

	signal	asd_start			: std_logic;			
	signal	asd_sel				: natural range 0 to 7;			
	signal	asd_save			: std_logic;

--	+----------------------------------------------------------------------------
-- 	|	Пространственная обработка 
--	|		SPP = SPace Processing
--	+----------------------------------------------------------------------------
	component agc_mean_par8 is
		generic
		(
			DWIDTH	 	 	: natural	:= 16	
		);
		port
		(
			clk				: in	std_logic;								-- dsp clock
			reset	 		: in	std_logic;								-- active reset
			func			: in	std_logic_vector(1 downto 0); 			-- 0 - master; 1 - mean; 2 - max
			master			: in	std_logic_vector(2 downto 0); 			-- select channel in master mode
			data_a			: in	std_logic_vector(DWIDTH-1 downto 0); 	-- 
			data_b			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			data_c			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			data_d			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			data_e			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			data_f			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			data_g			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			data_h			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			result 			: out	std_logic_vector(DWIDTH-1 downto 0)
		);
	end component;

	signal	spp_data			: t_data ;		
--	+----------------------------------------------------------------------------
-- 	|	Временная обработка 
--	|		TIP = TIme Processing	
--	+----------------------------------------------------------------------------
		
	component agc_mean_serial0 is
		generic
		(
			DWIDTH	 	 	: natural	:= 16	; 
			STAGES			: natural	:= 10	;	
			STAGES_LOG2		: natural	:= 4	
		);
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			div				: in	std_logic_vector(STAGES_LOG2-1 downto 0); 	
			max_n_mean		: in	std_logic;
			valid_i			: in	std_logic;			
			data_i 			: in	std_logic_vector(DWIDTH-1 downto 0); 	
			valid_o			: out	std_logic;			
			data_o 			: out	std_logic_vector(DWIDTH-1 downto 0)
		);
	end component;

	signal	tip_valid			: std_logic;			
	signal	tip_data 			: t_data;

--	signal	tip_data 			: t_data;

--	+----------------------------------------------------------------------------
-- 	|	Блок расчета dB для тракта статуса
--	|		TIP = TIme Processing
--	+----------------------------------------------------------------------------
--	signal	top_data			: t_data ;		
	signal	tip_stop			: std_logic;			
	signal	tip_result			: std_logic_vector(DWIDTH_WH+DWIDTH_FR-1 downto 0) ;	

--	+----------------------------------------------------------------------------
-- 	|	Управляемый таймер обратной связи
--	|		
--	+----------------------------------------------------------------------------
	component agc_timer_drv0 is
		port
		(
			clk				: in	std_logic;			-- dsp clock
			reset	 		: in	std_logic;			-- active reset
			enable			: in	std_logic;
			div				: in	std_logic_vector(3 downto 0);
			acc_control		: out	std_logic_vector(4 downto 0);
			start			: out	std_logic
		);
	end component;

	signal	acc_control		: std_logic_vector(4 downto 0);
	signal	host_start		: std_logic;

--	+----------------------------------------------------------------------------
-- 	|	Управление
--	|		
--	+----------------------------------------------------------------------------

	component agc_host2 is
		generic
		(
			DWIDTH_WH 	 	: natural	:= 7	;     -- Разрядность целой части результата
			DWIDTH_FR   	: natural	:= 2    ; 	  -- Разрядность дробной части результата
			MAX_CONTROL		: natural	:= 300		  -- 299
		);
		port
		(
			clk				: in	std_logic;								-- dsp clock
			reset	 		: in	std_logic;								-- active reset
			reference		: in	std_logic_vector(15 downto 0);	-- опорный уровень ((dB * 4)), например  90dB * 4 = 360
			deadzone		: in	std_logic_vector(7 downto 0);		-- зона нечувствительности (dB * 4)
			valid			: in	std_logic;								-- active reset
			data			: in	std_logic_vector(DWIDTH_WH+DWIDTH_FR-1 downto 0); 	
			control			: out	natural range 0 to MAX_CONTROL-1;		-- control (dB * 4) - т.к. это содержимое таблицы!
			start_spi		: out	std_logic;
			no_change		: out	std_logic;								-- устанавливается на время до слекдущего цикла.
			no_jammer		: out	std_logic								-- признак того, что нет помехи...
		);
	end component;
	
	signal	int_control			: natural range 0 to MAX_CONTROL-1;		-- control (dB * 4)
	signal	int_start_spi		: std_logic;
	signal	int_no_change		: std_logic;			-- устанавливается на время до слекдущего цикла.
	signal	int_no_jammer		: std_logic;			-- признак того, что нет помехи...
	
	signal	delay				: std_logic_vector(3 downto 0);
	
--	+----------------------------------------------------------------------------
-- 	|	Преобразование управления в управление усилителями
--	|	через таблицы	
--	+----------------------------------------------------------------------------
	
	component agc_rom0 is
		port 
		(	
			clk			: in	std_logic;
			control		: in	natural range 0 to 299;				-- control (dB * 4)
			gain		: out	std_logic_vector(15 downto 0);		-- 8.8 (dB) actual gain	
			rf			: out	std_logic_vector(1 downto 0);		-- 0-3
			ifa			: out	std_logic_vector(5 downto 0);		-- 0-63
			adc			: out	std_logic
		);
	end component;
	
	signal	 rom_gain		: std_logic_vector(15 downto 0);	-- 8.8 (dB) actual gain	
	signal	 rom_rf			: std_logic_vector(1 downto 0);		-- 0-3
	signal	 rom_rf_n		: std_logic_vector(1 downto 0);		-- 0-3
	signal	 rom_ifa		: std_logic_vector(5 downto 0);		-- 0-63
	signal	 rom_adc		: std_logic;

--	+----------------------------------------------------------------------------
-- 	|	Выходной сигнал 
--	|		
--	+----------------------------------------------------------------------------
	signal status			: work.sts_package.t_agc_status := default_agc_status;




begin


--	+----------------------------------------------------------------------------
-- 	|	Пик-детекторы
--	|		
--	+----------------------------------------------------------------------------
	for_channel :
	for i in 0 to 7 generate
		pic_det : pick_detector0 
			generic map
			(
				DWIDTH  	=> DWIDTH
			)
			port map
			(
				clk			=>	clk					,		
				reset	 	=>	reset				,		
				data		=>	data(i)				,				
				result		=>	pick(i)
			);
	end generate;
--	
--	result	<= status;

--	+----------------------------------------------------------------------------
-- 	|	Блок расчета dB для тракта статуса
--	|		
--	+----------------------------------------------------------------------------
	asd_data		<= pick(asd_sel);

	asd_db : decibel0 
		generic map
		(
			DWIDTH_IN      	=>	DWIDTH 		,	-- Разрядность входных данных
			DWIDTH_WH 	 	=>	DWIDTH_WH  	,	-- Разрядность целой части результата
			DWIDTH_FR   	=>	DWIDTH_FR  		-- Разрядность дробной части результата
		)
		port map
		(
			clk				=>	clk				,	
			reset	 		=>	reset			,	
			start			=>	asd_start		,	
			stop			=>	asd_stop		,	
			ready			=>	open			,	
			data			=>	asd_data		,	
			result			=>	asd_result		
		);

--	+----------------------------------------------------------------------------
-- 	|	Контроллер формирования статусного расчета 
--	|		
--	+----------------------------------------------------------------------------

	asd_host : agc_status_drv0 
		port map
		(
			clk				=>	clk			,	-- dsp clock
			reset	 		=>	reset	 	,	-- active reset
			start			=>	asd_start	,	
			stop			=>	asd_stop	,	
			sel				=>	asd_sel		,			
			save			=>	asd_save		
		);
	
--	+----------------------------------------------------------------------------
-- 	|	Выходной сигнал 
--	|		(в части детекторов)
--	+----------------------------------------------------------------------------

	process (clk, reset)
	begin
		if (reset = '1') then
			status.level	<= (others => (others => '0'));
		elsif (rising_edge(clk)) then
			if asd_save = '1' then
				status.level(asd_sel) <= std_logic_vector(resize(unsigned(asd_result(DWIDTH_WH+DWIDTH_FR-1 downto DWIDTH_FR)), 8));
			end if;
		end if;
	end process;
		
		
--	+----------------------------------------------------------------------------
-- 	|	Пространственная обработка 
--	|		SPP = SPace Processing
--	+----------------------------------------------------------------------------
	spp : agc_mean_par8
		generic map
		(
			DWIDTH	 	 => DWIDTH
		)
		port map
		(
			clk				=>	clk				,	-- dsp clock
			reset	 		=>	reset	 		,	-- active reset
			func			=>	control.func	,	-- 0 - master; 1 - mean; 2 - max
			master			=>	control.master	,	-- select channel in master mode
			data_a			=>	pick(0)			,	-- 
			data_b			=>	pick(1)			,	
			data_c			=>	pick(2)			,	
			data_d			=>	pick(3)			,	
			data_e			=>	pick(4)			,	
			data_f			=>	pick(5)			,	
			data_g			=>	pick(6)			,	
			data_h			=>	pick(7)			,	
			result 			=>	spp_data
		);


--	+----------------------------------------------------------------------------
-- 	|	Временная обработка 
--	|		TIP = TIme Processing	
--	+----------------------------------------------------------------------------
		
	tip : agc_mean_serial0 
		generic map
		(
			DWIDTH	 	 	=>	DWIDTH	 	 	,	 
			STAGES			=>	STAGES			,		
			STAGES_LOG2		=>	STAGES_LOG2			
		)
		port map
		(
			clk				=>	clk					,	
			reset	 		=>	reset	 			,	
			div				=>	acc_control			,	
			max_n_mean		=>	control.max_n_mean	,	
			valid_i			=>	'1'					,	
			data_i 			=>	spp_data			,	
			valid_o			=>	tip_valid			,	
			data_o 			=>	tip_data 			
		);


--	+----------------------------------------------------------------------------
-- 	|	Блок расчета dB для тракта статуса
--	|		TIP = TIme Processing
--	+----------------------------------------------------------------------------

	tip_db : decibel0 
		generic map
		(
			DWIDTH_IN      	=>	DWIDTH 		,	-- Разрядность входных данных
			DWIDTH_WH 	 	=>	DWIDTH_WH  	,	-- Разрядность целой части результата
			DWIDTH_FR   	=>	DWIDTH_FR  		-- Разрядность дробной части результата
		)
		port map
		(
			clk				=>	clk				,	
			reset	 		=>	reset			,	
			start			=>	tip_valid		,	
			stop			=>	tip_stop		,	
			ready			=>	open			,	
			data			=>	tip_data		,	
			result			=>	tip_result		
		);


--	+----------------------------------------------------------------------------
-- 	|	Управляемый таймер обратной связи
--	|		
--	+----------------------------------------------------------------------------
	timer : agc_timer_drv0 
		port map
		(
			clk				=>	clk				,	
			reset	 		=>	reset	 		,	
			enable			=>	control.enable	,	
			div				=>	control.div		,	
			acc_control		=>	acc_control		,	
			start			=>	host_start
		);
	


--	+----------------------------------------------------------------------------
-- 	|	Управление
--	|		
--	+----------------------------------------------------------------------------

	host : agc_host2 
		generic map
		(
			DWIDTH_WH 	 	=>	DWIDTH_WH 	,	  -- Разрядность целой части результата
			DWIDTH_FR		=>	DWIDTH_FR	,	
			MAX_CONTROL		=>	MAX_CONTROL		  -- 299
		)
		port map
		(
			clk				=>	clk					,	-- dsp clock
			reset	 		=>	reset	 			,	-- active reset
			reference		=>	control.reference	,		-- опорный уровень ((dB * 4)), например  90dB * 4 = 360
			deadzone		=>	control.deadzone	,		-- зона нечувствительности (dB * 4)
			valid			=>	host_start			,	-- active reset
			data			=>	tip_result			,		
			control			=>	int_control			,	-- control (dB * 4) - т.к. это содержимое таблицы!
			start_spi		=>	int_start_spi		,	
			no_change		=>	int_no_change		,	-- устанавливается на время до слекдущего цикла.
			no_jammer		=>	int_no_jammer		-- признак того, что нет помехи...
		);

	status.no_change	<= int_no_change ;
	status.no_jammer	<= int_no_jammer ;


	process (clk, reset)
	begin
		if (reset = '1') then
			delay		<= (others => '0');
			start_spi	<= '0';
		elsif (rising_edge(clk)) then
			delay	<= delay(2 downto 0) & int_start_spi;
			if delay(0) = '1' then
				start_spi	<= '1';
			elsif delay(3) = '1' then
				start_spi	<= '0';
			end if;
		end if;
	end process;

--	+----------------------------------------------------------------------------
-- 	|	Преобразование управления в управление усилителями
--	|	через таблицы	
--	+----------------------------------------------------------------------------
	
	roms : agc_rom0 
		port map
		(	
			clk			=>	clk			,	
			control		=>	int_control	,	-- control (dB * 4)
			gain		=>	rom_gain	,	-- 8.8 (dB) actual gain	
			rf			=>	rom_rf		,	-- 0-3
			ifa			=>	rom_ifa		,	-- 0-63
			adc			=>	rom_adc			
		);		
	
	status.filter		<= rom_adc; 				-- 	: std_logic;	
	
	rom_rf_n			<= NOT rom_rf;
	
	status.rf			<= b"000000" & rom_rf_n; 		-- 	: t_char;
--	status.ifa			<= b"00" & rom_ifa; 		-- 	: t_char;

	process(rom_ifa, rom_adc, control.filter_ena)
	begin
		if control.filter_ena = '1' then
			status.ifa		<= b"00" & rom_ifa; 		-- 	: t_char;
		else
			if rom_adc = '1' then
				status.ifa		<= X"3F";
			else
				status.ifa		<= b"00" & rom_ifa; 		-- 	: t_char;
			end if;
		end if;
	end process;

	status.gain_dB		<= rom_gain; 				-- 	: t_short;
		
	result	<= status;
		
				
end rtl;
					

