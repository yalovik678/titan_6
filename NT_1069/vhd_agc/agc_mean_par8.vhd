--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

--	последовательный усреднитель

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;


-- library work;
-- use work.agc_package.all;


entity agc_mean_par8 is
	generic
	(
		DWIDTH	 	 	: natural	:= 16	
	);
	port
	(
		clk				: in	std_logic;								-- dsp clock
		reset	 		: in	std_logic;								-- active reset
		func			: in	std_logic_vector(1 downto 0); 			-- 0 - master; 1 - mean; 2 - max
		master			: in	std_logic_vector(2 downto 0); 			-- select channel in master mode
		data_a			: in	std_logic_vector(DWIDTH-1 downto 0); 	-- 
		data_b			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		data_c			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		data_d			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		data_e			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		data_f			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		data_g			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		data_h			: in	std_logic_vector(DWIDTH-1 downto 0); 	
		result 			: out	std_logic_vector(DWIDTH-1 downto 0)
	);
end entity agc_mean_par8;

		

architecture rtl of agc_mean_par8 is

--	+----------------------------------------------------------------------------
-- 	|	BASE TYPE
--	|		
--	+----------------------------------------------------------------------------
	subtype t_data is std_logic_vector(DWIDTH-1 downto 0);		
	
	type t_data_vector is array (natural range <>) of t_data; 
	
	subtype t_data_vector2 is t_data_vector(0 to 1);		
	subtype t_data_vector4 is t_data_vector(0 to 3);		
	subtype t_data_vector8 is t_data_vector(0 to 7);		
	
--	+----------------------------------------------------------------------------
-- 	|	MODE TYPE
--	|		
--	+----------------------------------------------------------------------------
	type t_mode 	is (MASTER2, MEAN2, MAX2, RESERVED);

	signal	mode			: t_mode;
--	+----------------------------------------------------------------------------
-- 	|	ACC2 CONTEXT
--	|		
--	+----------------------------------------------------------------------------
	-- 	расчет нового значения в зависимости от позиции и режима работы
	--	на входе phase игнорируется
	
	function acc2(input0, input1 : t_data; sel : std_logic; mode : t_mode) return t_data is
		subtype t_edata is std_logic_vector(DWIDTH downto 0);		
		alias 	arg_a is  input0;
		alias 	arg_b is  input1;
		variable  data_a 	: t_edata;
		variable  data_b 	: t_edata;
		variable  summa 	: t_edata;
		variable  result 	: t_data;
	begin
		case mode is
			when MEAN2 	=> 		
			-- среднее из двух
				data_a 	:= std_logic_vector(resize(signed(arg_a), DWIDTH+1)); 
				data_b 	:= std_logic_vector(resize(signed(arg_b), DWIDTH+1)); 
				summa 	:= std_logic_vector(signed(data_a) + signed(data_b));
				result 	:= summa(DWIDTH downto 1);
			when MAX2 	=> 	
			-- вычисляем максимальное из двух
				if signed(input1) > signed(input0) then
					result := input1;
				else
					result := input0;
				end if;
			when others => 		
			-- мастер режима
				if sel = '1' then
					result := input1;
				else
					result := input0;
				end if;
		end case;
		return result;
	end function acc2;

--	+----------------------------------------------------------------------------
-- 	|	Каскады
--	|		
--	+----------------------------------------------------------------------------
	signal data 				: t_data_vector8 ;
	signal stage0 				: t_data_vector4 ;
	signal stage1 				: t_data_vector2 ;
	signal stage2 				: t_data ;



begin

--	+----------------------------------------------------------------------------
-- 	|	Управление и вход
--	|		
--	+----------------------------------------------------------------------------
	mode	<= t_mode'val(to_integer(unsigned(func)));
	
--	+----------------------------------------------------------------------------
-- 	|	Вход
--	|		
--	+----------------------------------------------------------------------------

	data(0) <= data_a; 
	data(1) <= data_b; 
	data(2) <= data_c; 
	data(3) <= data_d; 
	data(4) <= data_e; 
	data(5) <= data_f; 
	data(6) <= data_g; 
	data(7) <= data_h; 
	
--	+----------------------------------------------------------------------------
-- 	|	Каскад 0 
--	|		
--	+----------------------------------------------------------------------------

	for_stage0 :
	for i in 0 to 3 generate
		process (clk, reset)
		begin
			if (reset = '1') then
				stage0(i)	<= (others => '0');
			elsif (rising_edge(clk)) then
				stage0(i)	<= acc2(data(2*i), data(2*i + 1), master(0), mode);
			end if;
		end process;
	end generate;
	

--	+----------------------------------------------------------------------------
-- 	|	Каскад 1 
--	|		
--	+----------------------------------------------------------------------------

	for_stage1 :
	for i in 0 to 1 generate
		process (clk, reset)
		begin
			if (reset = '1') then
				stage1(i)	<= (others => '0');
			elsif (rising_edge(clk)) then
				stage1(i)	<= acc2(stage0(2*i), stage0(2*i + 1), master(1), mode);
			end if;
		end process;
	end generate;

--	+----------------------------------------------------------------------------
-- 	|	Каскад 2 
--	|		
--	+----------------------------------------------------------------------------

	process (clk, reset)
	begin
		if (reset = '1') then
			stage2	<= (others => '0');
		elsif (rising_edge(clk)) then
			stage2	<= acc2(stage1(0), stage1(1), master(2), mode);
		end if;
	end process;

	result	<= stage2;



end rtl;
					

