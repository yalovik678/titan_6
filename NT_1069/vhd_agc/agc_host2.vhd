--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------
--	rev 2.	Переменный REF и зона нечувствительности	

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

entity agc_host2 is
	generic
	(
		DWIDTH_WH 	 	: natural	:= 7	;     -- Разрядность целой части результата
		DWIDTH_FR   	: natural	:= 2    ; 	  -- Разрядность дробной части результата
		MAX_CONTROL		: natural	:= 300		  -- 299
	);
	port
	(
		clk				: in	std_logic;								-- dsp clock
		reset	 		: in	std_logic;								-- active reset
		reference		: in	std_logic_vector(15 downto 0);			-- опорный уровень ((dB * 4)), например  90dB * 4 = 360
		deadzone		: in	std_logic_vector(7 downto 0);			-- зона нечувствительности (dB * 4)
		valid			: in	std_logic;								-- active reset
		data			: in	std_logic_vector(DWIDTH_WH+DWIDTH_FR-1 downto 0); 	
		control			: out	natural range 0 to MAX_CONTROL-1;		-- control (dB * 4) - т.к. это содержимое таблицы!
		start_spi		: out	std_logic;
		no_change		: out	std_logic;								-- устанавливается на время до слекдущего цикла.
		no_jammer		: out	std_logic								-- признак того, что нет помехи...
	);
end entity agc_host2;

		

architecture rtl of agc_host2 is

	attribute mark_debug		: string;

	constant  DWIDTH	: natural := DWIDTH_WH + DWIDTH_FR + 1; -- 7 + 2 + 1 = 10
	
	subtype  t_data 	is std_logic_vector(DWIDTH-1 downto 0); 
	subtype  t_whole 	is std_logic_vector(DWIDTH_WH-1 downto 0); 
		
	constant MAX_STEP	: natural := 20; -- 10dB * 4			
--	+----------------------------------------------------------------------------
-- 	|	Линия задержки valid
--	|		
--	+----------------------------------------------------------------------------
	constant PIPE				: natural := 5;

	signal pipeline				: std_logic_vector(PIPE-1 downto 0);
	signal valid_acc			: std_logic;
	
--	+----------------------------------------------------------------------------
-- 	|	Текущий уровень в dB (целая часть)
--	|	
--	+----------------------------------------------------------------------------
	signal	whole				: t_whole;
	
--	+----------------------------------------------------------------------------
-- 	|	STAGE 0
--	|	abs(REF-DET)	
--	+----------------------------------------------------------------------------
	signal 	ref					: t_data;
	signal 	det					: t_data;
	----------------------------------------
	signal 	mism				: t_data;
	signal 	mism_magn			: t_data;
	signal 	mism_sign			: std_logic;

--	+----------------------------------------------------------------------------
-- 	|	STAGE 1
--	|	deadzone
--	+----------------------------------------------------------------------------
	signal 	dz					: t_data;
	signal 	mism_dz				: t_data;

--	+----------------------------------------------------------------------------
-- 	|	STAGE 2
--	|		MAX LIMIT & sign
--	+----------------------------------------------------------------------------
	signal 	mism_dz_limit		: t_data;
	signal 	step				: t_data;
	
	signal 	step_int			: integer range -MAX_STEP to MAX_STEP;
--	+----------------------------------------------------------------------------
-- 	|	STAGE 3
--	|		 ACC - Принятие решения
--	+----------------------------------------------------------------------------
	signal	gain_control		: natural range 0 to MAX_CONTROL-1;
	signal	gain_control_bk		: natural range 0 to MAX_CONTROL-1;

	
-- debug
	attribute mark_debug of	valid_acc				: signal is "true";
	attribute mark_debug of	whole					: signal is "true";
	attribute mark_debug of	ref						: signal is "true";
	attribute mark_debug of	det						: signal is "true";
	attribute mark_debug of	mism					: signal is "true";
	attribute mark_debug of	mism_magn				: signal is "true";
	attribute mark_debug of	mism_sign				: signal is "true";
	attribute mark_debug of	dz						: signal is "true";
	attribute mark_debug of	mism_dz					: signal is "true";
	attribute mark_debug of	mism_dz_limit			: signal is "true";
	attribute mark_debug of	step					: signal is "true";
	attribute mark_debug of	gain_control			: signal is "true";
	attribute mark_debug of	gain_control_bk			: signal is "true";
	
	


begin

--	+----------------------------------------------------------------------------
-- 	|	Линия задержки valid
--	|		
--	+----------------------------------------------------------------------------

	process (clk, reset)
	begin
		if (reset = '1') then
			pipeline	<= (others => '0');
		elsif (rising_edge(clk)) then
			pipeline	<= pipeline(PIPE-2 downto 0) & valid;
		end if;
	end process;
	
	valid_acc	<= pipeline(PIPE-1);

--	+----------------------------------------------------------------------------
-- 	|	Текущий уровень в dB (целая часть)
--	|	
--	+----------------------------------------------------------------------------
	whole	<= data(DWIDTH_WH+DWIDTH_FR-1 downto DWIDTH_FR);

--	+----------------------------------------------------------------------------
-- 	|	STAGE 0
--	|	abs(REF-DET)	
--	+----------------------------------------------------------------------------
	ref		<= reference(DWIDTH-1 downto 0);
	
	det		<= std_logic_vector(resize(unsigned(data), DWIDTH));
	
	mism	<= std_logic_vector(signed(ref) - signed(det));
	

	process (clk, reset)
	begin
		if (reset = '1') then
			mism_magn	<= (others => '0');
			mism_sign	<= '0';
		elsif (rising_edge(clk)) then
			if pipeline(0) = '1' then 
			--	mism_magn	<= std_logic_vector(abs(signed(mism)));
				mism_magn	<= std_logic_vector(shift_right(abs(signed(mism)), 2));
				mism_sign	<= mism(DWIDTH-1);
			end if;
		end if;
	end process;
	
--	+----------------------------------------------------------------------------
-- 	|	STAGE 1
--	|	deadzone
--	+----------------------------------------------------------------------------
	dz	<= std_logic_vector(resize(unsigned(deadzone), DWIDTH));

	process (clk, reset)
	begin
		if (reset = '1') then
			mism_dz	<= (others => '0');
		elsif (rising_edge(clk)) then
			if pipeline(1) = '1' then 
				if unsigned(mism_magn) > unsigned(dz) then
					mism_dz <= std_logic_vector(unsigned(mism_magn) - unsigned(dz));
				else
					mism_dz	<= (others => '0');
				end if;
			end if;
		end if;
	end process;


--	+----------------------------------------------------------------------------
-- 	|	STAGE 2
--	|		MAX LIMIT & sign
--	+----------------------------------------------------------------------------

	process(mism_dz)
	begin
		if unsigned(mism_dz) > to_unsigned(MAX_STEP,  DWIDTH) then
			mism_dz_limit	<= std_logic_vector(to_unsigned(MAX_STEP,  DWIDTH));
		else
			mism_dz_limit	<= mism_dz;
		end if;
	end process;

	process (clk, reset)
	begin
		if (reset = '1') then
			step	<= (others => '0');
		elsif (rising_edge(clk)) then
			if pipeline(2) = '1' then 
				if mism_sign = '1' then
					step 	<= std_logic_vector(-signed(mism_dz_limit));
				else
					step	<= mism_dz_limit;
				end if;
			end if;
		end if;
	end process;

	step_int	<= to_integer(signed(step));


--	+----------------------------------------------------------------------------
-- 	|	STAGE 3
--	|		 ACC - Принятие решения
--	+----------------------------------------------------------------------------

	process (clk, reset)
		variable	temp 	: integer ;	
	begin
		if (reset = '1') then
			gain_control	<= 0;
			gain_control_bk	<= 0;
			no_change		<= '0';
			start_spi		<= '0';
			
		elsif (rising_edge(clk)) then
			temp :=  gain_control;
			
			start_spi <= '0';
			
			if (valid_acc = '1') then
				temp := temp + step_int;

				if temp < 0 then
					temp := 0;
				end if;

				if temp > MAX_CONTROL-1 then
					temp := MAX_CONTROL-1;
				end if;
				
				gain_control		<= temp;
				gain_control_bk		<= gain_control;
				
				if (gain_control = gain_control_bk) then
					no_change	<= '1';
				else
					no_change	<= '0';
					start_spi 	<= '1';
				end if;
				
			end if;
		end if;
	end process;

	control	<= gain_control;


	process (clk, reset)
	begin
		if (reset = '1') then
			no_jammer		<= '0';
		elsif (rising_edge(clk)) then
		-- порог тревожности нужно будет аккуратно подобрать !!!
			if (gain_control = (MAX_CONTROL-1)) AND (unsigned(whole) < to_unsigned(70, DWIDTH_WH)) then
				no_jammer		<= '1';
			else
				no_jammer		<= '0';
			end if;
		end if;
	end process;
	
	
	
				
end rtl;
					

