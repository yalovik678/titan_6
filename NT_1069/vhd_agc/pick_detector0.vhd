--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.numeric_bit.all;
use ieee.math_real.all;
use ieee.math_complex.all;

entity pick_detector0 is
	generic
	(
		DWIDTH      	: natural	:= 16	  -- Разрядность входных данных
	);
	port
	(
		clk				: in	std_logic;			-- dsp clock
		reset	 		: in	std_logic;			-- active reset
		data			: in	std_logic_vector(DWIDTH-1 downto 0); 			
		result			: out	std_logic_vector(DWIDTH-1 downto 0)
	);
end entity pick_detector0;

		

architecture rtl of pick_detector0 is

--	+----------------------------------------------------------------------------
-- 	|	Входной регистр
--	|		
--	+----------------------------------------------------------------------------
	subtype  t_data is std_logic_vector(DWIDTH-1 downto 0); 


	constant size		: natural := 32;
	type t_data_vector is array (0 to size-1) of t_data; 

	function init_data_vector return t_data_vector is
		variable	max_value	: real;
		variable	step		: real;
		variable	d 			: real;
		variable	temp 		: t_data_vector;
	begin
		max_value := 2.0**(DWIDTH-2);
		step := max_value ** (1.0/real(size));
		for i in 0 to size-1 loop
			d := step**real(i);
			temp(i) := std_logic_vector(to_signed(integer(d), DWIDTH)); 
		end loop;
		return temp;
	end function init_data_vector;
	
	constant decrement_vector 	: t_data_vector := init_data_vector;

	signal	rg_data				: t_data;	
	signal	abs_data			: t_data;	

--	+----------------------------------------------------------------------------
-- 	|	Измеритель пика
--	|		
--	+----------------------------------------------------------------------------
	signal	pick_data			: t_data;	

--	+----------------------------------------------------------------------------
-- 	|	Timer
--	|		
--	+----------------------------------------------------------------------------
	signal	timer_clear			: std_logic;
	signal	timer				: natural range 0 to 9;	
	signal	decrement			: t_data;
	
	signal 	counter				: natural range 0 to size-1;


begin

--	+----------------------------------------------------------------------------
-- 	|	Входной регистр
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			rg_data		<= (others => '0');
			abs_data	<= (others => '0');
		elsif (rising_edge(clk)) then
			rg_data		<= data;
			abs_data	<= std_logic_vector(abs(signed(rg_data)));
		end if;
	end process;

--	+----------------------------------------------------------------------------
-- 	|	Измеритель пика
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			timer_clear	<= '0';
			pick_data	<= (others => '0');
		elsif (rising_edge(clk)) then
			timer_clear	<= '0';
			if signed(abs_data) > signed(pick_data) then
				pick_data	<= abs_data; 
					timer_clear	<= '1';
			elsif signed(pick_data) > signed(decrement) then
				pick_data	<= std_logic_vector(signed(pick_data) -  signed(decrement)); 
			else	
				pick_data	<= (others => '0');
			end if;
		end if;
	end process;

	result	<= pick_data;

--	+----------------------------------------------------------------------------
-- 	|	Timer
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			timer		<= 0;
			counter		<= 0;
		--	decrement	<= (others => '0');
		elsif (rising_edge(clk)) then
			if timer_clear = '1' then
				timer		<= 0;
				counter 	<= 0;
			--	decrement 	<= std_logic_vector(to_signed(1, DWIDTH));
			elsif timer = 4 then
				timer		<= 0;
				if counter < size-1 then
					counter <= counter + 1;
				end if;
			--	if signed(decrement) /= to_signed(2**(DWIDTH-4), DWIDTH) then
			--		decrement 	<= std_logic_vector(shift_left(signed(decrement), 1));
			--	end if;	
			else	
				timer	<= timer + 1;
			end if;
		end if;
	end process;



	process (clk, reset)
	begin
		if (reset = '1') then
			decrement	<= (others => '0');
		elsif (rising_edge(clk)) then
			decrement 	<= decrement_vector(counter);
		end if;
	end process;


				
end rtl;
