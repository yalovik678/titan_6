--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

library work;
use work.sts_package.all;
use work.lvds_package.all;

entity agc_meter_top0 is
	port
	(
		clk				: in	std_logic;								-- dsp clock
		reset	 		: in	std_logic;								-- active reset
		data			: in	work.lvds_package.t_adc_data_vector; 	-- ADC			
		result			: out 	work.sts_package.t_agc_status
	);
end entity agc_meter_top0;

		
architecture rtl of agc_meter_top0 is

--	+----------------------------------------------------------------------------
-- 	|	Измерительные каналы
--	|		
--	+----------------------------------------------------------------------------
	component agc_meter_channel0 is
		port
		(
			clk				: in	std_logic;							-- dsp clock
			reset	 		: in	std_logic;							-- active reset
			data			: in	std_logic_vector(15 downto 0); 		-- ADC			
			level			: out	std_logic_vector(7 downto 0)
		);
	end component;

--	+----------------------------------------------------------------------------
-- 	|	Выходной сигнал 
--	|		
--	+----------------------------------------------------------------------------
	signal status			: work.sts_package.t_agc_status := default_agc_status;
	

begin


--	+----------------------------------------------------------------------------
-- 	|	Каналы
--	|		
--	+----------------------------------------------------------------------------
	
	for_channel :
	for i in 0 to 7 generate
	
		meter_channel : agc_meter_channel0 
			port map
			(
				clk			=>	clk					,	
				reset	 	=>	reset				,	
				data		=>	data(i)				,	
				level		=>	status.level(i)
			);

	end generate;
	
	result	<= status;


				
end rtl;
					

