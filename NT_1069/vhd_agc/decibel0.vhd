--   +--------------------------------------------------------------------------------------------
--   |           __________     _______
--   |          /  ____/  /    /  ____/  
--   |         /  /   /  /     \  \          
--   |        /  /   /  /_______\  \
--   |       /__/   /_______/______/           
--   |                               
--   |      Romanov A.V.  2o21-2o22 NTLab   
--   +--------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;

--	Dmax_log    	: natural: = 20*log10(2^(WIDTH-1));
--	DWIDTH_LOG  	: natural: = double(int32(log2(max_log))+1);     % Разрядность целой части
--	DWIDTH_OUT  	: natural: = WIDTH_LOG + WIDTH_FR;

entity decibel0 is
	generic
	(
		DWIDTH_IN      	: natural	:= 14	;     -- Разрядность входных данных
		DWIDTH_WH 	 	: natural	:= 7	;     -- Разрядность целой части результата
		DWIDTH_FR   	: natural	:= 3     	  -- Разрядность дробной части результата
	);
	port
	(
		clk				: in	std_logic;			-- dsp clock
		reset	 		: in	std_logic;			-- active reset
		start			: in	std_logic;			-- включение
		stop			: out	std_logic;			-- выходной импульс
		ready			: out	std_logic;			-- готовность
		data			: in	std_logic_vector(DWIDTH_IN - 1 downto 0); 			
		result			: out	std_logic_vector(DWIDTH_WH + DWIDTH_FR - 1 downto 0) 	
	);
end entity decibel0;

		

architecture rtl of decibel0 is

--	+----------------------------------------------------------------------------
-- 	|	Функция расчета коэффициентов
--	|		
--	+----------------------------------------------------------------------------
	
	constant DWIDTH_OUT			: natural	:= DWIDTH_WH + DWIDTH_FR;
	
	subtype  t_data is std_logic_vector(DWIDTH_IN - 1 downto 0);
	
	constant CWIDTH_WH 	 		: natural	:= 24	;     -- Разрядность целой части результата
	constant CWIDTH_FR   		: natural	:= 8	;     -- Разрядность дробной части результата
	constant CWIDTH				: natural	:= CWIDTH_WH + CWIDTH_FR;

	subtype  t_coeff is std_logic_vector(CWIDTH - 1 downto 0);
	
	type t_coeff_vector is array (0 to DWIDTH_OUT - 1) of t_coeff;
	
	function init_coeff_vector return t_coeff_vector is
		variable temp		: real;
		variable result 	: t_coeff_vector ;	
	begin
		for i in 0 to DWIDTH_OUT - 1 loop
		--	coeff(i) = 10^((2^(i-WIDTH_FR))/20);
			temp := 10.0**(2.0**real(i - DWIDTH_FR)/20.0);
			temp := temp * 2.0**real(CWIDTH_FR);
			result(i) := std_logic_vector(to_unsigned(integer(temp), CWIDTH));
		end loop;
		return result;
	end init_coeff_vector;

	function init1 return t_coeff is
		variable temp		: real;
		variable result 	: t_coeff ;	
	begin
			temp := 1.0 * 2.0**real(CWIDTH_FR);
			result := std_logic_vector(to_unsigned(integer(temp), CWIDTH));
		return result;
	end init1;

--	function whole_part(arg : t_coeff) return t_data is
--		variable temp		: t_data;
--	begin
--		temp := arg(DWIDTH_IN + CWIDTH_FR - 1 downto CWIDTH_FR);
--		return temp;
--	end whole_part;
	
	signal	coeff_vector		: t_coeff_vector := init_coeff_vector;
	signal	coeff1				: t_coeff := init1;
	
--	+----------------------------------------------------------------------------
-- 	|	Input register
--	|		
--	+----------------------------------------------------------------------------
	signal 	data_load			: std_logic;
	signal 	data_rg				: std_logic_vector(DWIDTH_IN-1 downto 0); 	
	signal 	data_rg_ext			: t_coeff; 		
--	+----------------------------------------------------------------------------
-- 	|	stage
--	|		
--	+----------------------------------------------------------------------------
	signal 	stage				: t_coeff; 	
	signal 	stage_set			: std_logic; 	
	signal 	stage_add			: std_logic; 	

--	+----------------------------------------------------------------------------
-- 	|	mult & Crop
--	|		
--	+----------------------------------------------------------------------------
	signal 	mult				: std_logic_vector(2*CWIDTH - 1 downto 0); 	
	signal 	mult_crop			: t_coeff;
--	signal	level				: t_data;
	signal	level				: t_coeff;

--	+----------------------------------------------------------------------------
-- 	|	Comparer
--	|		
--	+----------------------------------------------------------------------------
	signal 	mult_less_input		: std_logic;
	
--	+----------------------------------------------------------------------------
-- 	|	Pre Result Register
--	|		
--	+----------------------------------------------------------------------------
	signal 	out_bits			: std_logic_vector(DWIDTH_OUT - 1 downto 0); 	
	signal 	out_bits_load		: std_logic;
	
	
--	+----------------------------------------------------------------------------
-- 	|	Output Register
--	|		
--	+----------------------------------------------------------------------------
	signal 	result_load			: std_logic;
	
--	+----------------------------------------------------------------------------
-- 	|	phase Selector (Counter)
--	|		
--	+----------------------------------------------------------------------------
	signal 	phase				: natural range 0 to DWIDTH_OUT-1; 	-- указатель фазы
	signal 	phase_init			: std_logic;
	signal 	phase_dec			: std_logic;
	signal 	phase_stop			: std_logic;
	
--	+----------------------------------------------------------------------------
-- 	|	Machine 
--	|		
--	+----------------------------------------------------------------------------
	type t_machine 	is  (
							s_idle, 
							s_pause,
							s_save,
							s_exit,
							s_stop
						);
								
	attribute syn_encoding 	: string;
	attribute syn_encoding of t_machine 	: type is "safe";      
	signal host_d   		: t_machine;
	signal host_q   		: t_machine;


begin
--	+----------------------------------------------------------------------------
-- 	|	Input register
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			data_rg	<= (others => '0');
		elsif (rising_edge(clk)) then
			if (data_load = '1') then
				data_rg	<= data;
			end if;
		end if;
	end process;

	data_rg_ext	<= std_logic_vector(shift_left(resize(unsigned(data_rg), CWIDTH), CWIDTH_FR));

--	+----------------------------------------------------------------------------
-- 	|	stage
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			stage	<= (others => '0');
		elsif (rising_edge(clk)) then
			if (stage_set = '1') then
				stage	<= coeff1;
			elsif (stage_add = '1') then
				stage	<= std_logic_vector(unsigned(mult_crop));
			else
				stage	<= stage;
			end if;
		end if;
	end process;

--	+----------------------------------------------------------------------------
-- 	|	mult & Crop
--	|		
--	+----------------------------------------------------------------------------
	-- 24.8 x 24.8 = 48.16 	

	process (clk, reset)
	begin
		if (reset = '1') then
			mult	<= (others => '0');
		elsif (rising_edge(clk)) then
			mult	<= std_logic_vector(unsigned(stage) * unsigned(coeff_vector(phase)));
		end if;
	end process;

	-- 48.16  ->  24.8 
	mult_crop	<= mult(CWIDTH_FR + CWIDTH - 1 downto CWIDTH_FR);	-- 

--	level	<= whole_part(mult_crop);
	level	<= mult_crop;

--	+----------------------------------------------------------------------------
-- 	|	Comparer
--	|		
--	+----------------------------------------------------------------------------
	mult_less_input	<= '1' when (unsigned(level) < unsigned(data_rg_ext)) else '0';

--	+----------------------------------------------------------------------------
-- 	|	Pre Result Register
--	|		
--	+----------------------------------------------------------------------------

	process (clk, reset)
	begin
		if (reset = '1') then
			out_bits	<= (others => '0');
		elsif (rising_edge(clk)) then
			if (data_load = '1') then
				out_bits	<= (others => '0');
			elsif (out_bits_load = '1') then
				out_bits(phase) <= mult_less_input;
			end if;
		end if;
	end process;
	
--	+----------------------------------------------------------------------------
-- 	|	Output Register
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			result	<= (OTHERS => '0');
		elsif (rising_edge(clk)) then
			if (result_load = '1') then
				result <= out_bits;	
			end if;
		end if;
	end process;

--	+----------------------------------------------------------------------------
-- 	|	phase Selector (Counter)
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if (reset = '1') then
			phase <= 0;
		elsif (rising_edge(clk)) then
			if phase_init = '1' then
				phase <= DWIDTH_OUT-1;
			elsif phase_dec = '1' then
				phase <= phase - 1;
			end if;	
		end if;
	end process;

	phase_stop 	<= '1' when (phase = 0) else '0';

--	+----------------------------------------------------------------------------
-- 	|	Machine 
--	|		
--	+----------------------------------------------------------------------------
	process (clk, reset)
	begin
		if reset = '1' then
			host_q <= s_idle;
		elsif (rising_edge(clk)) then
			host_q <= host_d;
		end if;	
	end process;

	process (
			host_q, 
				start,
				mult_less_input,
				phase_stop
			)

	begin
		host_d <= host_q;

		ready			<= '0';	
		stop			<= '0';	
		data_load		<= '0';	
		stage_set		<= '0'; 	
		stage_add		<= '0'; 	
	 	out_bits_load	<= '0';
	 	result_load		<= '0';
	 	phase_init		<= '0';
	 	phase_dec		<= '0';
		

		case host_q is
			when s_idle =>
				data_load	<= '1';
				stage_set	<= '1';
				phase_init	<= '1';
				ready		<= '1';
				if start = '1' then
					host_d <= s_pause; 
				end if;

			when s_pause =>
				host_d <= s_save; 

			when s_save =>
				out_bits_load <= '1';
				if mult_less_input = '1' then
					stage_add <= '1';
					
				end if;
				if phase_stop = '1' then
					host_d 	<= s_exit; 
				else
					host_d 	<= s_pause; 
						phase_dec <= '1';				
				end if;
				
			when s_exit =>
				host_d 	<= s_stop; 
					result_load	<= '1';

			when s_stop =>
				host_d 	<= s_idle; 
					stop	<= '1';
			
			when others => 
				host_d 	<= s_idle; 
				
		end case;
	end process;


				
end rtl;
					


-----------------------------------------------------------------------------
-- 						MATLAB
-----------------------------------------------------------------------------
--			WIDTH       = 14;     % Разрядность АЦП
--			max_log      = 20*log10(2^(WIDTH-1));
--			WIDTH_LOG   = double(int32(log2(max_log))+1);     % Разрядность целой части
--			WIDTH_FR    = 3;                                  % Разрядность дробной части
--			WIDTH_OUT   = WIDTH_LOG + WIDTH_FR;
--			coeff       = zeros(WIDTH_LOG,1);
--			for i=1:WIDTH_OUT
--			    coeff(i) = 10^((2^(i-WIDTH_FR))/20);
--			end     
--			
--			
--			len = 8192;
--			x = (1:1:len)';
--			y2 = 20*log10(x);
--			
--			y  = zeros(len,1);
--			for i=1:len
--			    y(i) = adc2db (x(i), WIDTH_OUT, WIDTH_LOG, coeff);
--			end     
--			
--			mism = y2-y;
--			
--			figure_log = figure('Name', 'dB'); 
--			subplot(2, 1, 1);
--			plot(x, y, 'r');
--			hold on;
--			plot(x, y2, 'g');
--			subplot(2, 1, 2);
--			plot(x, mism, 'm');
--			
--			
--			
--			% WIDTH_OUT - разрядность выходного слова
--			% WIDTH_LOG - число разрядов целой части
--			% x - вход
--			function y = adc2db (x, WIDTH_OUT, WIDTH_LOG, coeff)
--			    y = 0;
--			    stage  = zeros(WIDTH_OUT+1,1);
--			    stage(1)  = 1;
--			    for i=1:WIDTH_OUT
--			        temp = stage(i)*coeff(WIDTH_OUT+1-i);
--			        if temp <= x
--			            y = y + 2^(WIDTH_LOG+1-i);
--			            stage(i+1) = temp;
--			        else
--			            stage(i+1) = stage(i);
--			        end   
--			    end     
--			end 

