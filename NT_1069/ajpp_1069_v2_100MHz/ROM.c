#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"
#include "rom.h"


int ADC_rom(int data){
  if (data>254)
    return 1;
  else 
    return 0;
}


uint8_t rf_ifa_rom(int data){
  int* coff_ifa;
  int* coff_rf;
  coff_rf = coff_rf_rom;
  coff_ifa = coff_ifa_rom; 
  int a = coff_ifa[data];
  int b = coff_rf[data];
  // int c = coff_ifa[data]<<2 | 0x3&(~coff_rf[data]);
  int a6 = coff_ifa[data] & 0x1;
  int a5 = (coff_ifa[data] & 0x2) >>1;
  int a4 = (coff_ifa[data] & 0x4) >>2;
  int a3 = (coff_ifa[data] & 0x8) >>3;
  int a2 = (coff_ifa[data] & 0x10) >>4;
  int a1 = (coff_ifa[data] & 0x20) >>5;
  
  //int key_ifa = a6 | a2<<1 | a1<<2 | a5<<3 | a3<<4 | a4<<5;
  int key_ifa = a4 | a5<<1 | a2<<2 | a1<<3 | a3<<4 | a6<<5;
  
  
  
  
  return key_ifa | (0x3&(~coff_rf[data])<<2);

}


/*int rf_rom(int data){
  int* coff_rf;
  coff_rf = coff_rf_rom;
  return coff_rf[data];

}*/



