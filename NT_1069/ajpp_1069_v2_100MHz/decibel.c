#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"

//uint8_t DWIDTH = 16;
// uint8_t size = 32;
  int DWIDTH_IN = 14;
  int DWIDTH_WH = 7;
  int DWIDTH_FR = 3;

  int CWIDTH_WH = 24;
  int CWIDTH_FR = 8; 
  int CWIDTH = 32;
  
int coeff_vector[10] ;
int state;


void Init1(void){
   double temp;
   temp = 1 * pow(2, CWIDTH_FR);
   state = (int) temp ;   
}

void Init_coeff_vector(void){
   double temp;   
   for(int i=0; i<10; i++){
     temp =  pow(2, (i - DWIDTH_FR));
     temp = pow(10,(temp/20)); 
     temp = temp * pow(2, CWIDTH_FR);
     coeff_vector[i]= (int) temp ;
   }    
    Init1();
} 
 
uint16_t Machine(uint16_t data){ 

  uint32_t   level; 
  uint16_t out_data = 0;
  Init1();
  
  for (int i=9; i>=0; i--){
    //level = 0xA5;
    //level = (unsigned) state ;
    //level =  (unsigned)(coeff_vector[i]);
    level = state * (unsigned)(coeff_vector[i]);
    level = (level>>8) & 0xFFFFFFFF ; 
   
    if ((unsigned) level<(unsigned) data){
      state = level;
      out_data = out_data | (0x1 << i) ;       
    }  
  }
   return  out_data;
} 




