#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"
#include "rom.h"


uint8_t rf_ifa_rom(int data){
  int coff_ifa;
  int coff_rf;

  //coff_ifa = coff_ifa_rom; 
  
   if (data <= 37) coff_rf=0;
   if (data > 37 & data <= 55) coff_rf=1;
   if (data > 55 & data <= 65) coff_rf=2;
   if (data > 65) coff_rf=3;
   
   if (data <= 26) coff_ifa=0;
   if (data > 27 & data <= 35) coff_ifa=2;
   if (data > 35 & data <= 52) coff_ifa=4;
   if (data > 52 & data <= 83) coff_ifa=5;
   if (data > 83 & data <= 94) coff_ifa=8;
   if (data > 94 & data <= 109) coff_ifa=12;
   
   if (data > 109 & data <= 124) coff_ifa=16;
   if (data > 124 & data <= 141) coff_ifa=23;
   if (data > 141 & data <= 156) coff_ifa=27;
   if (data > 156 & data <= 170) coff_ifa=33;
   if (data > 170 & data <= 182) coff_ifa=37;
   if (data > 182 & data <= 199) coff_ifa=43;
   if (data > 199 & data <= 214) coff_ifa=47;
   if (data > 214 & data <= 229) coff_ifa=53;
   if (data > 229 & data <= 280) coff_ifa=57;
   if (data > 280 & data <= 289) coff_ifa=61;
   if (data > 289 & data <= 299) coff_ifa=63;


   
  int a6 = coff_ifa & 0x1;
  int a5 = (coff_ifa & 0x2) >>1;
  int a4 = (coff_ifa & 0x4) >>2;
  int a3 = (coff_ifa & 0x8) >>3;
  int a2 = (coff_ifa & 0x10) >>4;
  int a1 = (coff_ifa & 0x20) >>5;
  
  int key_ifa = a4 | a5<<1 | a2<<2 | a1<<3 | a3<<4 | a6<<5;  
  
  
  return key_ifa | (0x3&(~coff_rf)<<2);

}




