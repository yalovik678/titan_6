%% TESTBENCH FOR AGC
clear all; 
clc; 
close all;

%% system configuration
R               = 50;                       % 50 �� - ������������
ADC.Fclk        = 100E6;                    % ������� ������� ������
ADC.Vpp         = 1;                        % 1����� ��� �� ���
ADC.NBits       = 14;                       % �������� ���
ADC.LSB         = ADC.Vpp / 2^ADC.NBits;    % LSB ���

len             = 2^14;

fragment.n      = 2^3;
fragment.len    = len / fragment.n;
fragment.min    = -130;
fragment.max    = 5;

jammer.magn     = 1;

aperture        = int32(1e-6 * ADC.Fclk);

display.jammer  = 0;
display.digital = 1;

% ��� ������ ���� 75 ��. 
% ��� ��������� ����� �������� ������������ �������� ��� ��������� ������.
% �� �������  = +/- 20 LSB � ����. ��� ���� ������� ��������� �� ������ �������� 7 ������ 
% ������������� 3 �����.
% ���������� ������ ���� ����� ��������, ����� �������� ������� ������� �� 6000 �� 8000. 
% ����� ����� �������� ����� �����, ������� 1 �� � ��������, �� 3-4�� ��� ����� � ����. 
% ���� �������� ���������� ���� 10000, �� ����� ������� ����, ��������, ����� -6...-10 �� �� ���.
% ���� �������� ���������� ���� 10, ��� ����� < 75 ��, ������ ������� ������� ���, 
% ������ ����� +6...+10 �� �� ���, ��� ���� ����������� ����� �� ������� ������ 75. 
% 


%%-------------------------------------------------------------------------------
%%  SIGNAL GENERATION
%%-------------------------------------------------------------------------------
jammer.wave     = jammer_wave_generation(len, jammer.magn); 
jammer.db       = jammer_db_generation(fragment); 
jammer.wave     = jammer.wave .* dbm2rms(jammer.db, R);
jammer.dBm      = rms2dbm (jammer.wave, R);

%jammer.gained  = gain(jammer.wave, 45);
%digital.adc = analog2digital (jammer.gained, ADC);
%digital.det = detector(digital.adc, aperture);
%digital.det_dB = factor2dB(digital.det);

% AGC
digital.agc     = 75 * ones(len,1);
jammer.gained   = zeros(len, 1);
digital.adc     = zeros(len, 1);
digital.det     = zeros(len, 1);
digital.det_dB  = zeros(len, 1);
digital.present = zeros(len, 1);
digital.change  = zeros(len, 1);

cnt = 1;

for i=2:len
    if cnt==50
        cnt = 1;
        agc_ena = true;
    else
        cnt = cnt + 1;
        agc_ena = false;
    end
    
    jammer.gained(i) = gain(jammer.wave(i), digital.agc(i-1)); 
    digital.adc(i)   = analog2digital (jammer.gained(i), ADC);
    
       %mean(abs(temp(i:i+aperture)));
    
    
    if agc_ena == true
        digital.det(i)    = mean(abs(digital.adc(i-49:i)));
        digital.det_dB(i) = factor2dB(digital.det(i));
        
        if (digital.det_dB(i) < 20) && (digital.agc(i) == 75)
            digital.present(i) = 0;
        else
            digital.present(i) = 1;
        end 
        
        
        digital.change(i) = 1;
        
        if digital.det_dB(i) < 0 
            digital.agc(i) = digital.agc(i-1) + 30;
            if digital.agc(i) > 75 
                digital.agc(i) = 75;
            end    
        elseif digital.det_dB(i) < 10 
            digital.agc(i) = digital.agc(i-1) + 15;
            if digital.agc(i) > 75 
                digital.agc(i) = 75;
            end   
        elseif digital.det_dB(i) < 20 
            digital.agc(i) = digital.agc(i-1) + 5;
            if digital.agc(i) > 75 
                digital.agc(i) = 75;
            end    
        elseif digital.det_dB(i) < 30 
            digital.agc(i) = digital.agc(i-1) + 1;
            if digital.agc(i) > 75 
                digital.agc(i) = 75;
            end
        elseif digital.det_dB(i) < 60 
            digital.agc(i) = digital.agc(i-1);
            digital.change(i) = 0;
            if digital.agc(i) > 75 
                digital.agc(i) = 75;
            end   
        elseif digital.det_dB(i) < 67
            digital.agc(i) = digital.agc(i-1) - 1;
            if digital.agc(i) < -20 
                digital.agc(i) = -20;
            end   
        elseif digital.det_dB(i) < 70 
            digital.agc(i) = digital.agc(i-1) - 5;
            if digital.agc(i) < -20 
                digital.agc(i) = -20;
            end   
        elseif digital.det_dB(i) < 80 
            digital.agc(i) = digital.agc(i-1) - 15;
            if digital.agc(i) < -20 
                digital.agc(i) = -20;
            end    
        else
            digital.agc(i) = digital.agc(i-1) - 30;
            if digital.agc(i) < -20 
                digital.agc(i) = -20;
            end    
        end
        
        if digital.agc(i) == digital.agc(i-1)
            digital.change(i) = 0; 
        end 
    else
        digital.agc(i) = digital.agc(i-1);
    end    
end    


%%-------------------------------------------------------------------------------
%%  DISPLAY
%%-------------------------------------------------------------------------------

n = (0:len-1);
t = n/ADC.Fclk;

if display.jammer == 1
    figure_jammer = figure('Name', 'jammer'); 
    subplot(2, 1, 1);
    plot(t, real(jammer.wave), 'r');
    hold on;
    plot(t, imag(jammer.wave), 'g');
    hold off;
    subplot(2, 1, 2);
    plot(t, jammer.dBm);
    hold on;
    plot(t, jammer.db);
    hold off;
end 


if display.digital == 1
    figure_digital = figure('Name', 'digital'); 
    subplot(3, 1, 1);
    plot(t, real(digital.adc), 'r');
    hold on;
    plot(t, imag(digital.adc), 'b');
    title('ADC I(r) Q(b)', 'FontName', 'Arial Cyr')
    
    hold off;
    subplot(3, 1, 2);
    plot(t, real(digital.det_dB), 'm');
    hold on;
    plot(t, real(digital.agc), 'b');
    title('det_dB (m) AGC (b)', 'FontName', 'Arial Cyr')
    
    hold off;
    subplot(3, 1, 3);
    plot(t, real(digital.present), 'r');
    hold on;
    plot(t, real(digital.change + 1.1),  'b');
    title('jammer present (r) AGC change (b)', 'FontName', 'Arial Cyr')
    
end 



%%-------------------------------------------------------------------------------
%%  FUNCTION
%%-------------------------------------------------------------------------------

%% ADC

function code = analog2digital (wave, ADC)
    code = wave ./ ADC.LSB ;
    for i=1:length(wave)
        if real(code(i)) > (2^(ADC.NBits - 1)-1)
            I = (2^(ADC.NBits - 1)-1);
        elseif real(code(i)) < -2^(ADC.NBits - 1)
            I = -2^(ADC.NBits - 1);
        else
            I = real(code(i));
        end     
        if imag(code(i)) > (2^(ADC.NBits - 1)-1)
            Q = (2^(ADC.NBits - 1)-1);
        elseif imag(code(i)) < -2^(ADC.NBits - 1)
            Q = -2^(ADC.NBits - 1);
        else
            Q = imag(code(i));
        end     
        code(i) = complex(I, Q); 
    end
    code = double(int16(code));
end


%% convert jammers parameters to wave
function wave = jammer_wave_generation(len, magn)
    RJ = random('Normal', 0, magn, [len, 2]);
    wave = complex(RJ(1:end, 1), RJ(1:end, 2));
end 

function wave = jammer_db_generation(fragment)
    n   = fragment.n    ; 
    len = fragment.len  ;
    len_d2 = int32(len/2);
    min = fragment.min  ;
    max = fragment.max  ;
    wave = ones(len*n, 1) .* min;
    step = (max-min) / (n-1);
    for i=1:n
        wave(((i-1)*len + 1) : (i*len-len_d2)) = min + (i-1) * step ;
    end 
end 


%% detector
function wave_o = detector(wave_i, aperture)
    len  = length(wave_i);
    temp = zeros(len + aperture, 1);
    temp(1:len)     = wave_i(1:end);
    temp(len+1:end) = wave_i(len);
    for i=1:len
        wave_o(i) = mean(abs(temp(i:i+aperture)));
    end 
end


%% gain
function wave_o = gain(wave_i, dB)
    factor = db2factor(dB);
    wave_o = wave_i  .* factor;
end 


%% -- Vrms -> dBm
function dBm = rms2dbm(V, R)
    dBm = real( 10 .* log10( V.^2 ./ (0.001 .* R)) );
end

%% -- dBm -> Vrms
function V = dbm2rms(dBm, R)
    V = sqrt(R .* 10.^((dBm - 30) ./ 10));
end


%% dB to factor
function factor = db2factor(dB)
    factor = 10 .^ (dB ./ 20);
end 

%% factor to dB
function dB = factor2dB(factor)
    dB = 20 .* log10(factor);
end