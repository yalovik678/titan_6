#include "MCU32.h"
#include "AK.h"
#include "wake.h"

//int AGC_on = 0;
//extern unsigned dac_data[];
SYN_dataT SYN_data;
//AK_dataT AK_data;
uint32_t trimming_step = 0;
uint32_t trimming_chan = 0;
uint32_t trimming_err = 0;
uint16_t trimming_pause = 0;
uint32_t cal_offset_result[7];
uint8_t RF_AGC_level = 15;
uint8_t IFA_AGC_level = 10;
uint8_t rf_agc_lev[8];
uint8_t ifa_agc_lev[8];
uint32_t ATT_level = 0;

extern unsigned char board_rx_buf[];
extern uint32_t board_rx_buf_wr_ptr, board_rx_buf_rd_ptr;
extern uint32_t board_rx_buf_len;

extern uint32_t prog_status, prog_cntrl;
//------------------------------------------------------------------------------
extern void WriteToSYN(unsigned word);
//extern void send_DAC(void);
extern void send_to_board(uint8_t bt);
extern void init_board_rx(void);
void wr_RFIC(uint8_t rfic, uint8_t reg, uint8_t data); 
uint8_t rd_RFIC(uint8_t rfic, uint8_t reg); 
//------------------------------------------------------------------------------
/*void Update_AK(void)
{
volatile int* AK = (volatile int *)AK_BASE_ADR;  

  AK[0] = AK_data.R[0];
  AK[1] = AK_data.R[1];
  for (int i = 3; i < 7; i++)
      AK[i] = AK_data.R[i];
  for (int i = 8; i < 16; i++)
      AK[i] = AK_data.R[i];
} */
//------------------------------------------------------------------------------
void initAK(void)
{
volatile int* AK = (volatile int *)AK_BASE_ADR;  

  trimming_err = 0;
#ifdef XCH_RFIC
  AK[COMMON] = 0x0b600;       //
#else  
  AK[COMMON] = 0x0b900;       //      ����� ���������, �������� Q, real_mode, ���������� ������� 
#endif  
  AK[TBI_CONFIG_1] = 0x0;       //      ������������ ��������������� � ���������� ���. �������� ��������������.
/*#ifdef L1_L2_GPS  
  AK[TBI_CONFIG_4] = 0x000140FF;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.
#endif  
#ifdef L1_GPS_GLO  
  AK[TBI_CONFIG_4] = 0x0001E0FF;       
#else
  AK[TBI_CONFIG_4] = 0x000100FF;
#endif  */
  AK[TBI_CONFIG_5] = 0x0;       //  ������������ ��������������� � ���������� ���. �������� �������������� � ������� ������ ���������� ������.
  AK[AK_CONFIG_1] = 0x20A63A0E;       //      ��������� �������� ���������� ������ ����������������. ����� ������ ����������������.
  AK[AK_CONFIG_2] = 0;                  //      ��������� ������� ���������� � ��� ����������������.

  AK[REG_INDEX] = 0;       //      ��������� ���� ������������. ������ � ���� ���� �������� ��������� ������ ������ ������������. 
                            // �� ������ �������� ������ �� ����� ����������� ������ �� ������ ������������.
  AK[REG_SEL] = 0;       //      ��������� ������� ����������� ������������
  AK[REG_CONTRL] = 0;       //      ��������� � ���������� ������� ������������
#if (SW_VERSION == 205) // Vco 1580   
  AK[IF_FIR] = 0x55;   // GPS only
#endif    
  
// ��������� ��  
//---------------------------------------------------------  like SATURN 
#ifdef DA14_DA16 // (L2)                                             
  // Fd = 1590000000/30 
  AK[TBI_CONFIG_4] = 0x000110FF;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.
  AK[FRQ_BAND1] = 0x44D4873E;       //      ��� ������� ��������� 1 (GLO1_L2)      14.25 MHz  Fvco = 1230 MHz
  AK[FRQ_BAND2] = 0xF4685503;       //      ��� ������� ��������� 2 (GPS L2OC)     -2.4 MHz    Fvco = 1230 MHz
  AK[FRQ_BAND3] = (4294967296 * -22860000) / CPU_CLK;       //      ��� ������� ��������� 3 (E5b,B2)       -22.86 MHz   Fvco = 1230 MHz
  AK[FRQ_BAND4] = 0x55BC609A;       //      ��� ������� ��������� 4 (GLO2_L2)      17.75 MHz    Fvco = 1230 MHz
  
  AK[FRQ_OUT_BAND1] = 0x27D95BC6;       //      ��� ������� ����������� ��������� 1 (GLO1_L2) 8.25 MHz Fvco = 1230 MHz
  AK[FRQ_OUT_BAND2] = 0xF4685503;       //      ��� ������� ����������� ��������� 2 (GPS L2OC)
  AK[FRQ_OUT_BAND3] = (4294967296 * -22860000) / CPU_CLK;       //      ��� ������� ����������� ��������� 3 (E5b,B2) 
  AK[FRQ_OUT_BAND4] = 0x55BC609A;       //      ��� ������� ����������� ��������� 4 (GLO2_L2) Fvco = 1230 MHz  
#else  //  DA6, DA8 (L1)
  // Fd = 1590000000/30
  AK[TBI_CONFIG_4] = 0x0011001F;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.  
//  AK[TBI_CONFIG_4] = 0x00010bFF;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.  
  AK[FRQ_BAND1] =  0x2F1826A4;      //      ��� ������� ��������� 1 (GLO1)      9.75 MHz    Fvco = 1590
#if (SW_VERSION == 205) // Vco 1580 
  AK[FRQ_BAND2] =(4294967296 * -4580000) / CPU_CLK;  //      ��� ������� ��������� 2 (GPS)       -4.58 MHz    Fvco =  1580
  AK[FRQ_OUT_BAND2] = (4294967296 * -4580000) / CPU_CLK;       //      ��� ������� ����������� ��������� 2 (GPS)
#else  
  AK[FRQ_BAND2] =(4294967296 * -14580000) / CPU_CLK;  //      ��� ������� ��������� 2 (GPS)       -14.58 MHz    Fvco =  1590
  AK[FRQ_OUT_BAND2] = (4294967296 * -14580000) / CPU_CLK;       //      ��� ������� ����������� ��������� 2 (GPS)
#endif  
 // AK[FRQ_BAND3] = 0x8C8C8023;       //      ��� ������� ��������� 3 (BeiDou)     -23.902 MHz   Fvco =  1585
  AK[FRQ_BAND3] = 0;       //  off
  AK[FRQ_BAND4] = 0x421CFB2B;       //      ��� ������� ��������� 4 (GLO2)      13.6875 MHz    Fvco = 1590

  AK[FRQ_OUT_BAND1] = 0x2F1826A4;       //      ��� ������� ����������� ��������� 1 (GLO1)   3.75 MHz
  
  AK[FRQ_OUT_BAND3] = 0x8C8C8023;       //      ��� ������� ����������� ��������� 3 (BeiDou) 
  AK[FRQ_OUT_BAND4] = 0x421CFB2B;       //      ��� ������� ����������� ��������� 4 (GLO2) 
#endif    
    
  AK[IMMIT_CNTRL] = 0;  
  
//  AK[IMMIT_CNTRL] = 0x102;  // sin/cos test signal
  AK[IMMIT_FRQ] = (4294967296 * 14000000) / CPU_CLK;
}  
//------------------------------------------------------------------------------

int CheckRegistratorConst(uint16_t out_const, uint8_t rd_mask) {   
uint32_t temp, registrator_addr;   //temp2, 
volatile int* AK = (volatile int *)AK_BASE_ADR;
  
    AK[AK_CONFIG_2] = 0; //(ConfigAK.AK_Ovfw_selector<<8) + ConfigAK.AK_Out_selector;
    AK[REG_SEL] = 9;        // AK input / HSDI output
    AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
    AK[REG_CONTRL] = 0x400003fe;     // Set REG_Auto_start & after_start = 1023
    AK[REG_CONTRL] |= 0x80000000;  // Set REG_Enable
//    temp2 = (out_const + (out_const<<16)) ^ 0x08000800;     // compl2
    while((AK[REG_CONTRL] & 0x20000000) == 0);   // wait REG_Ready 
//    if (!rd_mask) {             //A[I,Q], B[I,Q]
      registrator_addr = 0;
      for (int i = 0; i < 1024; i++) {  //A[I,Q]
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        if ((temp ^ out_const) & 0xfff) {
//        if ((temp ^ temp2) & 0x0fff0fff) {
          AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
          return 0;
        }  
      }
      registrator_addr = 1;
      for (int i = 0; i < 1024; i++) {  //B[I,Q]
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        if ((temp ^ out_const) & 0xfff) { //if ((temp ^ temp2) & 0x0fff0fff) {
          AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
          return 0;
        }  
      }  
//    } else { // if (!rd_mask) {         // D[Q], E[I,Q], F[I]        
      registrator_addr = 2;       // C[I,Q]      
      for (int i = 0; i < 1024; i++) {
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        if ((temp ^ out_const) & 0xfff) { //if ((temp ^ temp2) & 0x0fff0000) {  // D[Q]
          AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
          return 0;
        }   
      }  
      registrator_addr = 3;       // D[I,Q]      
      for (int i = 0; i < 1024; i++) {
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        if ((temp ^ out_const) & 0xfff) { //if ((temp ^ temp2) & 0x0fff0fff) {  
          AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
          return 0;
        }   
      }
//    } // if (!rd_mask) { 
    AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
    return 1;
}  
//------------------------------------------------------
uint8_t ATT_level0 = 15;

void RF_AGC(void) 
{
uint8_t lev_ifa, regn, regd;
uint32_t rf_gain;
volatile int* GPIO = (volatile int *)GPIO_BASE;

  for (int i = 0; i < ADC_NUM*4; i++) {
    wr_RFIC(i & 0x4 ? 1 : 0, 5, (i<<4) & 0x30);         // channel
    ifa_agc_lev[i] = rd_RFIC(i & 0x4 ? 1 : 0, 10);     // IFA gain value
    rf_agc_lev[i] = rd_RFIC(i & 0x4 ? 1 : 0, 9) << 4;     // RF gain value
  }
  
  if (prog_cntrl & ATT_ENABLE) {
#ifdef ATT_4BIT
    regn = ATT_level0;
    lev_ifa = ifa_agc_lev[4];
    rf_gain = rf_agc_lev[4];
    for (int i = 5; i < 8; i++) {
      lev_ifa += ifa_agc_lev[i];
      rf_gain += rf_agc_lev[i];
    }
    rf_gain = rf_gain>>4;
    if (lev_ifa > 2*4) 
      ATT_level0 = 15;
    else {
      switch (ATT_level0) {
      case 0:
        if ((lev_ifa > 3*4) && (rf_gain > 14*4)) 
          ATT_level0 = 5;
        break;
      case 5:
        if ((lev_ifa <= 0.5*4) && (rf_gain <= 5*4)) 
          ATT_level0 = 0;
        else {
          if ((lev_ifa > 3*4) && (rf_gain > 14*4)) 
            ATT_level0 = 10;
        }  
        break;
      case 10:
        if ((lev_ifa <= 1*4) && (rf_gain <= 6*4)) 
          ATT_level0 = 5;
        else {
          if ((lev_ifa > 3*4) && (rf_gain > 14*4)) 
            ATT_level0 = 15;
        }  
        break;
      default:
        if ((lev_ifa <= 1*4) && (rf_gain <= 4.5*4)) 
          ATT_level0 = 10;
      }  
    } 
 //   if (regn != ATT_level0)
    GPIO[GPIO_OUT_1] = (GPIO[GPIO_OUT_1] & (~0xf0000000)) | (ATT_level0<<28);
#else //#ifdef ATT_4BIT    
    if (ifa_agc_lev[7] < 3) 
      GPIO[GPIO_OUT_1] &= ~0x80000000;  // att = 16 db 
    else {
      if (ifa_agc_lev[7] > 11) 
   //     GPIO[GPIO_OUT_1] |= 0x80000000;  // att = 0 db
        GPIO[GPIO_OUT_1] |= 0xF0000000;  // att = 0 db
    } 
#endif //ATT_4BIT     
  } else {  // if (prog_cntrl & ATT_ENABLE) {
     GPIO[GPIO_OUT_1] = (GPIO[GPIO_OUT_1] & (~0xf0000000)) | ATT_level;  
  }  

  if (prog_cntrl & (RF_AGC_MANUAL | IFA_AGC_MANUAL)) { 
    for (int i = 0; i < ADC_NUM*4; i++) {
      regn = 17 + (i&0x3)*7;
      regd = rd_RFIC(i & 0x4 ? 1 : 0, regn);
      if (prog_cntrl & IFA_AGC_MANUAL) {
        wr_RFIC(i & 0x4 ? 1 : 0, regn+1, (IFA_AGC_level<<5) + 0xA);
        regd &= 0xfc;
        regd |= ((IFA_AGC_level>>3) & 0x03);
      }  
      if (prog_cntrl & RF_AGC_MANUAL) {
        regd &= 0x0f;
        regd |= ((RF_AGC_level<<4) & 0xf0);
      } 
      wr_RFIC(i & 0x4 ? 1 : 0, regn, regd);  
    }    
  }
  
}

/*void SET_RF_AGC(void) 
{
uint8_t lev_ifa, regn, rf_gain, regd;

  for (int i = 0; i < ADC_NUM*4; i++) {
    wr_RFIC(i & 0x4 ? 1 : 0, 5, (i<<4) & 0x30);         // channel
    lev_ifa = rd_RFIC(i & 0x4 ? 1 : 0, 10);     // IFA gain value
    ifa_agc_lev[i] = lev_ifa;
    regn = 17 + (i&0x3)*7;
    regd = rd_RFIC(i & 0x4 ? 1 : 0, regn);
    rf_gain = regd & 0xf0;
    regd &= 0x0f;
    wr_RFIC(i & 0x4 ? 1 : 0, regn, ((RF_AGC_level<<4) & 0xf0) | regd);  // max gain
    rf_agc_lev[i] = rf_gain;  
  }  
}*/

/******************************************************
**                  End Of File
*******************************************************/