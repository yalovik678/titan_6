#ifndef WAKE_H
#define WAKE_H

#define CHANNEL_NUM 4
//-----------------------------------------------------------------------------
#define DS_RX_BUFF_SIZE 256
#define BOARD_TX_BUFF_SIZE 32
#define BOARD_RX_BUFF_SIZE 32
// status
//#define DS_RX_BUFF_OVERFL 0x0001
//#define BRD_RX_BUFF_OVERFL 0x0002
//#define TRIMMING_READY 0x0004
//#define BOARD_TRIMMING_ERR 0x0008
//#define CAL_LOCK 0x0010
#define WR_FLASH_ERR 0x0020
//#define CLK_PLL_LOCK 0x0040
#define TRIMMING_ADC0_FAIL 0x0100
#define TRIMMING_ADC1_FAIL 0x0200
#define INIT_RFIC_1_FAIL 0x0400
#define INIT_ADC_1_FAIL 0x0800
#define INIT_MODUL_FAIL 0x1000
#define DEBUG_FLAG 0x8000
// control
#define WR_FLASH_IMAGE 0x10000
//#define RF_AGC_ON 0x20000
#define DO_TRIMMING 0x40000
#define AGC_ON 0x80000
#define ADC_CALIBR_ON 0x100000
#define RESTART_ALL 0x200000
#define RF_AGC_MANUAL   0x400000
#define ATT_ENABLE   0x800000
#define UPDATE_FLASH   0x1000000
#define IFA_AGC_MANUAL   0x2000000
//-----------------------------------------------------------------------------
#define HOST_TX_BUFF_SIZE 640

// status PC receive
#define PC_INIT_ERROR  0x1
#define PC_RECV_ERROR  0x2
#define PC_VALID_DATA  0x4
#define PC_RECV_OK_STAT 0x8
#define PC_RECV_READY 0x10
#define PC_RECV_MSG 0x20

typedef struct {
    int status;     // ������
    int rsv_state;  // ��������� �����
    int cmnd;
    uint32_t wdata;   
} host_rx_dataT;

#define CMND_WAIT 0
#define BYTE1_WAIT 1
#define BYTE2_WAIT 2
#define BYTE3_WAIT 3
#define BYTE4_WAIT 4
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// wake
typedef struct {
    uint8_t address;
    uint8_t command;
    int32_t length;
    uint8_t data[256];
    uint8_t _crc;
    uint8_t _fFESC;
    int32_t _index;
    uint32_t _status;
//    portTickType _timeout;
} WakePackageT;

#define WAKE_READ_WAITING_FEND 0
#define WAKE_READ_COMPLETE 0x1
#define WAKE_RESV_PACKET 0x2
//#define WAKE_READ_OWERFULL 0x4
#define WAKE_READ_CRC_ERROR 0x8
#define WAKE_READ_ERROR 0x10
#define WAKE_CMND_ERROR 0x20
#define WAKE_READ_TIMEOUT 0x40

#define WAKE_WRITE_COMPLETE 0x80
#define WAKE_WRITE_IN_PROCESS 0

#define CRC_WAKE_INIT 0xDE
#define FEND 0xC0
#define FESC 0xDB
#define TFESC 0xDD
#define TFEND 0xDC

#define WAKE_FEND_INDEX         (-4)
#define WAKE_ADDRESS_INDEX      (-3)
#define WAKE_COMMAND_INDEX      (-2)
#define WAKE_LENGTH_INDEX       (-1)
#define WAKE_DATA_INDEX         (0)

#define WAKE_CMD_AHRS_INIT      2
#define WAKE_CMD_AHRS_DATA      1
#define WAKE_CMD_SYNC           0

#define WAKE_C_Nop              0
#define WAKE_C_Err              1
#define WAKE_C_SWv              2
#define WAKE_C_Info             3
#define WAKE_C_RdRg             4
#define WAKE_C_WrRg             5
#define WAKE_C_Registrator      6
//#define WAKE_C_WrSyn	        7
#define WAKE_C_WrImg            7
#define WAKE_C_RdGPIO	        8
#define WAKE_C_WrRFIC           9
#define WAKE_C_RdRFIC           10
#define WAKE_C_WrPLL            11
#define WAKE_C_RdPLL            12
#define WAKE_C_WrGPIO           13
#define WAKE_C_WrADC            14
#define WAKE_C_RdADC            15

#endif // WAKE_H
/******************************************************
**                  End Of File
*******************************************************/
















