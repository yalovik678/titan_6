#include "MCU32.h"
#include "wake.h"
#include "selftest.h"
#include "ak.h"


/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/
// F = (8*B_cnt + A_cnt)*1.242 MHz
// a_b_cnt = 0x9D34;  A=13, B=157 - F = 1576.098 MHz
int CalibrTonOn(uint32_t a_b_cnt, uint32_t pow);  // b20..b8 = B_cnt, b6..b2 = A_cnt, pow = 0..3 = -14, -11, -8, -5 dB
void CalibrTonOff(void);
//------------------------------------------------------------------------------
//------------------------------------------------------
void ReadRegistratorData(uint32_t chan, uint32_t sel, int32_t* dat_I, int32_t* dat_Q) {   
uint32_t temp, registrator_addr;
volatile int* AK = (volatile int *)AK_BASE_ADR;
int16_t temp_short;
  
    AK[AK_CONFIG_2] = 0; //(ConfigAK.AK_Ovfw_selector<<8) + ConfigAK.AK_Out_selector;
    AK[REG_SEL] = sel;        // 0..9
    AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
    AK[REG_CONTRL] = 0x400003fe;     // Set REG_Auto_start & after_start = 1023
    AK[REG_CONTRL] |= 0x80000000;  // Set REG_Enable
    registrator_addr = chan;
    while((AK[REG_CONTRL] & 0x20000000) == 0);   // wait REG_Ready 
    for (int i = 0; i < 1024; i++) {  //A[I,Q]
      AK[REG_INDEX] = registrator_addr;
      registrator_addr += 0x8;          //registrator_addr++;
      temp = AK[REG_INDEX];
      temp_short = temp >> 16;
      dat_Q[i] = temp_short;
      temp_short = temp;
      dat_I[i] = temp_short;
    }
    AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
}  
//---------------------------------------------------------------------------
uint32_t FFT_power(int n, int32_t* II, int32_t* QQ)
{
//float x[1024], y[1024];
int32_t* x = II;
int32_t* y = QQ;
int32_t tmp; 
int32_t n2=n>>1, kd, i1, i;
int32_t Tx,Ty;
int32_t Wx, Wy;

/*  for(int i=0; i<n; i++) {
    x[i] = II[i];
    y[i] = QQ[i];
  }
*/
  for(int j=i=0; i<n-1; i++) {
      if(i<j) {
         tmp = x[i]; x[i] = x[j]; x[j] = tmp;   //swap
         tmp = y[i]; y[i] = y[j]; y[j] = tmp;   //swap
      }      
      for(kd=n2;j>=kd;kd=(kd+1)>>1) j-=kd;
      j+=kd;
  }

  for(kd=1; kd<=n2; kd<<=1)
    for(int j=0; j<kd; j++) {
//            Wx=cosl(M_PI/kd*j);
//            Wy=sinl(M_PI/kd*j);
      Wx = icos[512/kd*j];
      Wy = isin[512/kd*j];
      for(i=j; i<n; i+=kd<<1) {
        i1=i+kd;
        Tx=x[i1]*Wx - y[i1]*Wy;
        Ty=x[i1]*Wy + y[i1]*Wx;
        Tx = Tx/8192; // >>13;
        Ty = Ty/8192; // >>13;
        x[i1]=x[i]-Tx;
        y[i1]=y[i]-Ty;
        x[i]+=Tx;
        y[i]+=Ty;
      }
    }
  
  Wx = 0; i1 = 0;
  for(i=0; i<n; i++) {
    x[i]/=n2;
    y[i]/=n2;
   //     y[i] = sqrt(x[i]*x[i]+y[i]*y[i]); 
    y[i] = (x[i]*x[i]+y[i]*y[i]); 
//    II[i] = ((uint32_t)y[i])>>8;
    if (y[i] > Wx) {
      Wx = y[i];
      i1 = i;
    }
  }
  return i1;
}
//---------------------------------------------------------------------------
//------------------------------------------------------------------------------
int SelfTest(void)
{
volatile int* AK = (volatile int *)AK_BASE_ADR;  
int32_t II[1024], QQ[1024];  
uint32_t res;

 // F = (8*B_cnt + A_cnt)*1.242 MHz
 // a_b_cnt = 0x9D34;  A=13, B=157 - F = 1576.098 MHz
//int CalibrTonOn(uint32_t a_b_cnt, uint32_t pow)  // b20..b8 = B_cnt, b6..b2 = A_cnt, pow = 0..3 = -14, -11, -8, -5 dB
if(CalibrTonOn((157<<8)|(15<<2), 0)) { // F = 1571.13 MHz  gps, glonas(band1,2,3)
    CalibrTonOff();
    return 0;
  }  

  AK[AK_CONFIG_1] = 0xC0A63A0E;         // host_mode = 3 AK off
  for (int i = 0; i < 4; i++) {
    // void ReadRegistratorData(uint32_t chan, uint32_t sel, uint16_t* dat_I, uint16_t* dat_Q)
    ReadRegistratorData(i, 0, II, QQ);   // in_data
    res = FFT_power(1024, II, QQ);
    if (( res < 10) || (res > 15))  
      ; //return 0;
  }
  
  AK[AK_CONFIG_1] = 0x00A63A0E; // host_mode = 0 AK on
  
  if(CalibrTonOn((157<<8)|(5<<2), 0)) { // F = 1566.162 MHz beidou (band4)
    CalibrTonOff();
    return 0;
  }  

 
  CalibrTonOff();
  return 1;
}

//------------------------------------------------------------------------------

/******************************************************
**                  End Of File
*******************************************************/
