#include "MCU32.h"
#include "AK.h"
#include "wake.h"

//int AGC_on = 0;
//extern unsigned dac_data[];
SYN_dataT SYN_data;
//AK_dataT AK_data;
uint32_t trimming_step = 0;
uint32_t trimming_chan = 0;
uint32_t trimming_err = 0;
uint16_t trimming_pause = 0;
uint32_t cal_offset_result[7];
uint8_t RF_AGC_level = 15;
uint8_t IFA_AGC_level = 10;
uint8_t rf_agc_lev[8];
uint8_t ifa_agc_lev[8];
uint32_t ATT_level = 0;

extern unsigned char board_rx_buf[];
extern uint32_t board_rx_buf_wr_ptr, board_rx_buf_rd_ptr;
extern uint32_t board_rx_buf_len;

extern uint32_t prog_status, prog_cntrl;
//------------------------------------------------------------------------------

extern uint16_t Pic(uint16_t data, uint16_t pick_data);
extern uint16_t Machine(uint16_t data);
extern int Host(uint16_t data, int control);
extern uint8_t rf_ifa_rom(int data);

extern void WriteToMax14662(uint8_t chan,  uint8_t dat) ;
//------------------------------------------------------------------------------
extern void WriteToSYN(unsigned word);
//extern void send_DAC(void);
extern void send_to_board(uint8_t bt);
extern void init_board_rx(void);
void wr_RFIC(uint8_t rfic, uint8_t reg, uint8_t data); 
uint8_t rd_RFIC(uint8_t rfic, uint8_t reg); 
//------------------------------------------------------------------------------
/*void Update_AK(void)
{
volatile int* AK = (volatile int *)AK_BASE_ADR;  

  AK[0] = AK_data.R[0];
  AK[1] = AK_data.R[1];
  for (int i = 3; i < 7; i++)
      AK[i] = AK_data.R[i];
  for (int i = 8; i < 16; i++)
      AK[i] = AK_data.R[i];
} */
//------------------------------------------------------------------------------
void initAK(void)
{
volatile int* AK = (volatile int *)AK_BASE_ADR;  

  trimming_err = 0;
#ifdef XCH_RFIC
  AK[COMMON] = 0x0b000;       //
#else  
  AK[COMMON] = 0x0b000;       //      ����� ���������, �������� Q, real_mode, ���������� ������� 
#endif  
  AK[TBI_CONFIG_1] = 0x0;       //      ������������ ��������������� � ���������� ���. �������� ��������������.
/*#ifdef L1_L2_GPS  
  AK[TBI_CONFIG_4] = 0x000140FF;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.
#endif  
#ifdef L1_GPS_GLO  
  AK[TBI_CONFIG_4] = 0x0001E0FF;       
#else
  AK[TBI_CONFIG_4] = 0x000100FF;
#endif  */
  AK[TBI_CONFIG_5] = 0x0;       //  ������������ ��������������� � ���������� ���. �������� �������������� � ������� ������ ���������� ������.
  AK[AK_CONFIG_1] = 0x20A63A0E;       //      ��������� �������� ���������� ������ ����������������. ����� ������ ����������������.
  AK[AK_CONFIG_2] = 0;                  //      ��������� ������� ���������� � ��� ����������������.

  AK[REG_INDEX] = 0;       //      ��������� ���� ������������. ������ � ���� ���� �������� ��������� ������ ������ ������������. 
                            // �� ������ �������� ������ �� ����� ����������� ������ �� ������ ������������.
  AK[REG_SEL] = 0;       //      ��������� ������� ����������� ������������
  AK[REG_CONTRL] = 0;       //      ��������� � ���������� ������� ������������
#if (SW_VERSION == 205) // Vco 1580   
  AK[IF_FIR] = 0x55;   // GPS only
#endif    
  
// ��������� ��  
//---------------------------------------------------------  like SATURN 
#ifdef DA14_DA16 // (L2)                                             
  // Fd = 1590000000/30 
  AK[TBI_CONFIG_4] = 0x00011FFF;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.
  AK[FRQ_BAND1] = 0x44D4873E;       //      ��� ������� ��������� 1 (GLO1_L2)      14.25 MHz  Fvco = 1230 MHz
  AK[FRQ_BAND2] = 0xF4685503;       //      ��� ������� ��������� 2 (GPS L2OC)     -2.4 MHz    Fvco = 1230 MHz
  AK[FRQ_BAND3] = (4294967296 * -22860000) / CPU_CLK;       //      ��� ������� ��������� 3 (E5b,B2)       -22.86 MHz   Fvco = 1230 MHz
  AK[FRQ_BAND4] = 0x55BC609A;       //      ��� ������� ��������� 4 (GLO2_L2)      17.75 MHz    Fvco = 1230 MHz
  
  AK[FRQ_OUT_BAND1] = 0x27D95BC6;       //      ��� ������� ����������� ��������� 1 (GLO1_L2) 8.25 MHz Fvco = 1230 MHz
  AK[FRQ_OUT_BAND2] = 0xF4685503;       //      ��� ������� ����������� ��������� 2 (GPS L2OC)
  AK[FRQ_OUT_BAND3] = (4294967296 * -22860000) / CPU_CLK;       //      ��� ������� ����������� ��������� 3 (E5b,B2) 
  AK[FRQ_OUT_BAND4] = 0x55BC609A;       //      ��� ������� ����������� ��������� 4 (GLO2_L2) Fvco = 1230 MHz  
#else  //  DA6, DA8 (L1)
  // Fd = 1590000000/30
  AK[TBI_CONFIG_4] = 0x00110F1F;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.  
//  AK[TBI_CONFIG_4] = 0x00010bFF;       //  ������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.  
  AK[FRQ_BAND1] = 0x2F1826A4;       //      ��� ������� ��������� 1 (GLO1)      9.75 MHz    Fvco = 1590
#if (SW_VERSION == 205) // Vco 1580 
  AK[FRQ_BAND2] =(4294967296 * -4580000) / CPU_CLK;  //      ��� ������� ��������� 2 (GPS)       -4.58 MHz    Fvco =  1580
  AK[FRQ_OUT_BAND2] = (4294967296 * -4580000) / CPU_CLK;       //      ��� ������� ����������� ��������� 2 (GPS)
#else  
  AK[FRQ_BAND2] =(4294967296 * -14580000) / CPU_CLK;  //      ��� ������� ��������� 2 (GPS)       -14.58 MHz    Fvco =  1590
  AK[FRQ_OUT_BAND2] = (4294967296 * -14580000) / CPU_CLK;       //      ��� ������� ����������� ��������� 2 (GPS)
#endif  
 // AK[FRQ_BAND3] = 0x8C8C8023;       //      ��� ������� ��������� 3 (BeiDou)     -23.902 MHz   Fvco =  1585
  AK[FRQ_BAND3] = 0;       //  off
  AK[FRQ_BAND4] = 0;       //      ��� ������� ��������� 4 (GLO2)      13.6875 MHz    Fvco = 1590

  AK[FRQ_OUT_BAND1] = 0;       //      ��� ������� ����������� ��������� 1 (GLO1)   3.75 MHz
  
  AK[FRQ_OUT_BAND3] = 0x8C8C8023;       //      ��� ������� ����������� ��������� 3 (BeiDou) 
  AK[FRQ_OUT_BAND4] = 0x421CFB2B;       //      ��� ������� ����������� ��������� 4 (GLO2) 
#endif    
    
  AK[IMMIT_CNTRL] = 0;  
  
  //AK[IMMIT_CNTRL] = 0x102;  // sin/cos test signal
  AK[IMMIT_FRQ] = (4294967296 * 14000000) / CPU_CLK;
}  
//------------------------------------------------------------------------------

uint16_t pick_data = 0;
uint16_t pick_data_A = 0;
uint16_t pick_data_B = 0;
uint16_t pick_data_C = 0;
uint16_t pick_data_D = 0;
uint16_t decibel_data_A = 0;
uint16_t decibel_data_B = 0;
uint16_t decibel_data_C = 0;
uint16_t decibel_data_D = 0;
uint16_t decibel_data = 0;

int count=0;

int control = 299;
int control_bk = 299;
uint8_t lev_rf_ifa = 0x3F;
uint8_t lev_rf_ifa_bk = 0x3F;

void CheckRegistratorData(void) {   
uint32_t temp=0, registrator_addr;  

volatile int* AK = (volatile int *)AK_BASE_ADR;
  
    AK[AK_CONFIG_2] = 0; //(ConfigAK.AK_Ovfw_selector<<8) + ConfigAK.AK_Out_selector;
    AK[REG_SEL] = 9;        // AK input / HSDI output
    AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
    AK[REG_CONTRL] = 0x400003fe;     // Set REG_Auto_start & after_start = 1023
    AK[REG_CONTRL] |= 0x80000000;  // Set REG_Enable

    while((AK[REG_CONTRL] & 0x20000000) == 0);   // wait REG_Ready 

     registrator_addr = 0;      
      for (int i = 0; i < 1024; i++) {  //A[I,Q]        
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        //temp = (~AK[REG_INDEX]) & 0xfff;
    
        if(temp<0xF000)     
        pick_data_A=Pic(temp,pick_data_A);        
      }      
      //decibel_data_A = Machine(pick_data_A);
      
      registrator_addr = 1;
      for (int i = 0; i < 1024; i++) {  //B[I,Q]        

        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        //temp = (~AK[REG_INDEX]) & 0xfff;
        
        if(temp<0xF000)
        pick_data_B=Pic(temp,pick_data_B);
        //decibel_data_B = Machine(pick_data_B);  
      }  
      
      registrator_addr = 2;       // C[I,Q]      
      for (int i = 0; i < 1024; i++) {
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        //temp = (~AK[REG_INDEX]) & 0xfff; 
        
        if(temp<0xF000)
        pick_data_C=Pic(temp,pick_data_C);  
      } 
      //decibel_data_C = Machine(pick_data_C);

      registrator_addr = 3;       // D[I,Q]      
      for (int i = 0; i < 1024; i++) {
        AK[REG_INDEX] = registrator_addr;
        registrator_addr += 0x8;          //registrator_addr++;
        temp = AK[REG_INDEX];
        //temp = (~AK[REG_INDEX]) & 0xfff; 
        
        if(temp<0xF000)
        pick_data_D=Pic(temp,pick_data_D);  
      }    
      //decibel_data_D = Machine(pick_data_D);
     
      // max pick_data 
      pick_data = pick_data_A;  
        if(pick_data<pick_data_B)
          pick_data = pick_data_B;
        if(pick_data<pick_data_C)
          pick_data = pick_data_C;
        if(pick_data<pick_data_D)
          pick_data = pick_data_D;
       
       decibel_data = Machine(pick_data);     
      
      //decibel_data = decibel_data_A;
        //decibel_data= decibel_data_B;
          //decibel_data=  decibel_data_C ;
           //decibel_data=  decibel_data_D;
        
        control = Host(decibel_data,control);  
    
        lev_rf_ifa_bk = lev_rf_ifa;
        lev_rf_ifa = rf_ifa_rom(control);        
         
        
        if(lev_rf_ifa_bk != lev_rf_ifa){
          
             WriteToMax14662(0x0, lev_rf_ifa); //CS0 GAIN_D, 0x3F-max gain
             WriteToMax14662(0x1, lev_rf_ifa); //CS1 GAIN_C, 0x3F-max gain 
             WriteToMax14662(0x2, lev_rf_ifa);  //CS2 GAIN_B, 0x3F-max gain 
             WriteToMax14662(0x3, lev_rf_ifa);  //CS3 GAIN_A, 0x3F-max gain
            
           
           decibel_data = decibel_data+1;
           decibel_data = decibel_data-1;
           
           control = control +1;
           control = control -1;
           
           lev_rf_ifa = lev_rf_ifa +1;
           lev_rf_ifa = lev_rf_ifa -1;             
           
       }
        else{
          lev_rf_ifa = lev_rf_ifa+1;
          lev_rf_ifa = lev_rf_ifa-1;
          
          decibel_data = decibel_data+1;
          decibel_data = decibel_data-1;
        }
      
//    } // if (!rd_mask) { 
    AK[REG_CONTRL] = 0;     // Clear REG_Auto_start & REG_Enable
    //return 1;
}  

//-----------------------------------------------------------------------------

uint8_t ATT_level0 = 15;



/******************************************************
**                  End Of File
*******************************************************/