#include <string.h>
#include "MCU32.h"
#include "wake.h"
#include "ak.h"
/************************************************************************************
*                                CONSTANTS
*************************************************************************************/

/************************************************************************************
*                                VARIABLES
*************************************************************************************/
extern WakePackageT wake_UART_1_rx;
int RxWakeTimer;

//int Tics_2kHz = 0;  // ���� ���������� 2 ���
int SysTickFlg = 0;  // ���� ������� �� ���������� �������
//int Cnt10ms = 0;    // ������� 0..19 ���������� 2 ���
//int Cnt40ms = 0;    // ������� 0..3 ����� �������� 25 ��

//extern int AGC_on;

unsigned int sys_tics = 0;             // ����� �� ������ �������� ������ � SysTickFlg
uint32_t prog_status, prog_cntrl;
uint32_t AK_off_timer = 0;


extern uint32_t trimming_step;
//extern uint32_t trimming_err;
extern uint16_t trimming_pause;
extern uint8_t ifa_agc_lev[];

void WriteToMax14662(uint8_t chan,  uint8_t dat) ;
/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/

extern void HW_Init(void);
extern uint8_t rd_RFIC(uint8_t rfic, uint8_t reg);
extern int init_CLOCK_PLL(void);
extern void change_main_clock(int);
//void CPU_InitTick (void);
extern void sppStartDataProcessing(int chan);
//int soft_SPI(void);
extern void WakeTxStart(WakePackageT * package);
//extern void CheckRxWake(void);
extern void WakeReadInit(WakePackageT * package);
//extern void  USART1_Send (void);
//extern void  USART2_Rcv (void); 
//extern void init_host_rx(void); 
//extern void init_DAC(void);
extern void initAK(void);
//extern void init_ds_rx(void);

extern void asc_host(WakePackageT * package);
//extern void send_to_host(uint8_t bt);
//extern void init_board_rx(void);
extern void trimming(void);
//extern void send_registrator_data(void);
extern void read_flash_id(void);
extern void write_flash_image(void);
int trim_ADC_1056(int num);
void RF_AGC(void);

void CheckRegistratorData();

extern void CalibrTonOff(void);
extern void RESTART(void);
extern int init_MPLL(void);
extern int trim_ADC(int num);
extern void HSI_Init(void);

extern void Init_data_vector(void);
extern void Init_coeff_vector(void);

/*************************************************************************************
*                                  main()
* Arguments   : none
**************************************************************************************/

int main()
{

int init_error = 0x7, init_try = 5;
volatile int *GPIO = (volatile int*) GPIO_BASE;
//int i;
  //  setlocale (LC_ALL, "C"); // ������������� stdio �������
volatile int *portNVIC_ISER = (volatile int *)NVIC_ISER; //0xe000e100;  
volatile int *portNVIC_IPR1 = (volatile int *)NVIC_IPR1;
volatile int* UART1 = (volatile int *)UART1_BASE;

  sys_tics = 0;
  SysTickFlg = 0;
  prog_status = TRIMMING_ADC1_FAIL | TRIMMING_ADC0_FAIL; // status word
//  prog_cntrl = RF_AGC_ON; // | RF_AGC_MANUAL;
  prog_cntrl = ATT_ENABLE;
//  AGC_on = 0; 
  trimming_step = 0;
  trimming_pause = 0;

  HW_Init();  // �������������, ���������� ����������


  HSI_Init(); 

  change_main_clock(SRC_HF_CLK);  
  
  initAK(); 
  

/////////������ MODEs ��� 1069-�� �  GAIN_A_B_C_D
  GPIO[GPIO_ALT_1] &= (~0x30000000);  // pin 61,60 set as GPIO
  GPIO[GPIO_DIR_1] |= 0x30000000;     // pin 61,60 set as Output
  GPIO[GPIO_OUT_1]  = 0x0; // �������� pin 61,60(CS)  ��� MODE_CD � MODE_AB  
  WriteToMax14662(0x0,0xff); //������ �������� �������� �� ������, ��� ��� CS ��� ����������  
  GPIO[GPIO_OUT_1]  = 0x30000000;  // ���������  pin 61,60(CS) ��� MODE_CD � MODE_AB
    
  WriteToMax14662(0x0, 0x3F); //CS0 GAIN_D, 0x3F-max gain
  WriteToMax14662(0x1, 0x3F); //CS1 GAIN_C, 0x3F-max gain 
  WriteToMax14662(0x2,0x3F);  //CS2 GAIN_B, 0x3F-max gain 
  WriteToMax14662(0x3,0x3F);  //CS3 GAIN_A, 0x3F-max gain
/////////  

  
  Init_data_vector();
  Init_coeff_vector();
  
  WakeReadInit(&wake_UART_1_rx);
  *portNVIC_IPR1 = 0x00000001;                // UART_1_priority
  *portNVIC_ISER = USART1_IRQ_Enable;         // set UART_1_INT 
  
  
//CheckRegistratorData();

while (!SysTickFlg); 
SysTickFlg = 0;  

if(init_MPLL()) 
  prog_status |= INIT_MODUL_FAIL; 
 

GPIO[GPIO_ALT_1]   = 0x030f7f00;  /////////////////////////
  
while (!SysTickFlg); 
SysTickFlg = 0;
//1058 handshake
GPIO[GPIO_ALT_1] |= 0x01000000;    // uart1 (tx_only)
GPIO[GPIO_DIR_1] &= ~(0x01000000);   // clear "out" direction for txd_1

int count = 0;
  
while (1) {
    if (SysTickFlg) {
      SysTickFlg = 0;
      sys_tics++;
      if (AK_off_timer) AK_off_timer--;
      //if ((sys_tics&0x3f) == 0) RF_AGC();
      
      //----------------------
      if ( count == 1024) {
        CheckRegistratorData(); 
        count = 0;
      }
      else count++;
      //---------------------
 
      if (prog_cntrl & RESTART_ALL)      // RESTART ALL  
        RESTART();
      
//      if ((sys_tics&0x3) == 0) {  // 100 Hz
      if ((sys_tics&0xf) == 0) {  // 400 Hz
        if (prog_cntrl & WR_FLASH_IMAGE) 
          write_flash_image();
      }
      if (RxWakeTimer) {
        RxWakeTimer--;
        if (RxWakeTimer <= 0) {
          wake_UART_1_rx._status |= WAKE_READ_TIMEOUT;
          UART1[UARTINTMASK] &= ~0x2;    // clear RXINT
          RxWakeTimer = 0;
        }    
      }  // (RxWakeTimer) {
    }  // if (SysTickFlg) {
    
    if ((wake_UART_1_rx._status & WAKE_RESV_PACKET) || (wake_UART_1_rx._status & WAKE_READ_TIMEOUT)) {  // received HOST command
      asc_host(&wake_UART_1_rx);
      WakeReadInit(&wake_UART_1_rx);
    }  
  }  // while (1) {
    return 0;
}
/*********************************************************************
*                            End Of File
**********************************************************************/
