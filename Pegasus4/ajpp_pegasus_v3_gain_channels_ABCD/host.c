#include "MCU32.h"
#include "AK.h"
#include "wake.h"
#include "stdlib.h"
#include "math.h"


int Host(uint16_t data, int temp){
   
  //int ref = 700;        //90dB * 4 = 360
  //int ref = 500;        //90dB * 4 = 360
  //int ref = 360;        //90dB * 4 = 360 
  int ref = 170;        //90dB * 4 = 360
  int det = data*4;  
  int mism = ref -  det;
  int mism_magn =(uint16_t) abs(mism)>>2;  
  uint16_t mism_sing = (mism>>13) & 0x1;
  
  uint16_t mism_dz, mism_dz_limit;
  uint16_t dz= 20;
  //uint16_t dz= 65;
  //uint16_t dz= 90;
  //uint16_t dz= 10;
  int max_step = 10;     //10dB * 4       

//////STAGE 1
  
  if (mism_magn > dz)
    mism_dz = mism_magn - dz;
  else
    mism_dz = 0;
  
  if(mism_dz > max_step)
    mism_dz_limit = max_step;
  else
    mism_dz_limit = mism_dz;
  
//////STAGE 2
 
  if(mism_sing == 0x1)
   temp = temp - mism_dz_limit;
  else
   temp = temp + mism_dz_limit;

//////STAGE 3  
 
  //int gain_control;
  int max_control =300;

  //temp = temp + step;
  
  if(temp<0)
    temp = 0;
  if(temp>max_control-1)
    temp = max_control-1;
  
  //gain_control = (int)temp;
  
  return temp;
 }


