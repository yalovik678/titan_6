#include "MCU32.h"
#include "AK.h"
#include "spi.h"
#include "wake.h"

#define FLASH_SPI SPI3_BASE

//extern AK_dataT AK_data;
extern uint32_t prog_status, prog_cntrl;
unsigned dac_data[8];
uint8_t trim_adc1, trim_adc2;
extern unsigned char inf_buf[];

uint8_t dmix[9] = {0x4d, 0x80, 0x80, 0x80, 0x80, 16, 0x50, 0x06, 0x00};  //1506-1590 MHz
uint8_t dpll[15] = {0x00, 0x04, 0x04, 0x39, 0x6c, 0x11, 0x51, 0x3e, 0x23, 0xd7, 0x10, 0x9a, 0x0d, 0x00, 0x01};

/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/
int CheckRegistratorConst(uint16_t out_const, uint8_t chan_in_bit);
//void send_DAC(void);
void change_main_clock(int src);
/************************************************************************************
*                                void InitSPI(void)
*************************************************************************************/
void InitSPI(void)
{
volatile int* SPI1 = (volatile int *)SPI1_BASE;
volatile int* SPI2 = (volatile int *)SPI2_BASE;
volatile int* SPI3 = (volatile int *)SPI3_BASE;

// ----------------------------------------------------------------------- SPI 1 --
SPI1[SSCR0] = 0x091F; // 32-bits, CPU_CLK / 20
SPI1[SSCR1] = 0x2488;
// ----------------------------------------------------------------------- SPI 2 --
SPI2[SSCR0] = 0x020f;     // 16- ��� ������, CPU_CLK / 6
//SPI3[SSCR0] = 0x040f;     // 16- ��� ������, CPU_CLK / 10
//SPI3[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
SPI2[SSCR1] = 0x0008;     // enable
// ----------------------------------------------------------------------- SPI 3 --
SPI3[SSCR0] = 0x040F;     // 16- ��� ������, CPU_CLK / 10
SPI3[SSCR1] = 0x0008;     // enable
}

//------------------------------------------------------------------------ RFIC --
void wr_RFIC(uint8_t rfic, uint8_t reg, uint8_t data) 
{
  volatile int* SPI = (volatile int *)SPI2_BASE;
#ifdef TITAN_V4 
  rfic =~rfic;
#endif  
  SPI[SSCR0] = 0x020f;     // 16- ��� ������, CPU_CLK / 6
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((rfic&0x1)<<6) + 0x448;     // ������ ��������, CS23, CS10, enable
  SPI[SSDR] = reg & 0x7F; SPI[SSDR] = data;
}

uint8_t rd_RFIC(uint8_t rfic, uint8_t reg) 
{
  volatile int* SPI = (volatile int *)SPI2_BASE;
  uint8_t bt; 
#ifdef TITAN_V4 
  rfic =~rfic;
#endif  
  SPI[SSCR0] = 0x020f;     // 16- ��� ������, CPU_CLK / 6
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((rfic&0x1)<<6) + 0x48;     // CS23, CS10, enable
  SPI[SSDR] = reg | 0x80; SPI[SSDR] = 0x0;
  while (SPI[SSSR]&0x8); // BSY
  bt = SPI[SSDR]; 
  bt = SPI[SSDR];
  return bt;
}

int init_RFIC(uint8_t rfic) 
{
//volatile int* GPIO = (volatile int *)GPIO_BASE;  
uint8_t bt, R43;  
uint8_t* ptr;
//int cnflen;

#ifndef DA14_DA16 //DA6, DA8
#ifdef XCH_RFIC
  //if(rfic == 0) ptr = conf_RFIC_L1_GPS_2480;
  //else 
#if (SW_VERSION == 205) // Vco1580  
    //ptr = conf_RFIC_L1_GPS_2480;
#else    
    //ptr = conf_RFIC_L1_GPS_2480;
#endif
#else
  if(rfic) ptr = conf_RFIC_L1_GPS_2480;
  else ptr = conf_RFIC_L1_GPS_2480;
#endif   
#endif
  
 
  if (rd_RFIC(rfic, 0) != 0x21) return 1;
  if (rd_RFIC(rfic, 1) != 0x61) return 1;
  
  for (int i = 0; i < sizeof(conf_RFIC_L1_GPS_1590); i+=2)
    wr_RFIC(rfic, ptr[i], ptr[i + 1]);

  for (int i = 0; i < sizeof(conf_RFIC_L1_GPS_1590); i+=2) {
    bt = ptr[i + 1];
    if (ptr[i] == 43) R43 = bt;
    if (rd_RFIC(rfic, ptr[i]) != bt)  
      return 1;
  } 
  wr_RFIC(rfic, 43, R43 | 0x1); // PLL_A start 
  for (int i = 0; i < 10; i++) {
    if ((rd_RFIC(rfic,44) & 0x1) == 0x1) {  // PLL_A lock ?
      wr_RFIC(rfic, 4, 0x1); // LPF auto-calibration start
      for (int j = 0; j < 10000; j++) 
        if ((rd_RFIC(rfic,4) & 0x3) == 0x2) // LPF auto-calibration system status
          return 0;
      return 1;
    }  
  }  
  return 1;
}  

//----------------------------------------------------------------- SPI ADC ----

uint8_t ReadFromADC(uint8_t chan, uint8_t adr)
{
volatile int* SPI = (volatile int *)SPI2_BASE;  
uint8_t bt;
#ifdef TITAN_V4 
  chan =~chan;
#endif
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0008;     // ������� 
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x4008;     // �������, hold CS
  SPI[SSDR] = (adr&0x7F)|0x80; SPI[SSDR] = 0x0; 
  while (SPI[SSSR]&0x8); // BSY
  bt = SPI[SSDR]; 
  bt = SPI[SSDR];
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0008;     // �������
  return bt;
}

void WriteToADC(uint8_t chan, uint8_t adr, uint8_t dat) 
{
volatile int* SPI = (volatile int *)SPI2_BASE;  
#ifdef TITAN_V4 
  chan =~chan;
#endif
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0408;     // ������ ��������, �������
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x4408;     // ������ ��������, �������, hold CS
  SPI[SSDR] = adr&0x7F; SPI[SSDR] = dat; 
  while ((SPI[SSSR]&0x1) == 0); // TFE  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0408;     // ������ ��������, �������, clear CS
}

int init_ADC(int adc) {
  
  WriteToADC(adc, 0, 0x80);          // Reset
  WriteToADC(adc, 1, 0x20);          // TWOSCOMP, Normal Operation
//  WriteToADC(adc, 1, 0x00);          // Normal Operation
//  WriteToADC(adc, 2, 0xE6);          // 1.75mA LVDS Output Driver Current, Internal Termination Off, Digital Outputs are Enabled, 110 = 1-Lane, 12-Bit Serialization 
  WriteToADC(adc, 2, 0xE2);          // 1.75mA LVDS Output Driver Current, Internal Termination Off, Digital Outputs are Enabled, 010 = 2-Lane, 12-Bit Serialization 
//  WriteToADC(adc, 2, 0x02);
  WriteToADC(adc, 3, 0xAA);          //  OUTTEST,  0xAA5 
  WriteToADC(adc, 4, 0x94);
  if (ReadFromADC(adc, 3) != 0xAA) return 1;
  if (ReadFromADC(adc, 2) != 0xE2) return 1;
  return 0;
}

int trim_ADC(int num) {
uint32_t temp;
int ret = 1;
volatile int* AK = (volatile int *)AK_BASE_ADR;  

  temp =  AK[COMMON]; // = 0x01900; 
  if (num) AK[COMMON] = temp | 0x100; // band0 < ADC1
  else AK[COMMON] = temp & ~(0x100); // band0 < ADC0 
  
  WriteToADC(num, 3, 0xAA);          //  OUTTEST,  0xAA5 
  WriteToADC(num, 4, 0x94);
  if (!CheckRegistratorConst(0xAA5, num)) ret = 0;
  if (ret) {
    WriteToADC(num, 3, 0x95);          //  OUTTEST,  0x55A 
    WriteToADC(num, 4, 0x68);
    if (!CheckRegistratorConst(0x55A, num)) ret = 0;
  } 
  WriteToADC(num, 3, 0x0);          //  OUTTEST off
  AK[COMMON] = temp;
  return 1;
}

void WriteToMax14662(uint8_t chan,  uint8_t dat) 
{
volatile int* SPI = (volatile int *)SPI2_BASE; 

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
  //SPI[SSCR0] = 0x0908;     // 8- ��� ������, CPU_CLK / 20  
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0408;     // ������ ��������, �������
  SPI[SSCR1] = ((chan)<<6) + 0x4408;     // ������ ��������, �������, hold CS(��������� ����� CS �������� ����� ���������� ������)
  SPI[SSDR] = 0x00; //�����(�������� �� �����)
  SPI[SSDR] = dat; 
  while ((SPI[SSSR]&0x1) == 0); // TFE  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0408;     // ������ ��������, �������, clear CS
}

//------------------------------------------------------------------------------
uint8_t ReadFromMIX(uint8_t adr)
{
volatile int* SPI = (volatile int *)SPI1_BASE; 
uint8_t res;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x088; //| ((adr&0x80)>>1);     // �������, CS2  
  SPI[SSDR] = ((adr&0x3F)<< 1)|0x1; SPI[SSDR] = 0x0; 
  while (SPI[SSSR]&0x8); // BSY
  res = SPI[SSDR]; 
  res = SPI[SSDR];
  return res & 0xff;
}

void WriteToMIX(uint8_t adr, uint8_t dat) 
{
  volatile int* SPI = (volatile int *)SPI1_BASE;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR1] = 0x0488;// | ((adr&0x80)>>1);     // ������ ��������, �������, CS2
  SPI[SSDR] = ((adr&0x3F)<< 1); SPI[SSDR] = dat; 
}  


uint32_t ReadFromMPLL(uint8_t adr)
{
  volatile int* SPI = (volatile int *)SPI1_BASE; 
  uint8_t res;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x008; //| ((adr&0x80)>>1);     // �������, CS0 , CS aktiv low   
  SPI[SSDR] = ((adr&0x3F)<< 1)|0x1; SPI[SSDR] = 0x0; 
  while (SPI[SSSR]&0x8); // BSY
  res = SPI[SSDR]; 
  res = SPI[SSDR];
  return res & 0xff;
}

void WriteToMPLL(uint8_t adr, uint32_t dat) 
{
  volatile int* SPI = (volatile int *)SPI1_BASE;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR1] = 0x408; //| ((adr&0x80)>>1);     //������ ��������, �������, CS0, CS aktiv low 
  SPI[SSDR] = ((adr&0x3F)<< 1); 
  SPI[SSDR] = dat; 
}  

int init_MPLL(int num_Pll) 
{
volatile int* SPI = (volatile int *)SPI1_BASE;
volatile int* GPIO = (volatile int *)GPIO_BASE;

while (SPI[SSSR]&0x8); // BSY  
SPI[SSCR0] = 0x090F; // 16-bits, CPU_CLK / 20
SPI[SSCR1] = 0x408;     // ������ ��������, �������, CS0, CS aktiv low
GPIO[GPIO_ALT_1]  |= 0x2F;        // �������� spi1 (cs1_0 and cs1_2)

dpll[7] = 0x3E;
dpll[6] = 0x29;
dpll[11] = 0x82;


for (int i = 0; i < 15; i++) {
  WriteToMPLL(0x80+i, dpll[i]);
}

//for (int i = 0; i < 15; i++) {
//  if(i==6)
//  ReadFromMPLL(0x80+i);
//}

// --------------------------------------------------------- Mixer ---
//set Mod_EN_57, enebele for LTC5589
GPIO[GPIO_ALT_1] &= (~0x10);  
GPIO[GPIO_DIR_1]  |= 0x10;      
GPIO[GPIO_OUT_1] = 0x0010; 

SPI[SSCR0] = 0x090F; // 16-bits, CPU_CLK / 20

for (int i = 0; i < 9; i++) {
  WriteToMIX(0x80+i, dmix[i]);  
}

for (int i = 2; i < 9; i++) {
   ReadFromMIX(0x80+i)  ;
}
//return 0;
} 
//--------------------------------------------------------------------
//--------------------------------------------------------------------
uint32_t FlashReadSectorProtection(uint32_t ulDstAddr)
{
volatile int* SPI = (volatile int *)FLASH_SPI;

int i;
uint32_t ucStatus;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x4008;          // HOLDCS 
  SPI[SSDR] = 0x3c;     // Read Sector Protection Register
  // Transfer the address 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
  while (SPI[SSSR]&0x8); // BSY
//  while (SPI[SSSR]&0x4) (void)SPI[SSDR];  // read dummy
  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR];
  SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR];
  SPI[SSCR1] = 0x0408;          // clear HOLDCS
  ucStatus = dac_data[i-1];
  return ucStatus;
}

void read_flash_id(void)
{
volatile int* SPI = (volatile int *)FLASH_SPI;
//uint8_t answ[8];
int i;

  SPI[SSCR0] = 0x021f;         // 32-bit
  SPI[SSCR1] = 0x0008;
  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x9F;  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; //SPI[SSDR] = 0; 
  while (SPI[SSSR]&0x8); // BSY
  i = 0;
  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR]; 
 // while (SPI[SSSR]&0x8); // BSY  
 // SPI[SSDR] = 0x03;  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; //SPI[SSDR] = 0; SPI[SSDR] = 0;
 // while (SPI[SSSR]&0x8); // BSY
 // i = 0;
 // while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR]; 
  return;
}
//--------------------------------------------------------------------
//*****************************************************************************
//      param None
//      This function is to Read SST25VFxx Status Register:the bit field follows:
//      BIT  7    6   5   4   3,2   1     0 
//          SPRL SPM EPE WPP  SWP  WEL RDY/BSY
//      return the value of status
//*****************************************************************************
uint32_t FlashStatusRegRead(void)
{   
  uint32_t ucStatus;
  volatile int* SPI = (volatile int *)FLASH_SPI;
//  int i;
  
  while (SPI[SSSR]&0x8); // BSY  
//  SPI[SSCR0] = 0x021f;         // 32-bit
  SPI[SSCR0] = 0x020f;         // 16-bit
  SPI[SSCR1] = 0x0008;
  SPI[SSDR] = 0x05;  SPI[SSDR] = 0;  
//      SPI[SSDR] = 0;  SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
//  i = 0;
//  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR]; 
  ucStatus = SPI[SSDR] << 8;
  ucStatus += SPI[SSDR]; 
//  ucStatus = ucStatus << 8; ucStatus += SPI[SSDR];
//  ucStatus = ucStatus << 8; ucStatus += SPI[SSDR];
  return ucStatus;    
}

//*****************************************************************************
//      param None
//      This function is to Wait for SST25VFxx is not busy
//      return None
//*****************************************************************************
void FlashWaitNotBusy(void)
{    
    //Wait the bit of busy is clear
    while((FlashStatusRegRead() & 0x01) == 0x01);
}

//*****************************************************************************
//      param None
//      This function is to enable the function 0f writing
//      return none
//*****************************************************************************
void FlashWriteEnable(void)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;
  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x0408;          // ������ ��������
  SPI[SSDR] = 0x06;   
  while (SPI[SSSR]&0x8); // BSY
}

//*****************************************************************************
//      param None
//      This function is to disable the function 0f writing
//      return none
//*****************************************************************************
void FlashWriteDisable(void)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x0408;          // ������ ��������
  SPI[SSDR] = 0x04;   
  while (SPI[SSSR]&0x8); // BSY
}

//*****************************************************************************
//      Erase all chip
//      param None
//      return none
//*****************************************************************************
void FlashChipErase(void)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Chip Erase
//  SPI[SSCR0] = 0x0207;   // 8-bit
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x60;   
//  while (SPI[SSSR]&0x8); // BSY  
  FlashWaitNotBusy();
}

//*****************************************************************************
//      Block Erase (4 Kbytes)
//      param ulDstAddr specifies the sector address which will be erased.
//      return none
//*****************************************************************************
void FlashSectorErase(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (4 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x20;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}

//*****************************************************************************
//      Erase a 32k Block
//      param ulDstAddr specifies the 32k Block address which will be erased.
//      return none
//*****************************************************************************
void FlashBlock32Erase(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (32 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x52;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}

//*****************************************************************************
//      brief Erase a 64k Block
//      param ulDstAddr specifies the 64k Block address which will be erased.
//      return none
//*****************************************************************************
void FlashBlock64Erase(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (64 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0xD8;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}
void FlashUnprotectSector(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (64 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x39;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}
//--------------------------------------------------------------------
void write_flash_image(void)
{
volatile int* SPI = (volatile int *)FLASH_SPI;
volatile int* AK = (volatile int *)AK_BASE_ADR;
volatile unsigned char* PTR = (unsigned char *)(0);
uint32_t adr = 0;
uint32_t image_size = 16384;
uint32_t sum = (unsigned char)(image_size >> 8) + (unsigned char)(image_size >> 0);
uint32_t rsum = 0, rw = 0;;
uint8_t bt;

  prog_status |= WR_FLASH_ERR;
  if (prog_cntrl & UPDATE_FLASH) PTR = (unsigned char *)(0x4000);
  prog_cntrl &= ~(WR_FLASH_IMAGE | UPDATE_FLASH);
  if ((AK[IMMIT_FRQ] != 0xA5A55A5A)) return;
  FlashUnprotectSector(0);
  FlashBlock32Erase(0);
  
  FlashWriteEnable();
  SPI[SSCR1] = 0x4408;          // ������ ��������, HOLDCS  
  SPI[SSDR] = 0x02;     // Write command
  // Transfer the address 
  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
  // Transfer count
  while ((SPI[SSSR]&0x10) == 0); // TFS
  SPI[SSDR] = (unsigned char)(image_size >> 0);
  SPI[SSDR] = (unsigned char)(image_size >> 8);
  SPI[SSDR] = 0; SPI[SSDR] = 0;
  // Transfer data 
  for (int i = 4; i < 256; i++) {
    while ((SPI[SSSR]&0x10) == 0); // TFS
    bt = PTR[adr++];
    sum += bt;
    SPI[SSDR] = bt;
  }  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x0408;          // clear HOLDCS
  FlashWaitNotBusy();
  
  for (int k = 1; k < image_size/256; k++) {
    FlashWriteEnable();
    SPI[SSCR1] = 0x4408;          // ������ ��������, HOLDCS  
    SPI[SSDR] = 0x02;     // Write command
    // Transfer the address 
    SPI[SSDR] = 0; SPI[SSDR] = k; SPI[SSDR] = 0;
    // Transfer data 
    for (int i = 0; i < 256; i++) {
      while ((SPI[SSSR]&0x10) == 0); // TFS
      bt = PTR[adr++];
      sum += bt;
      SPI[SSDR] = bt;
    }  
    while (SPI[SSSR]&0x8); // BSY
    SPI[SSCR1] = 0x0408;          // clear HOLDCS
    FlashWaitNotBusy();  
  } 
  
// ����������� ������
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x4008;          // HOLDCS 
  SPI[SSDR] = 0x03;     // read command
  // Transfer the address 
  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
  while (SPI[SSSR]&0x4) (void)SPI[SSDR];  // read dummy
  // read data 
  for (int i = 0; i < image_size/4; i++) {
      SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
      while (SPI[SSSR]&0x8); // BSY
      while (SPI[SSSR]&0x4) { 
        bt = SPI[SSDR];
        rsum += bt; 
        rw++; 
      }
  }  
  SPI[SSCR1] = 0x0408;          // clear HOLDCS
  if (sum == rsum) prog_status &= ~WR_FLASH_ERR;
}

uint32_t read_flash_image(uint32_t image_size)
{
volatile int* SPI = (volatile int *)FLASH_SPI;
uint32_t rsum = 0;
uint8_t bt;
 
  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x4008;          // HOLDCS 
  SPI[SSDR] = 0x03;     // read command
  // Transfer the address 
  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
  while (SPI[SSSR]&0x4) (void)SPI[SSDR];  // read dummy
  // read data 
  for (int i = 0; i < image_size/4; i++) {
      SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
      while (SPI[SSSR]&0x8); // BSY
      while (SPI[SSSR]&0x4) { 
        bt = SPI[SSDR];
        rsum += bt; 
      }
  }  
  SPI[SSCR1] = 0x0408;          // clear HOLDCS  
  return rsum;
}
//--------------------------------------------------------------------