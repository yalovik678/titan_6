#ifndef AK_H
#define AK_H

//#define AK_BASE_ADR 0x8008E000
  #define REVISION	0       //      R/o	X"20141007"	������ �������
  #define COMMON	6       //      RW	0	����� ���������, �������� Q, ���������� �������
  #define TBI_CONFIG_1	7       //      RW	0	������������ ��������������� � ���������� ���. �������� ��������������.
  #define TBI_CONFIG_4	10       //      RW	X"000000F7"	������������ ��������������� � ���������� ���. ���������� �������� � �������������� ����������.
  #define TBI_CONFIG_5	11       //      RW	0	������������ ��������������� � ���������� ���. �������� �������������� � ������� ������ ���������� ������.
  #define TBI_REF1	12       //      R/o		������� ������ ��������������� � ���������� ��� (������������ 1 � 2). 
  #define TBI_REF3	14       //      R/o		������� ������ ��������������� � ���������� ��� (��������� �����).

  #define OVERFLOW_ST	28       //      R/o		������� ���������� ������������ � ������ �����������������.
  #define AK_CONFIG_1	29       //      RW	X"00A63A0E"	��������� �������� ���������� ������ ����������������. ����� ������ ����������������.
  #define AK_CONFIG_2	30       //      RW	0	��������� ������� ���������� � ��� ����������������.

  #define REG_INDEX	32       //      RW	0	��������� ���� ������������. ������ � ���� ���� �������� ��������� ������ ������ ������������. �� ������ �������� ������ �� ����� ����������� ������ �� ������ ������������.
  #define REG_SEL	33       //      RW	0	��������� ������� ����������� ������������
  #define REG_CONTRL	34       //      RW	0	��������� � ���������� ������� ������������
  #define IF_FIR	35       //      RW	0			
  #define FRQ_BAND1	36       //      RW	X"B61B352D"	��� ������� ��������� 1 (GPS)
  #define FRQ_BAND2	37       //      RW	X"337C156F"	��� ������� ��������� 2 (GLO1)
  #define FRQ_BAND3	38       //      RW	X"47C4A33F"	��� ������� ��������� 3 (GLO2)
  #define FRQ_BAND4	39       //      RW	X"47C4A33F"	��� ������� ��������� 4 (BeiDou)
  #define FRQ_OUT_BAND1	40       //      RW	X"B61B352D"	��� ������� ����������� ��������� 1 (GPS)
  #define FRQ_OUT_BAND2	41       //      RW	X"337C156F"	��� ������� ����������� ��������� 2 (GLO1)
  #define FRQ_OUT_BAND3	42       //      RW	X"47C4A33F"	��� ������� ����������� ��������� 3 (GLO2)
  #define FRQ_OUT_BAND4	43       //      RW	X"47C4A33F"	��� ������� ����������� ��������� 4 (BeiDou)

  #define CX_W_AK_CH1   44      // ����������� ����������� ���������������� ������ 1
  #define CX_W_AK_CH2   45      // ����������� ����������� ���������������� ������ 2
  #define CX_W_AK_CH3   46      // ����������� ����������� ���������������� ������ 3

  #define IMMIT_CNTRL	50
  #define IMMIT_FRQ		51

  #define ANLG_CNFG_1	144
  #define ANLG_CNFG_2	145
  #define ANLG_CNFG_3	146
  #define ANLG_CNFG_4	147

/*typedef struct {
    uint32_t R[43];  
//    SYN_dataT SYN1_data;
//    SYN_dataT SYN2_data;   
} AK_dataT;*/

typedef struct {
//    uint32_t syn1_R0;     
//    uint32_t syn1_R1;  
//    uint32_t syn1_R2;
    uint32_t syn_cal_R0;     
    uint32_t syn_cal_R1;  
    uint32_t syn_cal_R2;
    uint32_t PLL_R0;     
    uint32_t PLL_R1;  
    uint32_t PLL_R2;
//    uint32_t state; 
} SYN_dataT;

//CALIBRATION
// trimming_steps
  #define LOAD_DEFAULT_TRIMM    1
  #define REMOVE_DC_TRIMM       10
  #define BAND1_IQ_TRIMM        20
  #define TONE_ON_TRIMM         40
  #define TONE_OFF_TRIMM        50
  #define GLO1_IQ_TRIMM         60
  #define GLO2_IQ_TRIMM         80

#define IFA_THRESHOLD 6
#define AK_ON_IFA_THRESHOLD_LEV 0x10

#endif // AK_H
/******************************************************
**                  End Of File
*******************************************************/