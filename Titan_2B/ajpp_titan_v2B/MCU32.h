#ifndef MCU_32_H
#define MCU_32_H

#define int16_t short
#define uint16_t unsigned short
#define int32_t int
#define uint32_t unsigned int
#define uint8_t unsigned char
//==================================================================================================
//===================================== TITAN_V2B (NT1057.2) =======================================
//==================================================================================================
//#define DA14_DA16  //L2_VERSION

#ifndef DA14_DA16
#define IDT5P49V5901A
#define SW_VERSION 211  
//#define XCH_L1_GPS_GLO
#else
#define SW_VERSION 221   // L2
#endif

#define RC_CLK 5500000

#define CPU_CLK 53000000

#define ADC_NUM      2

// NVIC
#define NVIC_ISER 0xE000E100 // RW 0x00000000 Interrupt Set-Enable Register, NVIC_ISER
  #define SPI3_IRQ_Enable       0x02                   // SPI3
  #define SPI2_IRQ_Enable       0x04                   // SPI2
  #define SPI1_IRQ_Enable       0x08                   // SPI1 
  #define USART1_IRQ_Enable     0x10                 // USART1 
  #define IIC_IRQ_Enable        0x80                 // I2C

  #define EXTI0_IRQ_Enable      0x100                  ; EXTI Line0                                             
  #define EXTI1_IRQ_Enable      0x200                  ; EXTI Line1                                             
  #define EXTI2_IRQ_Enable      0x400                  ; EXTI Line2                                             
  #define EXTI3_IRQ_Enable      0x800                  ; EXTI Line3                                             
  #define EXTI4_IRQ_Enable      0x1000                 ; EXTI Line4
  #define EXTI5_IRQ_Enable      0x2000                 ; EXTI Line2                                             
  #define EXTI6_IRQ_Enable      0x4000                 ; EXTI Line3 
#define NVIC_ICER 0xE000E180 // RW 0x00000000 Interrupt Clear Enable Register, NVIC_ICER
#define NVIC_ISPR 0xE000E200 // RW 0x00000000 Interrupt Set-Pending Register, NVIC_ISPR
#define NVIC_ICPR 0xE000E280 // RW 0x00000000 Interrupt Clear-Pending Register, NVIC_ICPR on
#define NVIC_IPR0 0xE000E400 // RW 0x00000000 Interrupt Priority Register 0
#define NVIC_IPR1 0xE000E404 // RW 0x00000000 Interrupt Priority Register 1
#define NVIC_IPR2 0xE000E408 // RW 0x00000000 Interrupt Priority Register 2
#define NVIC_IPR3 0xE000E40C // RW 0x00000000 Interrupt Priority Register 3

#define CLK_CNTRL_REG 0x80001040   // X"00000" & "00" & clk_src[0:1] & start_mode[0:1] & "0000" & pclk_div_c[0:1];
#define UART1_BASE 0x80081000           
    #define    UARTDR   0x000      // ������� ������
    #define    RXSTAT   0x004>>2      //    ������� ��������� ���������
    #define    H_UBRCR  0x008>>2      //    ������� ���������� �������(H)
    #define    M_UBRCR  0x00C>>2      //    ������� ���������� ��������� ������(M)
    #define    L_UBRCR  0x010>>2      //    ������� ���������� ��������� ������(L)
    #define    UARTCON  0x014>>2     //     ������� ����������
    #define    UARTFLG  0x018>>2      //    ������� ���������
    #define    UARTINTSTAT  0x01C>>2  //      ������� �������� ����������(masked)
    #define    UARTINTMASK  0x020>>2  //      ������� ������������ ����������
    #define    UARTINTSTATR     0x024>>2    //   ������� �������� ����������
#define SPI1_BASE 0x80021000
#define SPI2_BASE 0x80022000
#define SPI3_BASE 0x80023000
    #define    SSCR0   0x000            // ������� ����������  0
    #define    SSCR1   0x004>>2      //    ������� ����������  1
    #define    SSDR  0x008>>2      //    ������� ������
    #define    SSSR  0x00C>>2      //    ������� ���������
    #define    RX_CNT  0x010>>2      //    ������� ���������
#define IIC1_BASE 0x8008C000
    #define    TWB_DATA   0x000            
    #define    TWB_CMND   0x004>>2      
    #define    TWB_STAT  0x008>>2
    #define    TWB_CNFG  0x00C>>2  
#define GPIO_BASE 0x8008F000
    #define GPIO_DIR 0x008>>2
    #define GPIO_IN 0x004>>2
    #define GPIO_OUT 0x000
    #define GPIO_OUT_SET 0x00C>>2
    #define GPIO_OUT_CLR 0x010>>2
    #define GPIO_DIR_1 0x028>>2
    #define GPIO_IN_1 0x024>>2
    #define GPIO_OUT_1 0x020>>2
    #define GPIO_OUT_SET_1 0x02C>>2
    #define GPIO_OUT_CLR_1 0x030>>2
    #define GPIO_ALT_1 0x034>>2
    #define GPIO_ALT_2 0x038>>2
    #define GPIO_PUEN_1 0x03C>>2
#define AK_BASE_ADR 0x8008E000

#define SYST_CSR 0xE000E010  // RW  0x00000000 or 0x00000004 SysTick Control and Status Register, SYST_CSR
  #define COUNTFLAG 0x10000
  #define CLKSOURCE 0x4
  #define TICKINT 0x2
  #define ENABLE 0x1
#define SYST_RVR 0xE000E014 //RW UNKNOWN SysTick Reload Value Register, SYST_RVR 
#define SYST_CVR 0xE000E018  // RW UNKNOWN SysTick Current Value Register, SYST_CVR 
#define SYST_CALIB 0xE000E01C  // RO IMPLEMENTATION DEFINED SysTick Current Value Register, SYST_CVR 
#define SYST_AIRCR 0xE000ED0C  // SYSRESETREQ

#define UART_DEB_BASE 0x80001000           
    #define    SBUF   0x000      // ������� ������
    #define    SCONT   0x004>>2      //    ������� ����������
    // TX_Error_c & FE_PAR & Overrun & REN & (TX_busy_c or Tr_Data_Ready) & TR_Empty & Data_Ready
    #define    UBRG  0x008>>2      //    ������� ���������� ��������� ������
    #define    SCONF  0x014>>2     //     ������� ������������
    // ti_en & TI_source & Stop_Bits & brk_en & WithoutPresc & StepMode & Parity_En


typedef struct {
    unsigned int PLL_Clock;
    unsigned int CPU_Clock;
    unsigned int PERIF_Clock;
} PMU_clocks;

#define I2C_ACK 0
#define I2C_nACK 1

#define SRC_PLL_CLK     1
#define SRC_HF_CLK      2
#define SRC_RC_CLK      0

#endif // MCU_32_H
/******************************************************
**                  End Of File
*******************************************************/








