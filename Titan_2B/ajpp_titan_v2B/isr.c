#include "MCU32.h"
#include "wake.h"

/************************************************************************************
*                                VARIABLES
*************************************************************************************/
extern host_rx_dataT host_rx_data;

extern unsigned char uart1_tx_buf[];
extern uint32_t uart1_tx_buf_wr_ptr, uart1_tx_buf_rd_ptr;
extern uint32_t uart1_tx_buf_len;

extern WakePackageT wake_UART_1_rx;
extern WakePackageT wake_UART_1_tx;
extern int RxWakeTimer;

extern int SysTickFlg;  // ���� ������� �������
extern uint32_t prog_status;

/*********************************************************************
*                              IRQ HANDLER
* Arguments   : none
**********************************************************************/

void  USART1_IRQHandler (void)
{
volatile int *UART1 = (volatile int *)UART1_BASE;
unsigned char temp;

  if (UART1[UARTINTSTAT] & 0x1) { // TXINT
    if (uart1_tx_buf_len == 0) UART1[UARTINTMASK] &= ~(0x1);    // clear TXINT
    else {
      do {
        UART1[UARTDR] = uart1_tx_buf[uart1_tx_buf_rd_ptr];   // write to FIFO
        uart1_tx_buf_rd_ptr = (uart1_tx_buf_rd_ptr + 1); // & (HOST_TX_BUFF_SIZE-1);
        uart1_tx_buf_len--;
      } while ((UART1[UARTINTSTAT] & 0x1) && uart1_tx_buf_len);
      if (uart1_tx_buf_len == 0) UART1[UARTINTMASK] &= ~(0x1);    // clear TXINT
    }
  }  
  if (UART1[UARTINTSTAT] & 0x2) { // RXINT
//-------------------- ����� WAKE ��������� �� �� --------------------- start -
    do {
      temp = UART1[UARTDR];    
      if (wake_UART_1_rx._index == WAKE_FEND_INDEX) {
        if (temp == FEND) {
//        package->_crc = GetCRCWake(CRC_WAKE_INIT, byte);
          wake_UART_1_rx._fFESC = 0;
          wake_UART_1_rx._index = WAKE_ADDRESS_INDEX;
//        wake_UART_1_rx._status = WAKE_READ_IN_PROCESS;
      // init timer 500 ms    --  UART timeout
          RxWakeTimer = 50;     // 500 ms
        }
      } else { // package->_index
        // process unstuffing
        if (temp == FESC)  // if found FESC byte, then set flag _fFESC and wait next byte
          wake_UART_1_rx._fFESC = 1;
        else {
          if (wake_UART_1_rx._fFESC) { //if last byte was FESC, process unstuffing
            wake_UART_1_rx._fFESC = 0;
            switch (temp) {
              case TFEND:
                temp = FEND;
                break;
              case TFESC:
                temp = FESC;
                break;
              default: //in correct packege this is impposible
                wake_UART_1_rx._status = WAKE_READ_ERROR;
            } //  switch (temp) {
          }  // if (package->_fFESC)
          // process data
          switch (wake_UART_1_rx._index) {
            case WAKE_ADDRESS_INDEX:
              if (temp & 0x80) { //test first bit of address. If it's 0 then we have a command
                wake_UART_1_rx.address = temp & 0x7F; //switch off first bit of address
                wake_UART_1_rx._index = WAKE_COMMAND_INDEX;
              } else {        // command
                wake_UART_1_rx.address = 0x80;    // without address
                wake_UART_1_rx.command = temp;
                wake_UART_1_rx._index = WAKE_LENGTH_INDEX;
              }
              break;
            case WAKE_COMMAND_INDEX:
              if (temp & 0x80) { //in correct packege this is impposible
                wake_UART_1_rx._status = WAKE_READ_ERROR;
              } else {
                wake_UART_1_rx.command = temp;
                wake_UART_1_rx._index = WAKE_LENGTH_INDEX;
              }
              break;
            case WAKE_LENGTH_INDEX:
              if ((wake_UART_1_rx.command == WAKE_C_WrImg) && (temp == 0))
                wake_UART_1_rx.length = 256;
              else
                wake_UART_1_rx.length = temp;
              wake_UART_1_rx._index = WAKE_DATA_INDEX;
              break;
            default:
              if (wake_UART_1_rx._index != wake_UART_1_rx.length) { //if not all data bytes has been readed
                wake_UART_1_rx.data[wake_UART_1_rx._index & 0xff] = temp;
                wake_UART_1_rx._index++;
              } else {
                wake_UART_1_rx._crc = temp;
                wake_UART_1_rx._status |= WAKE_RESV_PACKET;
                UART1[UARTINTMASK] &= ~0x2;    // clear RXINT
                RxWakeTimer = 0;
                return;
              }
          }  // switch (wake_UART_1_rx._index) {
        }  // if (temp == FESC)
      } // if (wake_UART_1_rx._index
    } while (UART1[UARTINTSTAT] & 0x2);
//-------------------- ����� WAKE ��������� �� �� --------------------- end -
  } // if (UART1[UARTINTSTAT] & 0x2) {
}

//-----------------------------------------------------------------------------
/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  SysTickFlg = 1;
}
/******************************************************
**                  End Of File
*******************************************************/















