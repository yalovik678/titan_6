#include "MCU32.h"
#include "wake.h"
#include "AK.h"
//#include <string.h>
#include <stdio.h>

/************************************************************************************
*                                CONSTANTS
*************************************************************************************/

/************************************************************************************
*                                VARIABLES
*************************************************************************************/
WakePackageT wake_UART_1_rx;
WakePackageT wake_UART_1_tx;
unsigned char uart1_tx_buf[HOST_TX_BUFF_SIZE];
uint32_t uart1_tx_buf_wr_ptr, uart1_tx_buf_rd_ptr;
uint32_t uart1_tx_buf_len;

host_rx_dataT host_rx_data;
/*unsigned char uart2_tx_buf[DS_RX_BUFF_SIZE];
uint32_t uart2_tx_buf_wr_ptr, uart2_tx_buf_rd_ptr;
uint32_t uart2_tx_buf_len; */

unsigned char uart1_tx_buf[HOST_TX_BUFF_SIZE];
uint32_t uart1_tx_buf_wr_ptr, uart1_tx_buf_rd_ptr;
uint32_t uart1_tx_buf_len;

extern uint32_t prog_status, prog_cntrl;

unsigned char inf_buf[16];

//extern AK_dataT AK_data;
extern uint32_t trimming_step;
extern SYN_dataT SYN_data;

uint32_t registrator_addr;
uint8_t rd_wr_RFIC_adr;

//extern uint8_t IFA_TRESH;
extern uint8_t rf_agc_lev[];
extern uint8_t ifa_agc_lev[];
extern uint8_t trim_adc1, trim_adc2;
/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/
void WakeTxStart(WakePackageT * package);
void WakeReadInit(WakePackageT * package);
uint8_t GetCRCWake(uint8_t crc, uint8_t byte);
uint32_t CalcRxWakeCRC(WakePackageT * package);
int wake_wr_tx_buf(int ind, unsigned char d);

extern void wr_RFIC(uint8_t rfic, uint8_t reg, uint8_t data); 
extern uint8_t rd_RFIC(uint8_t rfic, uint8_t reg);
extern void SendToPLL(unsigned word); 
extern unsigned ReadFromPLL(int reg);
extern uint8_t ReadFromADC(uint8_t chan, uint8_t adr);
extern void WriteToADC(uint8_t chan, uint8_t adr, uint8_t dat);
extern void WriteToSYN(uint32_t word);
extern uint8_t read_IIC(uint8_t devadr, uint8_t regadr);
extern void write_IIC(uint8_t devadr, uint8_t regadr, uint8_t data);

void fletcher16(unsigned char* checkA, unsigned char* checkB, unsigned char* data, unsigned int len);
void send_GLO_cmnd(unsigned char* cmnd, int len);
extern void Pause(unsigned int t_ms);
void send_to_board(uint8_t bt);
/*************************************************************************************
** �������: InitUART
** ����������: ������������� UARTs
** ����: ���
** �����: ���
**************************************************************************************/
void InitUART(void)
{
int temp;
//volatile int *PMU, *DMA;
volatile int* UART1 = (volatile int *)UART1_BASE;
//volatile int* UART2 = (volatile int *)UART2_BASE;
//volatile int* BOARD_UART = (volatile int *)UART3_BASE;

// UART1
/*   RXSTAT
���     �����������     ���     ��������
0       FRAME   R       ������ � ������� �������
                        0 - ��� ������
                        1 - ������
1       PARITY  R       ������  ��������
                        0 - ��� ������
                        1 - ������
2       OVERRUN R       ������������ ������ ���������
                        0 - ��� ������������
                        1 - ������������. ��� ��������� ����� ������ ��������.
3       ERROR   R       ��������������� � 1, ���� ���� �� ���� ��������� ������ ��������������.
5:4     RSV     -       �� ������������
6       PA_EN (������ DB_SRT )   R/W     ���������� ������������ ���������� "prefetch abort"
7       LBM     R/W     �������� �����
                        0 - ���������� ������
                        1 - ������ � �������� ������      */
    UART1[RXSTAT] = 0x0;
/*    H_UBRCR
���     �����������     ���     ��������
0       BREAK   R/W     ���� ����� 1, ����� TXD ����������� ��������������� � ������� �������.
1       PRTEN   R/W     ���������� ��������� � ������������� ���� ��������
                        0 - ���������.
                        1 - ���������.
2       EVENPRT  R/W     ����� �������� �� ��������\����������
                        0 - ����������.
                        1 - ��������.
3       XSTOP   R/W     ���������� ����-����� (��������)
                        0 - ���� ����-���
                        1 - ��� ����-����
�������� �� ��������� ���������� �������� ����-���.
4       UFIFOEN R/W     ���������� ������� FIFO
                        0 - ��������� (������� ������ ����� 1 �����)
                        1 - ��������� FIFO ��������� � �����������.
6:5     WRDLEN  R/W     ����� ����� ������������ ������
                        00 - 8 bits
                        01 - 7 bits
                        10 - 6 bits
                        11 - 5 bits
7       INV     R/W     �������� TXD ����� ����� 1.        */
//    UART1[H_UBRCR] = 0x010;     // ��������� FIFO ��������� � �����������, 8 ���, 1 �������, ��� ��������
  UART1[H_UBRCR] = 0x018;     // ��������� FIFO ��������� � �����������, 8 ���, 2 �������, ��� ��������
#if (CPU_CLK == 56428571) || (CPU_CLK == 56607142)
  //temp = ((CPU_CLK >> 4) / 115200) - 1;      // ��� ������� = 1/16 clock
  temp = 30;
#else    
  #if (CPU_CLK == 51041667)
  temp = 27;  //51.0417 MHz
  #else
  temp = 28;  //52.83(3) || 53.0
  #endif
#endif
/*   M_UBRCR
���     �����������     ���     ��������
7:0     BRH     R/W     ������� 8 ��� �������� */
    UART1[M_UBRCR] = temp >> 8;
/*   L_UBRCR
���     �����������     ���     ��������
7:0     BRL     R/W     ������� 8 ��� �������� */
    UART1[L_UBRCR] = temp & 0xff;
/*   UARTCON
���     �����������     ���     ��������
0       UARTEN          R/W     ���������� ������ UART ����������
                                0 - ��������
                                1 - �������
1       UTXDIS          R/W     ���������� ������������
                                0 - �������
                                1 - ��������
2       URXDIS          R/W     ���������� ����������
                                0 - �������
                                1 - ��������
3       DMAONERR        R/W     ���������� �������� � ����������� ��� �� ����� ����������� ������ ������
                                0 - ����������� ������ �� ������ ������������ ������� � ����������� ��� �� ������������.
                                1 -������������ ������� � ����������� ��� �� ������������ ��������� ��� ����������� ������.
4       UTXFDIS         R/W     ���������� ������� �����������
                                0 - FIFO ��������
                                1 - FIFO ���������
5       URXFDIS         R/W     ���������� ������� ���������
                                0 - FIFO ��������
                                1 - FIFO ���������
7:6     UHBRE   R/W     ���������� ������������� ������ ���� �������
                                00 - 1/16 clock
                                01 - 1/4 clock
                                1� -  1/4 clock, ���������� ����������� ��������.  */
    UART1[UARTCON] = 0x01;  // ���������� ������ UART
/*   UARTINTMASK
���     �����������     ���     ��������
0       TXINT           R/W     ���������� ����������� ���������� �� ����� TXINT, 1 - ���������
1       RXINT           R/W     ���������� ����������� ���������� �� ����� RXINT, 1 - ���������
2       RXERRINT        R/W     ���������� ����������� ���������� �� ����� RXERRINT, 1 - ���������
3       MSINT           R/W     ���������� ����������� ���������� �� ����� MSINT, 1 - ���������
4       UDINT           R/W     ���������� ����������� ���������� �� ����� UDINT, 1 - ���������
5       UTXEINT         R/W     ���������� ����������� ���������� �� ����� UTXEINT, 1 - ���������
6       URXTINT         R/W     ���������� ����������� ���������� �� ����� URXTINT, 1 - ���������
7       RSV     -       �� ������������     */
   UART1[UARTINTMASK] = 0x0;
/*  UARTINTSTAT
���     �����������     ���     ��������
0       TXINT           R       ������ ���������� �� �����������
1       RXINT           R       ������ ���������� �� ���������
2       RXERRINT        R       ������ ������ �� ����� ������ � ������������ ���
3       MSINT           R/W     ������ ���������� �� ������
4       UDINT           R       ������ ���������� �� ��������� � ��������� "��������"
5       UTXEINT         R/W     ������ ���������� �� ������ ����������� ��� ���������� � ��� ������
6       URXTINT         R/W     ������ ���������� �� ��������� ��� ����������� �������� "time-out"
7       RSV     -       �� ������������               */
   UART1[UARTINTSTAT] = 0xff;
   UART1[UARTINTMASK] |= 0x2;  // ������ ���������� �� ���������

/*// UART2
    UART2[RXSTAT] = 0x0;
    UART2[H_UBRCR] = 0x010;     // ��������� FIFO ��������� � �����������, 8 ���, 1 �������, ��� ��������
 //   UART2[H_UBRCR] = 0x018;     // ��������� FIFO ��������� � �����������, 8 ���, 2 �������, ��� ��������
    temp = ((CPU_CLK >> 4) / 115200) - 1;      // ��� ������� = 1/16 clock
    UART2[M_UBRCR] = temp >> 8;
    UART2[L_UBRCR] = temp & 0xff;
    UART2[UARTCON] = 0x01;  // ���������� ������ UART
    UART2[UARTINTMASK] = 0x0;
    UART2[UARTINTSTAT] = 0xff;
//    UART2[UARTINTMASK] |= 0x2;  // ������ ���������� �� ��������� */

/*    uart2_tx_buf_wr_ptr = 0;
    uart2_tx_buf_rd_ptr = 0;
    uart2_tx_buf_len = 0; */
   
    uart1_tx_buf_rd_ptr = 0;
    uart1_tx_buf_wr_ptr = 0;
    uart1_tx_buf_len = 0;
    
/*    board_tx_buf_wr_ptr = 0;
    board_tx_buf_rd_ptr = 0;
    board_tx_buf_len = 0;
    board_rx_buf_rd_ptr = 0;
    board_rx_buf_wr_ptr = 0;
    board_rx_buf_len = 0;  */

}
//--------------------------------------------------------------------
/*void  USART1_Send (void)
{
volatile int *UART1;
unsigned char temp;

    UART1 = (volatile int *)UART1_BASE;

        if (UART1[UARTINTSTAT] & 0x1) { // TXINT
            do {
                UART1[UARTDR] = uart1_tx_buf[uart1_tx_buf_ptr++];   // write no FIFO
                    temp = '1'; //uart1_tx_buf_len - uart1_tx_buf_ptr;
                uart1_tx_buf_ptr = uart1_tx_buf_ptr & 0xff;    
            } while ((UART1[UARTINTSTAT] & 0x1) && temp);
}*/
//--------------------------------------------------------------------
/*void  USART2_Rcv (void)
{
volatile int *UART2;

UART2 = (volatile int *)UART2_BASE;

       if (UART2[UARTINTSTAT] & 0x2) { // RXINT
//-------------------- ����� ��������� �� �������� --------------------- start -
            do {
                        uart2_rx_buf[uart2_rx_buf_ptr++] = UART2[UARTDR];
                        uart2_rx_buf_ptr = uart2_rx_buf_ptr & 0xff;
            } while (UART2[UARTINTSTAT] & 0x2);
//-------------------- ����� ��������� �� �������� --------------------- end -
       }
}*/
//--------------------------------------------------------------------
void asc_host(WakePackageT * package)
{
volatile int* AK = (volatile int *)AK_BASE_ADR;
//volatile int* GPIO = (volatile int *)GPIO_BASE;
//volatile int* UART1 = (volatile int *)UART1_BASE;
uint32_t temp, registrator_addr;
//uint32_t* temp_a;
int i;
unsigned char inf_str[] = "NT1057 v0.10";

  if (package->_status & (WAKE_READ_ERROR | WAKE_READ_TIMEOUT)) {
    wake_UART_1_tx.command = WAKE_C_Err;
    wake_UART_1_tx.length = 1;
    wake_UART_1_tx.data[0] = package->_status & (WAKE_READ_ERROR | WAKE_READ_TIMEOUT);
    WakeTxStart(&wake_UART_1_tx);
    return;
  }

  if (package->_status & WAKE_RESV_PACKET) {
    package->_status |= CalcRxWakeCRC(package);
    if (package->_status & WAKE_READ_COMPLETE) {
      switch(package->command) {
        case WAKE_C_Nop:          
          wake_UART_1_tx.command = WAKE_C_Nop;
          wake_UART_1_tx.length = 1;
          WakeTxStart(&wake_UART_1_tx);
          break;
        case WAKE_C_Err:          
          WakeTxStart(&wake_UART_1_tx);    // repeat previous
          break;    
       case WAKE_C_SWv:          
          wake_UART_1_tx.command = WAKE_C_SWv;
          wake_UART_1_tx.length = package->length;
  //        for (i = 0; i < package->length; i++)
  //          wake_UART_1_tx.data[i] = package->data[i];
          wake_UART_1_tx.data[0] = SW_VERSION/100;
          wake_UART_1_tx.data[1] = SW_VERSION%100;
          WakeTxStart(&wake_UART_1_tx);
          break;
        case WAKE_C_Info:       
          wake_UART_1_tx.command = WAKE_C_Info;
          wake_UART_1_tx.length = 13;
          for (i = 0; i < 12; i++)
            wake_UART_1_tx.data[i] = inf_str[i];
          wake_UART_1_tx.data[12] = 0;
          WakeTxStart(&wake_UART_1_tx);  
          break;    
        case WAKE_C_RdRg:            
          wake_UART_1_tx.command = WAKE_C_RdRg;
          wake_UART_1_tx.length = 4;
          if (package->data[0] == 55) { 
            temp = (prog_cntrl&0xffff0000) | (prog_status & 0x0000ffff);
          } else if (package->data[0] == 56) {
//            temp_a = (uint32_t*)&rf_agc_lev[0];
//            temp = *temp_a;
            temp = (rf_agc_lev[0]>>4) | rf_agc_lev[1] | (rf_agc_lev[2]<<4) | (rf_agc_lev[3]<<8);
            temp |= (rf_agc_lev[4]<<12) | (rf_agc_lev[5]<<16) | (rf_agc_lev[6]<<20) | (rf_agc_lev[7]<<24);
          } else if (package->data[0] == 57) {
            temp = (ifa_agc_lev[1]<<8) | ifa_agc_lev[0] | (ifa_agc_lev[2]<<16) | (ifa_agc_lev[3]<<24);
          } else if (package->data[0] == 58) {
            temp = (ifa_agc_lev[5]<<8) | ifa_agc_lev[4] | (ifa_agc_lev[6]<<16) | (ifa_agc_lev[7]<<24);  
          } else if (package->data[0] == 60) {
            temp = (trim_adc2<<8) | trim_adc1;   
          } else temp = AK[package->data[0]];
          wake_UART_1_tx.data[0] = temp;
          wake_UART_1_tx.data[1] = temp>>8;
          wake_UART_1_tx.data[2] = temp>>16;
          wake_UART_1_tx.data[3] = temp>>24;
          WakeTxStart(&wake_UART_1_tx);         
          break;
        case WAKE_C_WrRg:  
          temp = package->data[1];
          temp += package->data[2]<<8;
          temp += package->data[3]<<16;
          temp += package->data[4]<<24;
//          if (package->data[0] == 55) prog_cntrl = temp&0xffff0000;
          if (package->data[0] == 55) {
            if ((prog_cntrl ^ temp) & (RF_AGC_MANUAL | IFA_AGC_MANUAL)) {  // change IFA and RF AGC mode
              uint8_t regn, new_bits = 0x18;
              if (temp & RF_AGC_MANUAL) new_bits &= ~0x10;         // bit 4
              if (temp & IFA_AGC_MANUAL) new_bits &= ~0x08;         // bit 3
              for (int i = 0; i < ADC_NUM*4; i++) {
                regn = 15 + (i&0x3)*7;
                wr_RFIC(i & 0x4 ? 1 : 0, regn, (rd_RFIC(i & 0x4 ? 1 : 0, regn) & 0xE7) | new_bits);  
              }     
            }  
            prog_cntrl = temp&0xffff0000;
          } else if (package->data[0] == 56) 
            ; //IFA_TRESH = temp;
          else AK[package->data[0]] = temp;  
          wake_UART_1_tx.command = WAKE_C_WrRg;
          wake_UART_1_tx.length = 1;
          wake_UART_1_tx.data[0] = 0;   // Ok
          WakeTxStart(&wake_UART_1_tx);  
          break; 
        case WAKE_C_Registrator:          
          wake_UART_1_tx.command = WAKE_C_Registrator;
          wake_UART_1_tx.length = 256;
          registrator_addr = package->data[1];
          registrator_addr = registrator_addr<<9;
          registrator_addr |= package->data[0];      
          for (i = 0; i < 64; i++) {
            AK[REG_INDEX] = registrator_addr;
            registrator_addr += 0x8;          //registrator_addr++;
            temp = AK[REG_INDEX];
            wake_UART_1_tx.data[i*4+0] = temp;
            wake_UART_1_tx.data[i*4+1] = temp>>8;
            wake_UART_1_tx.data[i*4+2] = temp>>16;
            wake_UART_1_tx.data[i*4+3] = temp>>24;
          }
          WakeTxStart(&wake_UART_1_tx);  
          break;
   //     case WAKE_C_WrSyn:              // SYN_Calibrate write
        case WAKE_C_WrImg: 
          temp = package->data[0];
          temp += package->data[1]<<8;
          temp = (temp & 0x3fff) | 0x4000;
          { 
          volatile unsigned char* PTR = (unsigned char *)temp;  
          temp = package->length-2;
          for (i = 0; i <= temp; i++)  
            PTR[i] = package->data[i+2];
          wake_UART_1_tx.command = WAKE_C_WrImg;
          wake_UART_1_tx.length = 1;
          wake_UART_1_tx.data[0] = 0;   // Ok
          WakeTxStart(&wake_UART_1_tx); 
          }
          break; 
/*        case WAKE_C_RdGPIO:            
          wake_UART_1_tx.command = WAKE_C_RdGPIO;
          wake_UART_1_tx.length = 4;
          temp = GPIO[GPIO_IN];         // SYN_Calibrate MUX read
          wake_UART_1_tx.data[0] = temp;
          wake_UART_1_tx.data[1] = temp>>8;
          wake_UART_1_tx.data[2] = temp>>16;
          wake_UART_1_tx.data[3] = temp>>24;
          WakeTxStart(&wake_UART_1_tx);         
          break;*/
        case WAKE_C_WrRFIC:              // write RFIC i
          wr_RFIC(package->data[0]&0x3, package->data[1], package->data[2]);
          wake_UART_1_tx.command = WAKE_C_WrRFIC;
          wake_UART_1_tx.length = 1;
          wake_UART_1_tx.data[0] = 0;   // Ok
          WakeTxStart(&wake_UART_1_tx);  
          break; 
        case WAKE_C_RdRFIC:            // read RFIC i
          wake_UART_1_tx.command = WAKE_C_RdRFIC;
          wake_UART_1_tx.length = 2;
          wake_UART_1_tx.data[0] = package->data[1];
          wake_UART_1_tx.data[1] = rd_RFIC(package->data[0]&0x3, package->data[1]);
          WakeTxStart(&wake_UART_1_tx);         
          break;
        case WAKE_C_WrPLL:              // write i2c PLL
          write_IIC(0xD0, package->data[0], package->data[1]);
          wake_UART_1_tx.command = WAKE_C_WrPLL;
          wake_UART_1_tx.length = 1;
          wake_UART_1_tx.data[0] = 0;   // Ok
          WakeTxStart(&wake_UART_1_tx);  
          break; 
        case WAKE_C_RdPLL:            // read i2c PLL
          wake_UART_1_tx.command = WAKE_C_RdPLL;
          wake_UART_1_tx.length = 2;
          wake_UART_1_tx.data[0] = package->data[0];
          wake_UART_1_tx.data[1] = read_IIC(0xD0, package->data[0]);
          WakeTxStart(&wake_UART_1_tx);         
          break;
/*        case WAKE_C_WrGPIO:              //  SYN_Calibrate RefGen enable/disable 
          temp = package->data[0];
          temp += package->data[1]<<8;
          temp += package->data[2]<<16;
          temp += package->data[3]<<24; 
          GPIO[GPIO_OUT] = temp;
          wake_UART_1_tx.command = WAKE_C_WrGPIO;
          wake_UART_1_tx.length = 1;
          wake_UART_1_tx.data[0] = 0;   // Ok
          WakeTxStart(&wake_UART_1_tx);  
          break; */
        case WAKE_C_WrADC:              // write ADC i
          WriteToADC(package->data[0]&0x3, package->data[1], package->data[2]);
          wake_UART_1_tx.command = WAKE_C_WrADC;
          wake_UART_1_tx.length = 1;
          wake_UART_1_tx.data[0] = 0;   // Ok
          WakeTxStart(&wake_UART_1_tx);  
          break; 
        case WAKE_C_RdADC:            // read ADC i
          wake_UART_1_tx.command = WAKE_C_RdADC;
          wake_UART_1_tx.length = 2;
          wake_UART_1_tx.data[0] = package->data[1];
          wake_UART_1_tx.data[1] = ReadFromADC(package->data[0]&0x3, package->data[1]);
          WakeTxStart(&wake_UART_1_tx);         
          break;
        default:  
          package->_status |= WAKE_CMND_ERROR;  
          break;  
      }  
    } 
  }  

  if (package->_status & (WAKE_CMND_ERROR | WAKE_READ_CRC_ERROR)) {
    wake_UART_1_tx.command = package->command;
    wake_UART_1_tx.length = 1;
    wake_UART_1_tx.data[0] = package->_status & (WAKE_CMND_ERROR | WAKE_READ_CRC_ERROR);
    WakeTxStart(&wake_UART_1_tx);
    return;
  }
}  
/*-----------------------------------------------------------*/
void WakeTxStart(WakePackageT * package)
{
uint8_t d;
int32_t i;
volatile int* UART1 = (volatile int *)UART1_BASE;

  uart1_tx_buf_len = 1;
  package->_crc = CRC_WAKE_INIT;
  for(i = WAKE_FEND_INDEX; i < package->length; i++) {
        if ((i == WAKE_ADDRESS_INDEX) && (!package->address)) i = WAKE_COMMAND_INDEX;
        switch (i) {
          case WAKE_FEND_INDEX:
            d = FEND;
            uart1_tx_buf[0] = FEND;
            break;
          case WAKE_ADDRESS_INDEX:
            d = package->address & 0x7F;  //address must be less then 127
            uart1_tx_buf_len += wake_wr_tx_buf(uart1_tx_buf_len, d | 0x80);
            break;
          case WAKE_COMMAND_INDEX:
            d = package->command;
            uart1_tx_buf_len += wake_wr_tx_buf(uart1_tx_buf_len, d);
            break;
          case WAKE_LENGTH_INDEX:
            d = package->length;
            uart1_tx_buf_len += wake_wr_tx_buf(uart1_tx_buf_len, d);
            break;
          default:
            d = package->data[i];
            uart1_tx_buf_len += wake_wr_tx_buf(uart1_tx_buf_len, d);
            break;
        }  // switch (i) {
        package->_crc = GetCRCWake(package->_crc, d);
  }  // for(i = WAKE_FEND_INDEX; i <
  uart1_tx_buf_len += wake_wr_tx_buf(uart1_tx_buf_len, package->_crc);
  package->_status = WAKE_WRITE_IN_PROCESS;
  uart1_tx_buf_rd_ptr = 0;
//  uart1_mode = 1; // WAKE mode
  UART1[UARTINTMASK] |= 0x1;    // set TXINT. ���������� ����������� ���������� �� ����� TXINT
}
/*-----------------------------------------------------------*/
int wake_wr_tx_buf(int ind, unsigned char d)
{
  switch (d) { //stuffing FEND and FESC
    case FEND:
      uart1_tx_buf[ind] = FESC;
      uart1_tx_buf[ind+1] = TFEND;
      return 2;
    case FESC:
      uart1_tx_buf[ind] = FESC;
      uart1_tx_buf[ind+1] = TFESC;
      return 2;
    default:
      uart1_tx_buf[ind] = d;
      return 1;
    }
}
/*-----------------------------------------------------------*/
uint8_t GetCRCWake(uint8_t crc, uint8_t byte)
{
int i;

    for(i=0; i<8; i++)     {
        if(((byte ^ crc) & 1) != 0) {
            crc = ((crc ^ 0x18) >> 1) | 0x80;
        } else {
            crc = (crc >> 1) & ~0x80;
        }
        byte = byte >> 1;
    }
    return crc;
}
/*-----------------------------------------------------------*/
void WakeReadInit(WakePackageT * package) {
volatile int* UART1 = (volatile int *)UART1_BASE;

    package->_crc = 0;
    package->_fFESC = 0;
    package->_index = WAKE_FEND_INDEX;
    package->address = 0;
    package->command = 0;
    package->length = 0;
    package->_status = WAKE_READ_WAITING_FEND;
        // clear Rx FIFO
    while (!(UART1[UARTFLG] & 0x10))
        (void) UART1[UARTDR];
    UART1[UARTINTMASK] |= 0x2;    // set RXINT, ���������� ���������� �� ����� RXINT
}
/*-----------------------------------------------------------*/
uint32_t CalcRxWakeCRC(WakePackageT * package)
{
uint8_t byte;
int i;

    byte = GetCRCWake(CRC_WAKE_INIT, FEND);
    if (package->address & 0x80)         // without address
        package->address = 0;
    else
        byte = GetCRCWake(byte, package->address);
    byte = GetCRCWake(byte, package->command);
    byte = GetCRCWake(byte, package->length);
    for(i = 0; i < package->length; i++)
        byte = GetCRCWake(byte, package->data[i]);

    if (byte == package->_crc)
        return WAKE_READ_COMPLETE;
    else
        return WAKE_READ_CRC_ERROR;
}
//--------------------------------------------------------------------

/*********************************************************
*                    ����� ������
**********************************************************/


























































