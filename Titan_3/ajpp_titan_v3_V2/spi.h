#ifndef SPI_H
#define SPI_H

#include "MCU32.h"

uint8_t conf_RFIC_L2_GLO[] = {  // GLONAS L2 Fvco = 1230 MHz
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0C, // CDIV = 12 // 51.25 MHz
12, 0x18, 
//--------------------------------------------------------------------- chan 1
13, 0x01,  // upper side band 1240
14, 0x27,   // 20.31 MHz   GLONAS L2   >20
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, // threshold 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06,
//--------------------------------------------------------------------- chan 2
20, 0x01,  // upper side band 1240
21, 0x27,   // 20.31 MHz   GLONAS L2
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, // threshold 
24, 0xf1, // RF gain = 15 db 
//Reg25, 0xEA,
25, 0xea, // IF AGC digital detector threshold = 50%
26, 0x06, 
//--------------------------------------------------------------------- chan 3
27, 0x01,  // upper side band 1240
28, 0x27,   // 20.31 MHz   GLONAS L2
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, // threshold 
31, 0xf1, // RF gain = 15 db 
//Reg32, 0xEA,
32, 0xea, // IF AGC digital detector threshold = 50% 
33, 0x06, 
//--------------------------------------------------------------------- chan 4
34, 0x01,  // upper side band 1240
35, 0x27,   // 20.31 MHz   GLONAS L2
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, // threshold 
38, 0xf1, // RF gain = 15 db 
//Reg39, 0xEA,
39, 0xea, // IF AGC digital detector threshold = 50% 
40, 0x06,
//--------------------------------------------------------------------- PLL_A
41, 0x01, // L2, enable
42, 123/2, // N = 123   f = 10*123 = 1230 MHz GLONAS L2
43, 0x80, // R = 1
//--------------------------------------------------------------------- PLL_B
45, 0x00, // L2, disable 
46, 123/2, // N = 123   f = 10*123 = 1230 MHz
47, 0x80 // R = 1
};

uint8_t conf_RFIC_E5b_B2[] = {  // GPS L2C, Galileo E5b, Beidou B2 Fvco = 1230 MHz
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0C, // CDIV = 12 // 51.25 MHz
12, 0x18, 
//---------------------------------------------------------------------- chan 1
13, 0x03,  // lower side band
14, 0x3c,   // 26.5 MHz  Galileo E5, Beidou B2
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, // threshold 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 50% 
19, 0x06, 
//---------------------------------------------------------------------- chan 2
20, 0x03,  // lower side band
21, 0x3c,   // 26.5 MHz  Galileo E5, Beidou B2
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, // threshold 
24, 0xf1, // RF gain = 15 db 
//Reg25, 0xEA,
25, 0xea, // IF AGC digital detector threshold = 50%
26, 0x06, 
//---------------------------------------------------------------------- chan 3
27, 0x03,  // lower side band
28, 0x3c,   // 26.5 MHz  Galileo E5, Beidou B2
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, // threshold 
31, 0xf1, // RF gain = 15 db 
//Reg32, 0xEA,
32, 0xea, // IF AGC digital detector threshold = 50% 
33, 0x06, 
//---------------------------------------------------------------------- chan 4
34, 0x03,  // lower side band
35, 0x3c,   // 26.5 MHz  Galileo E5, Beidou B2
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, // threshold 
38, 0xf1, // RF gain = 15 db 
//Reg39, 0xEA,
39, 0xea, // IF AGC digital detector threshold = 50% 
40, 0x06,
//---------------------------------------------------------------------- PLL_A
41, 0x01, // L2, enable
42, 123/2, // N = 123   f = 10*123 = 1230 MHz
43, 0x80, // R = 1
//---------------------------------------------------------------------- PLL_B
45, 0x00, // L2 
46, 123/2, // N = 123   f = 10*123 = 1230 MHz
47, 0x80 // R = 1
};

//--------------------------------------------------------------- L1 -

uint8_t conf_RFIC_L1_GLO[] = {  // GLONAS L1 Fvco = 1585 MHz
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0f, // CDIV = 15 // 52.833(3) MHz
12, 0x18, 
//--------------------------------------------------------------------- chan 1
13, 0x01,  // upper side band
//14, 0x1E,   // 18 MHz   GLONAS L1
14, 0x2d,   // 22 MHz
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, // threshold 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//--------------------------------------------------------------------- chan 2
20, 0x01,  // upper side band
//21, 0x1E,   // 18 MHz   GLONAS L1
21, 0x2d,   // 22 MHz   GLONAS L1
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
//Reg25, 0xEA,
25, 0xea, // IF AGC digital detector threshold = 30%
26, 0x06, 
//--------------------------------------------------------------------- chan 3
27, 0x01,  // upper side band
//28, 0x1E,   // 18 MHz   GLONAS L1
28, 0x2d,   // 22 MHz
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
//Reg32, 0xEA,
32, 0xea, // IF AGC digital detector threshold = 30% 
33, 0x06, 
//--------------------------------------------------------------------- chan 4
34, 0x01,  // upper side band
//35, 0x1E,   // 18 MHz   GLONAS L1
35, 0x2d,   // 22 MHz
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
//Reg39, 0xEA,
39, 0xea, // IF AGC digital detector threshold = 30% 
40, 0x06,
//--------------------------------------------------------------------- PLL_A
41, 0x03, // L1, enable
42, 317/2, // N = 317   f = 5*317 = 1585 MHz GLONAS L1
43, 0x90, // R = 2
//--------------------------------------------------------------------- PLL_B
45, 0x02, // L1, disable 
46, 317/2, // N = 317   f = 5*317 = 1585 MHz GLONAS L1
47, 0x90, // R = 2
//65, 0x7b // clk out off
};

uint8_t conf_RFIC_L1_GLO_1590[] = {  // GLONAS L1 Fvco = 1590 MHz
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0f, // CDIV = 15 // 53 MHz
12, 0x18, 
//--------------------------------------------------------------------- chan 1
13, 0x01,  // upper side band
14, 0x27,   // 20.31 MHz
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, // threshold 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//--------------------------------------------------------------------- chan 2
20, 0x01,  // upper side band
21, 0x27,   // 20.31 MHz
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
25, 0xea, // IF AGC digital detector threshold = 30%
26, 0x06, 
//--------------------------------------------------------------------- chan 3
27, 0x01,  // upper side band
28, 0x27,   // 20.31 MHz
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
32, 0xea, // IF AGC digital detector threshold = 30% 
33, 0x06, 
//--------------------------------------------------------------------- chan 4
34, 0x01,  // upper side band
35, 0x27,   // 20.31 MHz
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
39, 0xea, // IF AGC digital detector threshold = 30% 
40, 0x06,
//--------------------------------------------------------------------- PLL_A
41, 0x03, // L1, enable
42, 159/2, // f = 10*159 = 1590 MHz GLONAS L1
43, 0x80, // R = 1
//--------------------------------------------------------------------- PLL_B
45, 0x02, // L1, disable 
46, 159/2, // 
47, 0x80, // R = 1
};

uint8_t conf_RFIC_L1_GPS_1585[] = {  // GPS L1, Galileo E1, Beidou B1 Fvco = 1585 MHz  
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0F, // CDIV = 15 // 52.833 MHz
12, 0x18,
//---------------------------------------------------------------------- chan 1
13, 0x03,  // lower side band
14, 0x3c,   // 26.5 MHz  
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//---------------------------------------------------------------------- chan 2
20, 0x03,  // lower side band
21, 0x3c,   // 26.5 MHz
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
25, 0xea, // IF AGC digital detector threshold = 30%
//Reg26, 0x0A,
26, 0x06, 
//---------------------------------------------------------------------- chan 3
27, 0x03,  // lower side band
28, 0x3c,   // 26.5 MHz
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
32, 0xea, // IF AGC digital detector threshold = 30% 
//Reg33, 0x0A,
33, 0x06, 
//---------------------------------------------------------------------- chan 4
34, 0x03,  // lower side band
35, 0x3c,   // 26.5 MHz
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
39, 0xea, // IF AGC digital detector threshold = 30% 
//Reg40, 0x0A,
40, 0x06,
//---------------------------------------------------------------------- PLL_A
41, 0x03, // L1, enable
42, 317/2, // f = 5*317 = 1585 MHz GPS L1, Galileo E1, Beidou B1
43, 0x90, // R = 2
//---------------------------------------------------------------------- PLL_B
45, 0x02, // L1 
46, 317/2, //   
47, 0x90, // 
};

uint8_t conf_RFIC_L1_GPS_1590[] = {  // GPS L1, Galileo E1, Beidou B1 Fvco = 1590 MHz  
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0F, // CDIV = 15 // 53 MHz
12, 0x18,
//---------------------------------------------------------------------- chan 1
13, 0x03,  // lower side band
14, 0x48,   // 29.64 MHz  
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//---------------------------------------------------------------------- chan 2
20, 0x01,  // USB
21, 0x48,   // 29.64 MHz 
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
25, 0xea, // IF AGC digital detector threshold = 30%
//Reg26, 0x0A,
26, 0x06, 
//---------------------------------------------------------------------- chan 3
27, 0x03,  // lower side band
28, 0x48,   // 29.64 MHz 
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
32, 0xea, // IF AGC digital detector threshold = 30% 
//Reg33, 0x0A,
33, 0x06, 
//---------------------------------------------------------------------- chan 4
34, 0x01,  // USB
35, 0x48,   // 29.64 MHz 
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
39, 0xea, // IF AGC digital detector threshold = 30% 
//Reg40, 0x0A,
40, 0x06,
//---------------------------------------------------------------------- PLL_A
41, 0x03, // L1, enable
42, 159/2, // f = 5*317 = 1590 MHz GPS L1, Galileo E1, Beidou B1
43, 0x80, // R = 1
//---------------------------------------------------------------------- PLL_B
45, 0x02, // L1 
46, 159/2, //   
47, 8, // 
};
//------------------------------------------------------------------------------
uint8_t conf_RFIC_L1_GPS_1580[] = {  // GPS L1 Fvco = 1580 MHz  
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x10, // CDIV = 16 // 49.375 MHz
12, 0x18,
//---------------------------------------------------------------------- chan 1
13, 0x03,  // lower side band
14, 0x0,   // 11.22 MHz  
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//---------------------------------------------------------------------- chan 2
20, 0x03,  // lower side band
21, 0x0,   // 11.22 MHz
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
25, 0xea, // IF AGC digital detector threshold = 30%
//Reg26, 0x0A,
26, 0x06, 
//---------------------------------------------------------------------- chan 3
27, 0x03,  // lower side band
28, 0x0,   // 11.22 MHz
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
32, 0xea, // IF AGC digital detector threshold = 30% 
//Reg33, 0x0A,
33, 0x06, 
//---------------------------------------------------------------------- chan 4
34, 0x03,  // lower side band
35, 0x0,   // 11.22 MHz
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
39, 0xea, // IF AGC digital detector threshold = 30% 
//Reg40, 0x0A,
40, 0x06,
//---------------------------------------------------------------------- PLL_A
41, 0x03, // L1, enable
42, 158/2, // f = 1580 MHz GPS L1
43, 0x0, // R = 1
//---------------------------------------------------------------------- PLL_B
45, 0x02, // L1 
46, 158/2, //   
47, 0x0, // 
};
//------------------------------------------------------------------------------
uint8_t conf_RFIC_L5_1165[] = {  // L5 Fvco = 1165 MHz  
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0B, // CDIV = 11 
12, 0x10,   // LVDS
//---------------------------------------------------------------------- chan 1
13, 0x01,  // USB
14, 0x2D,   // 21.9 MHz  
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//---------------------------------------------------------------------- chan 2
20, 0x01,  // USB
21, 0x2D,   // 21.9 MHz  
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
//Reg25, 0xEA,
25, 0xea, // IF AGC digital detector threshold = 30%
//Reg26, 0x0A,
26, 0x06, 
//---------------------------------------------------------------------- chan 3
27, 0x01,  // USB
28, 0x2D,   // 21.9 MHz  
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
//Reg32, 0xEA,
32, 0xea, // IF AGC digital detector threshold = 30% 
//Reg33, 0x0A,
33, 0x06, 
//---------------------------------------------------------------------- chan 4
34, 0x01,  // USB
35, 0x33,   // 21.9 MHz  
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
//Reg39, 0xEA,
39, 0xea, // IF AGC digital detector threshold = 30% 
//Reg40, 0x0A,
40, 0x06,
//---------------------------------------------------------------------- PLL_A
41, 0x01, // L5, enable
42, 233/2, //   f = 1165 MHz 
43, 0x90, // R = 1
//---------------------------------------------------------------------- PLL_B
45, 0x01, // L5 
46, 233/2, //   f = 1165 MHz 
47, 0x90, // R = 1
};
//---------------------------------------------------------------------- 
//------------------------------------------------------------------------------
uint8_t conf_RFIC_L1_GPS_2480[] = {  // GPS L1 Fvco = 2480 MHz  
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x19, // CDIV = 25 // 49.6 MHz
12, 0x18,
//---------------------------------------------------------------------- chan 1
13, 0x01,  // upper side band
14, 0x52,   // 20.23 MHz  
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34,  
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//---------------------------------------------------------------------- chan 2
20, 0x01,  // upper side band
21, 0x52,   // 20.23 MHz  
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
25, 0xea, // IF AGC digital detector threshold = 30%
//Reg26, 0x0A,
26, 0x06, 
//---------------------------------------------------------------------- chan 3
27, 0x01,  // upper side band
28, 0x52,   // 20.23 MHz  
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
32, 0xea, // IF AGC digital detector threshold = 30% 
//Reg33, 0x0A,
33, 0x06, 
//---------------------------------------------------------------------- chan 4
34, 0x01,  // upper side band
35, 0x52,   // 20.23 MHz  
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
39, 0xea, // IF AGC digital detector threshold = 30% 
//Reg40, 0x0A,
40, 0x06,
//---------------------------------------------------------------------- PLL_A
41, 0x05, // L5, enable
42, 124/2, // f = 1580 MHz GPS L1
43, 0x0, // R = 1
//---------------------------------------------------------------------- PLL_B
45, 0x05, // L5 
46, 124/2, //   
47, 0x0, // 
};

uint8_t conf_RFIC_S_2485[] = {  // Navic Fvco = 2485 MHz
2, 0x3,
3, 0x0,  // 10 MHz, PLL "A" for all channels
5, 0x0, 
11, 0x0f, // CDIV = 15 
12, 0x50,   // LVDS, disabled
//--------------------------------------------------------------------- chan 1
13, 0x01,  // upper side band
14, 0x17,   // 15.3 MHz
15, 0x7a, // 400 mV, RF AGC auto
16, 0x34, // threshold 
17, 0xf1, // RF gain = 15 db 
18, 0xea, // IF AGC digital detector threshold = 30% 
19, 0x06, 
//--------------------------------------------------------------------- chan 2
20, 0x01,  // upper side band
21, 0x17,   // 15.3 MHz
22, 0x7a, // 400 mV, RF AGC auto 
23, 0x34, 
24, 0xf1, // RF gain = 15 db 
25, 0xea, // IF AGC digital detector threshold = 30%
26, 0x06, 
//--------------------------------------------------------------------- chan 3
27, 0x01,  // upper side band
28, 0x17,   // 15.3 MHz
29, 0x7a, // 400 mV, RF AGC auto 
30, 0x34, 
31, 0xf1, // RF gain = 15 db 
32, 0xea, // IF AGC digital detector threshold = 30% 
33, 0x06, 
//--------------------------------------------------------------------- chan 4
34, 0x01,  // upper side band
35, 0x17,   // 15.3 MHz
36, 0x7a, // 400 mV, RF AGC auto 
37, 0x34, 
38, 0xf1, // RF gain = 15 db 
39, 0xea, // IF AGC digital detector threshold = 30% 
40, 0x06,
//--------------------------------------------------------------------- PLL_A
41, 0x05, // S, enable
42, 497/2, // f = 2485 MHz 
43, 0xa0, // R = 4
//--------------------------------------------------------------------- PLL_B
45, 0x05, // S, disable 
46, 497/2, // 
47, 0xa0, // R = 4
};
uint8_t conf_PLL[] = {
//Byte#  BinaryValue HexValue Byte#
/*0x00,        0x61,  // ro OTP Control Register
0x01,        0xff,  // ro Factory Reserved Bits - Device ID for Chip Identification
0x02,        0x00,  // ro Factory Reserved Bits - ADC Gain Setting
0x03,        0x00,  // ro Factory Reserved Bits - ADC Gain Setting
0x04,        0x00,  // ro Factory Reserved Bits - ADC OFFSET
0x05,        0x00,  // ro Factory Reserved Bits - ADC OFFSET
0x06,        0x00,  // ro Factory Reserved Bits
0x07,        0x00,  // ro Factory Reserved Bits
0x08,        0x00,  // ro Factory Reserved Bits
0x09,        0xff,  // ro Factory Reserved Bits
0x0A,        0x01,  // ro Factory Reserved Bits
0x0B,        0xc0,  // ro Factory Reserved Bits         // ? 0x00
0x0C,        0x00,  // ro Factory Reserved Bits
0x0D,        0xb6,  // ro Factory Reserved Bits
0x0E,        0xb4,  // ro Factory Reserved Bits
0x0F,        0x92,  // ro Factory Reserved Bits         // ? 0x90*/
//config part
0x10,        0x40,  // rw Primary Source and Shutdown Register, set en_clkin
0x11,        0x0c,  // r0 VCO Band and Factory Reserved Bits
0x12,        0x01,  // ro Crystal X1 Load Capacitor Register
0x13,        0x02,  // rw Factory Reserved Bits, set PRIMSRC
0x14,        0x00,  // ro Factory Reserved Bits
0x15,        0x19,  // rw Reference Divider Register, Ref_div = 25
0x16,        0x0c,  // rw VCO Control Register and Pre-Divider, clear Bypass_prediv
0x17,        0x4e,  // rw Feedback Integer Divider Register
0x18,        0x20,  // rw Feedback Integer Divider Bits, FB_intdiv = 0x4e2(1250) Fref = 2 MHz
0x19,        0x00,  // rw Feedback Fractional Divider Registers
0x1A,        0x00,  // rw Feedback Fractional Divider Registers
0x1B,        0x00,  // rw Feedback Fractional Divider Registers
0x1C,        0x9f,  // rw Factory Reserved Bits
0x1D,        0xff,  // rw Factory Reserved Bits
0x1E,        0x88,  // rw RC Control Register
0x1F,        0x80,  // rw RC Control Register
0x20,        0x00,  // ro Unused Factory Reserved Register
0x21,        0x81,  // rw Output Divider 1 Control Register Settings, en_fod
0x22,        0x00,  // rw Output Divider 1 Fractional Settings
0x23,        0x00,  // rw Output Divider 1 Fractional Settings
0x24,        0x00,  // rw Output Divider 1 Fractional Settings
0x25,        0x00,  // rw Output Divider 1 Fractional Settings
0x26,        0x00,  // rw Output Divider 1 Step Spread Configuration Register
0x27,        0x00,  // rw Output Divider 1 Step Spread Configuration Register
0x28,        0x00,  // rw Output Divider 1 Step Spread Configuration Register
0x29,        0x00,  // rw Output Divider 1 Spread Modulation Rate Configuration Register
0x2A,        0x04,  // rw Output Divider 1 Spread Modulation Rate Configuration Register
0x2B,        0x00,  // rw Output Divider 1 Skew Integer Part
0x2C,        0x01,  // rw Output Divider 1 Skew Integer Part
0x2D,        0x01,  // rw Output Divider 1 Integer Part = 25
0x2E,        0x90,  // rw Output Divider 1 Integer Part
0x2F,        0x00,  // rw Output Divider 1 Skew Fractional Part
0x30,        0x00,  // ro Unused Factory Reserved Register
0x31,        0x0c,  // rw Output Divider 2 Control Register Settings, use previous channel's clock output
0x32,        0x00,  // rw Output Divider 2 Fractional Settings
0x33,        0x00,  // rw Output Divider 2 Fractional Settings
0x34,        0x00,  // rw Output Divider 2 Fractional Settings
0x35,        0x00,  // rw Output Divider 2 Fractional Settings
0x36,        0x00,  // rw Output Divider 2 Step Spread Configuration Register
0x37,        0x00,  // rw Output Divider 2 Step Spread Configuration Register
0x38,        0x00,  // rw Output Divider 2 Step Spread Configuration Register
0x39,        0x00,  // rw Output Divider 2 Spread Modulation Rate Configuration Register
0x3A,        0x04,  // rw Output Divider 2 Spread Modulation Rate Configuration Register
0x3B,        0x00,  // rw Output Divider 2 Skew Integer Part
0x3C,        0x01,  // rw Output Divider 2 Skew Integer Part
0x3D,        0x01,  // rw Output Divider 2 Integer Part = 25
0x3E,        0x90,  // rw Output Divider 2 Integer Part
0x3F,        0x00,  // rw Output Divider 2 Skew Fractional Part
0x40,        0x00,  // ro Unused Factory Reserved Register
0x41,        0x0c,  // rw Output Divider 3 Control Register Settings, use previous channel's clock output
0x42,        0x00,  // rw Output Divider 3 Fractional Settings
0x43,        0x00,  // rw Output Divider 3 Fractional Settings
0x44,        0x00,  // rw Output Divider 3 Fractional Settings
0x45,        0x00,  // rw Output Divider 3 Fractional Settings
0x46,        0x00,  // rw Output Divider 3 Step Spread Configuration Register
0x47,        0x00,  // rw Output Divider 3 Step Spread Configuration Register
0x48,        0x00,  // rw Output Divider 3 Step Spread Configuration Register
0x49,        0x00,  // rw Output Divider 3 Spread Modulation Rate Configuration Register
0x4A,        0x04,  // rw Output Divider 3 Spread Modulation Rate Configuration Register
0x4B,        0x00,  // rw Output Divider 3 Skew Integer Part
0x4C,        0x01,  // rw Output Divider 3 Skew Integer Part
0x4D,        0x01,  // rw Output Divider 3 Integer Part = 25
0x4E,        0x90,  // rw Output Divider 3 Integer Part
0x4F,        0x00,  // rw Output Divider 3 Skew Fractional Part
0x50,        0x00,  // ro Unused Factory Reserved Register
0x51,        0x0c,  // rw Output Divider 4 Control Register Settings, use previous channel's clock output
0x52,        0x00,  // rw Output Divider 4 Fractional Settings
0x53,        0x00,  // rw Output Divider 4 Fractional Settings
0x54,        0x00,  // rw Output Divider 4 Fractional Settings
0x55,        0x00,  // rw Output Divider 4 Fractional Settings
0x56,        0x00,  // rw Output Divider 4 Step Spread Configuration Register
0x57,        0x00,  // rw Output Divider 4 Step Spread Configuration Register
0x58,        0x00,  // rw Output Divider 4 Step Spread Configuration Register
0x59,        0x00,  // rw Output Divider 4 Spread Modulation Rate Configuration Register
0x5A,        0x04,  // rw Output Divider 4 Spread Modulation Rate Configuration Register
0x5B,        0x00,  // rw Output Divider 4 Skew Integer Part
0x5C,        0x00,  // rw Output Divider 4 Skew Integer Part
0x5D,        0x01,  // rw Output Divider 4 Integer Part = 25
0x5E,        0x90,  // rw Output Divider 4 Integer Part
0x5F,        0x00,  // rw Output Divider 4 Skew Fractional Part
0x60,        0x73,  // rw Clock1 Output Configuration  LVDS, 2.5v, output slew rate indicates 1*Normal
0x61,        0x01,  // rw Clock1 Output Configuration, en_clkbuf
0x62,        0x73,  // rw Clock2 Output Configuration  LVDS, 2.5v, output slew rate indicates 1*Normal
0x63,        0x01,  // rw Clock2 Output Configuration, en_clkbuf
0x64,        0x73,  // rw Clock3 Output Configuration  LVDS, 2.5v, output slew rate indicates 1*Normal
0x65,        0x01,  // rw Clock3 Output Configuration, en_clkbuf
0x66,        0x73,  // rw Clock4 Output Configuration  LVDS, 2.5v, output slew rate indicates 1*Normal
0x67,        0x01,  // rw Clock4 Output Configuration, en_clkbuf
0x68,        0x07,  // rw CLK_OE/Shutdown Function
0x69,        0xfc   // rw CLK_OS/Shutdown Function
};
//--------------------------------------------------------------------
#endif // SPI_H
/******************************************************
**                  End Of File
*******************************************************/