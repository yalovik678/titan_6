#include "MCU32.h"
#include "AK.h"
/************************************************************************************
*                                VARIABLES
*************************************************************************************/
PMU_clocks PMU_clk;
extern int SysTickFlg;  // ���� ������� ���������� �������
/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/
extern void InitUART(void);
extern void InitSPI(void);
extern void CPU_InitTick (void);
extern int load_rfic_config(void);

/*********************************************************************
* Description : ������� ��������� ������������� ���������� NT2000.2
*
* Arguments   : ���
**********************************************************************/
void HW_Init(void)
{
volatile int* GPIO = (volatile int *)GPIO_BASE;
volatile int *portCLK_CNTRL_REG = (volatile int *)CLK_CNTRL_REG; //0x80001040;
// X"00000" & "00" & clk_src[1:0] & start_mode[0:1] & "0000" & pclk_div_c[1:0];

*portCLK_CNTRL_REG = *portCLK_CNTRL_REG & (~0x00000300);

// gpio_alt_f <= "0000" & "00" & rxd_1 & txd_1 &
//              '0' & scs3_3 & scs2_3 & scs1_3 & scs0_3 & sclk_3 & mosi_3 & miso_3 &
//              '0' & scs3_2 & scs2_2 & scs1_2 & scs0_2 & sclk_2 & mosi_2 & miso_2 & 
//              '0' & scs3_1 & scs2_1 & scs1_1 & scs0_1 & sclk_1 & mosi_1 & miso_1;
GPIO[GPIO_ALT_1]   = 0x030f7f00;  //GPIO[GPIO_ALT_1]   = 0x030f7f37;        // uart1, spi3, spi2
GPIO[GPIO_PUEN_1]  = 0xfcffffd9;

// scs2_1 - CS_PLL, scs1_1 - CS_Mod
// scs3_2 - CS65_GLO_L1, scs2_2 - CS65_GPS_L1, scs1_2 - CS_ADC_GLO, scs0_2 - CS_ADC_GPS

GPIO[GPIO_ALT_2]   = 0x4;  // cx -> gpio[19:16] = pins 59..62

//GPIO[GPIO_PUEN_1]  = 0xf0cf80d9;
GPIO[GPIO_OUT_1] &= (~0x01000024);  // clear txd_1, scs2_1 & sclk_1 pins data  // HMS830 req
//GPIO[GPIO_OUT_1] |= 0x80000000;  // set att pin data
//GPIO[GPIO_DIR_1] |= 0x81000024;   // set "out" direction for att, txd_1 and scs2_2 & sclk_2 pins  // HMS830 req 

GPIO[GPIO_OUT_1] |= 0xF0000000;  // set att pin data. att = 0 db
GPIO[GPIO_DIR_1] |= 0xF1000024;   // set "out" direction for att, txd_1 and scs2_2 & sclk_2 pins  // HMS830 req

// SPI
  InitSPI();
//  UART 
  InitUART();
//  ������ SYSTIC
  CPU_InitTick();
}

/******************** TICKER INITIALIZATION ******************************
* Description : This function is called to initialize tick source
*               (timer generating interrupts 10 mS).
*
* Arguments   : ���
**********************************************************************/

void  CPU_InitTick (void)
{
volatile int *portNVIC_SYSTICK_RVR; 
volatile int *portNVIC_SYSTICK_CSR; 
//volatile int *portNVIC_ISER;

portNVIC_SYSTICK_RVR = (volatile int *)SYST_RVR; //0xe000e014;
portNVIC_SYSTICK_CSR = (volatile int *)SYST_CSR; //0xe000e010;
//portNVIC_ISER = (volatile int *)NVIC_ISER; //0xe000e100;

//*portNVIC_SYSTICK_RVR = CPU_CLK/100; // 100 Hz   = CPU_CLK/Fsystick
//*portNVIC_SYSTICK_RVR = CPU_CLK/200; // 200 Hz   = CPU_CLK/Fsystick
*portNVIC_SYSTICK_RVR = CPU_CLK/400; // 400 Hz   = CPU_CLK/Fsystick
//*portNVIC_SYSTICK_RVR = CPU_CLK/1000; // 1000 Hz   = CPU_CLK/Fsystick
*portNVIC_SYSTICK_CSR = 0x7;            // CLKSOURCE, TICKINT, ENABLE
//*portNVIC_ISER = 0x3f;

//    Tics_2kHz = 0;
//    Cnt10ms = 0;
    SysTickFlg = 0;
}

void HSI_Init(void)
{
volatile int* AK = (volatile int *)AK_BASE_ADR; 
//int var;
/*       P0_dser_clk_frm_inv => anlg_cnfg_1(3),
         P0_dser_mode        => anlg_cnfg_1(2 downto 0),
         P0_dser_vdl_c_mx    => anlg_cnfg_1(15 downto 8),
         P0_dser_vdl_d_mx    => anlg_cnfg_2,
         P0_en_rx_ck         => anlg_cnfg_1(24),
         P0_en_rx_dat        => (others=>anlg_cnfg_1(25)),
         P0_en_rx_fr         => anlg_cnfg_1(25),

         P1_dser_clk_frm_inv => anlg_cnfg_1(7),
         P1_dser_mode        => anlg_cnfg_1(6 downto 4),
         P1_dser_vdl_c_mx    => anlg_cnfg_1(23 downto 16),
         P1_dser_vdl_d_mx    => anlg_cnfg_3,
         P1_en_rx_ck         => anlg_cnfg_1(26),
         P1_en_rx_dat        => (others=>anlg_cnfg_1(27)),
         P1_en_rx_fr         => anlg_cnfg_1(27),

         en_rs               => anlg_cnfg_1(28),
         i_cc                => anlg_cnfg_1(30 downto 29)  */
  AK[TBI_CONFIG_4] = 0x0;  // off
  AK[COMMON] = 0x800000ff;      // all channel off, ak_reset
//  AK[ANLG_CNFG_1] = 0x50000044;
//  AK[ANLG_CNFG_1] = 0x5F000844;
//  AK[ANLG_CNFG_1] = 0x1F000044;     // i_cc = 0, en_rs, enable P0 and P1, P0 and P1 lvds mode = 4 (12 bit/2 line)
  AK[ANLG_CNFG_1] = 0x5F00004C;     // i_cc = 2, en_rs, enable P0 and P1, P0 and P1 lvds mode = 4 (12 bit/2 line)
  AK[ANLG_CNFG_2] = 0x0;            // delay
  AK[ANLG_CNFG_3] = 0x0;            // delay
//  AK[ANLG_CNFG_4] = 0x11;            // Ilvds = 4, LVDS CLK On
  AK[ANLG_CNFG_4] = 0x01;            // Ilvds = 0, LVDS CLK On
//  var = AK[ANLG_CNFG_1];
//  var = AK[ANLG_CNFG_2];
//  var = AK[ANLG_CNFG_3];
//  var = AK[ANLG_CNFG_4];
}

/*void change_main_clock(void)
{
volatile int *portCLK_CNTRL_REG = (volatile int *)CLK_CNTRL_REG; //0x80001040;
// X"00000" & "00" & clk_src[1:0] & start_mode[0:1] & "0000" & pclk_div_c[1:0];

*portCLK_CNTRL_REG = *portCLK_CNTRL_REG | 0x00000100;
(void)portCLK_CNTRL_REG[0];
} */ 

void change_main_clock(int src)
{
volatile int *portCLK_CNTRL_REG = (volatile int *)CLK_CNTRL_REG; //0x80001040;
// X"00000" & "00" & clk_src[1:0] & start_mode[0:1] & "0000" & pclk_div_c[1:0];

*portCLK_CNTRL_REG = (*portCLK_CNTRL_REG & 0xfffffcff) | (src<<8);
(void)portCLK_CNTRL_REG[0];
}  

void RESTART(void)  //SYSRESETREQ
{
volatile int *portSYST_AIRCR = (volatile int *)SYST_AIRCR; 
  change_main_clock(SRC_RC_CLK);
  *portSYST_AIRCR = 0x05fa0004;  //SYSRESETREQ
}  

/*********************************************************************
*                            End Of File
**********************************************************************/










