#include "MCU32.h"
#include "AK.h"
#include "spi.h"
#include "wake.h"

#define FLASH_SPI SPI3_BASE

//extern AK_dataT AK_data;
extern uint32_t prog_status, prog_cntrl;
unsigned dac_data[8];
uint8_t trim_adc1, trim_adc2;
extern unsigned char inf_buf[];

//uint8_t dmix[9] = {0x4d, 0x80, 0x80, 0x80, 0x80, 16, 0x50, 0x06, 0x00};  //1506-1590 MHz
uint8_t dmix[9] = {0x4d, 0x80, 0x80, 0x80, 0x80, 16, 0x50, 0x06, 0x00};  //1506-1590 MHz
/************************************************************************************
*                                FUNCTION PROTOTYPES
*************************************************************************************/
int CheckRegistratorConst(uint16_t out_const, uint8_t chan_in_bit);
//void send_DAC(void);
void change_main_clock(int src);
/************************************************************************************
*                                void InitSPI(void)
*************************************************************************************/
void InitSPI(void)
{
volatile int* SPI1 = (volatile int *)SPI1_BASE;
volatile int* SPI2 = (volatile int *)SPI2_BASE;
volatile int* SPI3 = (volatile int *)SPI3_BASE;

// ----------------------------------------------------------------------- SPI 1 --
SPI1[SSCR0] = 0x091F; // 32-bits, CPU_CLK / 20
SPI1[SSCR1] = 0x2488;
// ----------------------------------------------------------------------- SPI 2 --
SPI2[SSCR0] = 0x020f;     // 16- ��� ������, CPU_CLK / 6
//SPI3[SSCR0] = 0x040f;     // 16- ��� ������, CPU_CLK / 10
//SPI3[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
SPI2[SSCR1] = 0x0008;     // enable
// ----------------------------------------------------------------------- SPI 3 --
SPI3[SSCR0] = 0x040F;     // 16- ��� ������, CPU_CLK / 10
SPI3[SSCR1] = 0x0008;     // enable
}

//------------------------------------------------------------------------ RFIC --
void wr_RFIC(uint8_t rfic, uint8_t reg, uint8_t data) 
{
  volatile int* SPI = (volatile int *)SPI2_BASE;
#ifdef TITAN_V4 
  rfic =~rfic;
#endif  
  SPI[SSCR0] = 0x020f;     // 16- ��� ������, CPU_CLK / 6
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((rfic&0x1)<<6) + 0x488;     // ������ ��������, CS23, CS10, enable
  SPI[SSDR] = reg & 0x7F; SPI[SSDR] = data;
}

uint8_t rd_RFIC(uint8_t rfic, uint8_t reg) 
{
  volatile int* SPI = (volatile int *)SPI2_BASE;
  uint8_t bt;
#ifdef TITAN_V4 
  rfic =~rfic;
#endif  
  SPI[SSCR0] = 0x020f;     // 16- ��� ������, CPU_CLK / 6
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((rfic&0x1)<<6) + 0x88;     // CS23, CS10, enable
  SPI[SSDR] = reg | 0x80; SPI[SSDR] = 0x0;
  while (SPI[SSSR]&0x8); // BSY
  bt = SPI[SSDR]; 
  bt = SPI[SSDR];
  return bt;
}

int init_RFIC(uint8_t rfic) 
{
//volatile int* GPIO = (volatile int *)GPIO_BASE;  
uint8_t bt, R43;  
uint8_t* ptr;
//int cnflen;
/*
#ifndef L2_VERSION //DA14, DA16 
  if(rfic) ptr = conf_RFIC_L1_GLO;
  else ptr = conf_RFIC_L1_GPS_1585;  
#else   //DA6, DA8
  if(rfic) ptr = conf_RFIC_L2_GLO;
  else ptr = conf_RFIC_E5b_B2;   
#endif  
*/ 

#ifndef DA14_DA16 //DA6, DA8
#ifdef XCH_RFIC
  if(rfic == 0)  ptr = conf_RFIC_L1_GPS_1590;
  else           ptr = conf_RFIC_L5_1190;
#else
  if(rfic) ptr = conf_RFIC_L1_GPS_1590;
  else ptr = conf_RFIC_L5_1190;
#endif  
#else   //DA14, DA16 
  if(rfic) ptr = conf_RFIC_E5b_B2;
  else ptr = conf_RFIC_L2_GLO;   
#endif

  if (rd_RFIC(rfic, 0) != 0x21) return 1;
  if (rd_RFIC(rfic, 1) != 0x4A) return 1;
  for (int i = 0; i < sizeof(conf_RFIC_L1_GPS_1590); i+=2)
    wr_RFIC(rfic, ptr[i], ptr[i + 1]);

  for (int i = 0; i < sizeof(conf_RFIC_L1_GPS_1590); i+=2) {
    bt = ptr[i + 1];
    if (ptr[i] == 43) R43 = bt;
    if (rd_RFIC(rfic, ptr[i]) != bt)  
      return 1;
  } 
  wr_RFIC(rfic, 43, R43 | 0x1); // PLL_A start 
  for (int i = 0; i < 10; i++) {
    if ((rd_RFIC(rfic,44) & 0x1) == 0x1) {  // PLL_A lock ?
      wr_RFIC(rfic, 4, 0x1); // LPF auto-calibration start
      for (int j = 0; j < 10000; j++) 
        if ((rd_RFIC(rfic,4) & 0x3) == 0x2) // LPF auto-calibration system status
          return 0;
      return 1;
    }  
  }  
  return 1;
}  
//----------------------------------------------------------------- SPI PLL ----
/*uint8_t read_IIC(uint8_t devadr, uint8_t regadr)
{
volatile int* IIC = (volatile int *)IIC1_BASE;
//int i = 0;

  IIC[TWB_CNFG] = 0x82;		// mov	TWB_SRC,#(0x40 + 7)		;mode = IIC, presc = 7(400khz) 
  IIC[TWB_DATA] = devadr;         //  Start Addr
  IIC[TWB_CMND] = 0x05;         //        ;START & WRITE
  while ((IIC[TWB_STAT]&0x80) == 0);            
  if ((IIC[TWB_STAT]&0x04) == 0) { 	//  no ack	
    IIC[TWB_DATA] = regadr;
    IIC[TWB_CMND] = 0x04;               // WRITE
    while ((IIC[TWB_STAT]&0x80) == 0);
    IIC[TWB_DATA] = devadr | 1;         // Start read
    IIC[TWB_CMND] = 0x05;         // START & WRITE
    while ((IIC[TWB_STAT]&0x80) == 0);
    IIC[TWB_CMND] = 0x12;        // READ & NACK & STOP
    while ((IIC[TWB_STAT]&0x80) == 0);
    return IIC[TWB_DATA];*/
/*    do {		
      IIC[TWB_CMND] = 0x08;         // READ & ACK
      while ((IIC[TWB_STAT]&0x80) == 0);
      data[i++] = IIC[TWB_DATA];
   } while (i < len);   
   IIC[TWB_CMND] = 0x12;        // READ & NACK & STOP
   while ((IIC[TWB_STAT]&0x80) == 0); */
/*  }
  return 0;  // ??????????
}
//------------------------------------------------------------------------------
void read_IIC_block(uint8_t devadr, uint8_t regadr, uint8_t len, uint8_t* buff)
{
volatile int* IIC = (volatile int *)IIC1_BASE;
int i = 0;

  IIC[TWB_CNFG] = 0x82;		// mov	TWB_SRC,#(0x40 + 7)		;mode = IIC, presc = 7(400khz) 
  IIC[TWB_DATA] = devadr;         //  Start Addr
  IIC[TWB_CMND] = 0x05;         //        ;START & WRITE
  while ((IIC[TWB_STAT]&0x80) == 0);            
  if ((IIC[TWB_STAT]&0x04) == 0) { 	//  no ack	
    IIC[TWB_DATA] = regadr;
    IIC[TWB_CMND] = 0x04;               // WRITE
    while ((IIC[TWB_STAT]&0x80) == 0);
    IIC[TWB_DATA] = devadr | 1;         // Start read
    IIC[TWB_CMND] = 0x05;         // START & WRITE
    while ((IIC[TWB_STAT]&0x80) == 0);
    do {		
      IIC[TWB_CMND] = 0x08;         // READ & ACK
      while ((IIC[TWB_STAT]&0x80) == 0);
      buff[i++] = IIC[TWB_DATA];
    } while (i < len);   
    IIC[TWB_CMND] = 0x12;        // READ & NACK & STOP
    while ((IIC[TWB_STAT]&0x80) == 0); 
  }
}
//------------------------------------------------------------------------------
void write_IIC(uint8_t devadr, uint8_t regadr, uint8_t data)
{
volatile int* IIC = (volatile int *)IIC1_BASE;
//int i = 0;

  IIC[TWB_CNFG] = 0x82;		// ;mode = IIC, presc = 7(400khz) 
  IIC[TWB_DATA] = devadr;         //  Start Addr
  IIC[TWB_CMND] = 0x05;         //        ;START & WRITE
  while ((IIC[TWB_STAT]&0x80) == 0);            
  if ((IIC[TWB_STAT]&0x04) == 0) { 	//  no ack	
    IIC[TWB_DATA] = regadr;
    IIC[TWB_CMND] = 0x04;               // WRITE
    while ((IIC[TWB_STAT]&0x80) == 0);
    IIC[TWB_DATA] = data;
    IIC[TWB_CMND] = 0x06;               // WRITE & STOP
  }
  while ((IIC[TWB_STAT]&0x80) == 0);
}

void write_IIC_block(uint8_t devadr, uint8_t regadr, uint8_t len, uint8_t* buff)
{
volatile int* IIC = (volatile int *)IIC1_BASE;
int i = 0;

  IIC[TWB_CNFG] = 0x82;		// ;mode = IIC, presc = 7(400khz) 
  IIC[TWB_DATA] = devadr;         //  Start Addr
  IIC[TWB_CMND] = 0x05;         //        ;START & WRITE
  while ((IIC[TWB_STAT]&0x80) == 0);            
  if ((IIC[TWB_STAT]&0x04) == 0) { 	//  no ack	
    IIC[TWB_DATA] = regadr;
    IIC[TWB_CMND] = 0x04;               // WRITE
    while ((IIC[TWB_STAT]&0x80) == 0);
    do {
      IIC[TWB_DATA] = buff[i++];  
      IIC[TWB_CMND] = 0x04;               // WRITE
      while ((IIC[TWB_STAT]&0x80) == 0);
    } while (i < len-1);
    IIC[TWB_DATA] = buff[i++]; 
    IIC[TWB_CMND] = 0x06;               // WRITE & STOP
    while ((IIC[TWB_STAT]&0x80) == 0);
  }
}
*/
//-------------------------------------------------------------------- CLOCK_PLL --
/*int init_CLOCK_PLL(void)
{
//uint8_t bt = 0;
uint8_t buff[128];  

//  while ((read_IIC(0xD4, 0xd) != 0xb6));
  if (read_IIC(0xD4, 0xd) != 0xb6) return 1;
  if (read_IIC(0xD4, 0xe) != 0xb4) return 1;

  for (int i = 0; i < sizeof(conf_PLL); i+=2) 
     buff[i/2] = conf_PLL[i + 1];
  write_IIC_block(0xd4, 0x10, sizeof(conf_PLL)/2, buff);
  
//  for (int i = 0; i < 640; i++) bt++;
  
  read_IIC_block(0xD4, 0, 0x6a, buff);
  for (int i = 0; i < sizeof(conf_PLL); i+=2) //{
//    if ((conf_PLL[i] == 49) || (conf_PLL[i] == 75)) bt &= 0xfe;
    if (buff[i/2 + 0x10] != conf_PLL[i + 1]) 
      return 1;
//  }  
   // if (ReadFromPLL(2)&0x00002000) return 0; // Lock 
  return 0;
}  */
//----------------------------------------------------------------- SPI ADC ----

uint8_t ReadFromADC(uint8_t chan, uint8_t adr)
{
volatile int* SPI = (volatile int *)SPI2_BASE;  
uint8_t bt;
#ifdef TITAN_V4 
  chan =~chan;
#endif
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0008;     // ������� 
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x4008;     // �������, hold CS
  SPI[SSDR] = (adr&0x7F)|0x80; SPI[SSDR] = 0x0; 
  while (SPI[SSSR]&0x8); // BSY
  bt = SPI[SSDR]; 
  bt = SPI[SSDR];
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0008;     // �������
  return bt;
}

void WriteToADC(uint8_t chan, uint8_t adr, uint8_t dat) 
{
volatile int* SPI = (volatile int *)SPI2_BASE;  
#ifdef TITAN_V4 
  chan =~chan;
#endif
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x090f;     // 16- ��� ������, CPU_CLK / 20
//  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0408;     // ������ ��������, �������
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x4408;     // ������ ��������, �������, hold CS
  SPI[SSDR] = adr&0x7F; SPI[SSDR] = dat; 
  while ((SPI[SSSR]&0x1) == 0); // TFE  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = ((chan&0x1)<<6) + 0x0408;     // ������ ��������, �������, clear CS
}

int init_ADC(int adc) {
  
  WriteToADC(adc, 0, 0x80);          // Reset
  WriteToADC(adc, 1, 0x20);          // TWOSCOMP, Normal Operation
//  WriteToADC(adc, 1, 0x00);          // Normal Operation
//  WriteToADC(adc, 2, 0xE6);          // 1.75mA LVDS Output Driver Current, Internal Termination Off, Digital Outputs are Enabled, 110 = 1-Lane, 12-Bit Serialization 
  WriteToADC(adc, 2, 0xE2);          // 1.75mA LVDS Output Driver Current, Internal Termination Off, Digital Outputs are Enabled, 010 = 2-Lane, 12-Bit Serialization 
//  WriteToADC(adc, 2, 0x02);
  WriteToADC(adc, 3, 0xAA);          //  OUTTEST,  0xAA5 
  WriteToADC(adc, 4, 0x94);
  if (ReadFromADC(adc, 3) != 0xAA) return 1;
  if (ReadFromADC(adc, 2) != 0xE2) return 1;
  return 0;
}

int trim_ADC(int num) {
uint32_t temp;
int ret = 1;
volatile int* AK = (volatile int *)AK_BASE_ADR;  

  temp =  AK[COMMON]; // = 0x01900; 
  if (num) AK[COMMON] = temp | 0x100; // band0 < ADC1
  else AK[COMMON] = temp & ~(0x100); // band0 < ADC0 
  
  WriteToADC(num, 3, 0xAA);          //  OUTTEST,  0xAA5 
  WriteToADC(num, 4, 0x94);
  if (!CheckRegistratorConst(0xAA5, num)) ret = 0;
  if (ret) {
    WriteToADC(num, 3, 0x95);          //  OUTTEST,  0x55A 
    WriteToADC(num, 4, 0x68);
    if (!CheckRegistratorConst(0x55A, num)) ret = 0;
  } 
  WriteToADC(num, 3, 0x0);          //  OUTTEST off
  AK[COMMON] = temp;
  return 1;
}

//----------------------------------------------------------------- SPI SYN ----
/*void WriteToSYN(uint32_t word) 
{
volatile int* SPI = (volatile int *)SPI2_BASE;  

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0917;  // 24- ��� ������, CPU_CLK / 20
  SPI[SSCR1] = ((0)<<6) + 0x04408;     // ������ ��������, �������, hold CS0 
  SPI[SSDR] = word>>16; SPI[SSDR] = word>>8; SPI[SSDR] = word;  
  while ((SPI[SSSR]&0x1) == 0); // TFE  
  SPI[SSCR1] = ((0)<<6) + 0x00408;     // ������ ��������, �������, CS0 
}*/

// F = (8*B_cnt + A_cnt)*1.242 MHz
// a_b_cnt = 0x9D34;  A=13, B=157 - F = 1576.098 MHz
/*int CalibrTonOn(uint32_t a_b_cnt, uint32_t pow)  // b20..b8 = B_cnt, b6..b2 = A_cnt, pow = 0..3 = -14, -11, -8, -5 dB
{
volatile int* GPIO = (volatile int *)GPIO_BASE; 
volatile int* SPI = (volatile int *)SPI2_BASE;
uint32_t syn_R0, syn_R2, syn_R1 = 0x300050;
  
  SPI[SSCR1] = ((0)<<6) + 0x00408;     // ������ ��������, �������, CS0
  GPIO[GPIO_OUT_1] |= 0x00000020;  // set GEN_POWER_CAL
//  syn_R0 = 0x6c128 | ((pow&3) << 12);  // pow = 0..3 = -14, -11, -8, -5 dB 
  syn_R0 = 0x24128 | ((pow&3) << 12);  // pow = 0..3 = -14, -11, -8, -5 dB
  syn_R2 = a_b_cnt; // b20..b8 = B_cnt, b6..b2 = A_cnt
 for (int try = 0; try < 10; try++) {
//  while(1) {
    WriteToSYN(syn_R2 | 0x2);
    WriteToSYN(syn_R0 | 0x0);
    for (int i = 0; i<100000; i++) (void) GPIO[GPIO_IN_1];  //  ~20 ms pause
    WriteToSYN(syn_R1 | 0x1);
    for (int i = 0; i<10; i++) (void) GPIO[GPIO_IN_1];
    if (GPIO[GPIO_IN_1] & 0x10) // MUXOUT_CAL
      return 1; 
//    for (int i = 0; i<1000000; i++) (void) GPIO[GPIO_IN_1];  //  ~200 ms pause
  }
  return 0;  
}*/

/*void CalibrTonOff(void)
{
volatile int* GPIO = (volatile int *)GPIO_BASE;
volatile int* SPI = (volatile int *)SPI2_BASE;

  GPIO[GPIO_OUT_1] &= (~0x00000020);  // clear GEN_POWER_CAL
  WriteToSYN(0x9D34 | 0x1);
  WriteToSYN(0x16c128 | 0x0);   // ASYNCHRONOUS POWER-DOWN
  for (int i = 0; i<10000; i++) (void) GPIO[GPIO_IN_1];  //  ~10 ms pause
  WriteToSYN(0x300050 | 0x2);
  SPI[SSCR1] = ((0)<<6) + 0x04408;     // ������ ��������, �������, hold CS0 -> 0 
}*/
//------------------------------------------------------------------------------
uint8_t ReadFromMIX(uint8_t adr)
{
volatile int* SPI = (volatile int *)SPI1_BASE; 
uint8_t res;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x008 | ((adr&0x80)>>1);     // �������, CS0/1  
  SPI[SSDR] = ((adr&0x3F)<< 1)|0x1; SPI[SSDR] = 0x0; 
  while (SPI[SSSR]&0x8); // BSY
  res = SPI[SSDR]; 
  res = SPI[SSDR];
  return res & 0xff;
}

void WriteToMIX(uint8_t adr, uint8_t dat) 
{
  volatile int* SPI = (volatile int *)SPI1_BASE;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR1] = 0x0408 | ((adr&0x80)>>1);     // ������ ��������, �������, CS0/1
  SPI[SSDR] = ((adr&0x3F)<< 1); SPI[SSDR] = dat; 
}  


uint32_t ReadFromMPLL_OM(uint8_t adr)
{
  volatile int* SPI = (volatile int *)SPI1_BASE; 
  uint32_t res;
  uint32_t data = ((adr&0x1f) << 8);
  uint8_t* data8 = (uint8_t*)&data;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x00488;     // ������ ��������, �������, CS2 
  for (int i = 0; i < 4; i++) {
    while ((SPI[SSSR]&0x1) == 0); // TFE
    SPI[SSDR] = data8[3-i];  
  }
  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x0088;     // �������, CS2 
  SPI[SSDR] = data8[3]; SPI[SSDR] = data8[2]; SPI[SSDR] = data8[1]; SPI[SSDR] = data8[0];
  while (SPI[SSSR]&0x8); // BSY
  res = SPI[SSDR]; 
  res = (res << 8) | (SPI[SSDR] & 0xff);
  res = (res << 8) | (SPI[SSDR] & 0xff);
  res = (res << 8) | (SPI[SSDR] & 0xff);
  return res;
}

void WriteToMPLL_OM(uint8_t adr, uint32_t dat) 
{
  volatile int* SPI = (volatile int *)SPI1_BASE;
  uint32_t data = ((dat&0xffffff) << 8) | ((adr&0x1F) << 3);
  uint8_t* data8 = (uint8_t*)&data;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR1] = 0x00488;     // ������ ��������, �������, CS2 
  for (int i = 0; i < 4; i++) {
    while ((SPI[SSSR]&0x1) == 0); // TFE
    SPI[SSDR] = data8[3-i];  
  }
}  

int initMPLL_open_mode(void)
{
volatile int* SPI = (volatile int *)SPI1_BASE;
volatile int* GPIO = (volatile int *)GPIO_BASE;
uint32_t din, wc;

while (SPI[SSSR]&0x8); // BSY  
SPI[SSCR1] = 0x0488;     // ������ ��������, �������, CS2, CS aktiv low
//prog_status |= DEBUG_FLAG;

//while(1)
din = ReadFromMPLL_OM(0); // chip_ID A7975
inf_buf[0] = din >> 16; inf_buf[1] = din >> 8; inf_buf[2] = din;
din = din >> 7;
if (din != 0xA7975)
   return 1;
WriteToMPLL_OM(0, 0x000020);  // soft reset

WriteToMPLL_OM(2, 0x000001);  // Reference Divider = 1
//WriteToMPLL_OM(7, 0x00014d);  // PFD default
WriteToMPLL_OM(9, 0x547FFF);  // Charge Pump CP = 2.54mA, CP DN Offset = 420uA, Fractional Mode
WriteToMPLL_OM(0xB, 0x0FC0E1);  // PD UP and DN enabled, CSP On, Force CP disabled

WriteToMPLL_OM(5, 0x0f88);  // all subsystem enable
WriteToMPLL_OM(5, 0xE090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 11 = Max Gain, Divider Output Stage Gain Control = 1
//WriteToMPLL_OM(5, 0x8090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 00 = Min Gain, Divider Output Stage Gain Control = 1
//WriteToMPLL_OM(5, 0xA090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 01 = 0 db, Divider Output Stage Gain Control = 1
WriteToMPLL_OM(5, 0x2A98);  // RF Buffer = SE, Manual RFO Mode = 1, RF Buffer Bias = 10, Spare Don�t Cares = 0010
WriteToMPLL_OM(5, 0x60A0);  // default
WriteToMPLL_OM(5, 0x1628);  // default
WriteToMPLL_OM(5, 0);  // close reg_5 write

WriteToMPLL_OM(0xA, 0x2046);  // AutoCal enabled, VSPI trigger enabled
WriteToMPLL_OM(0x6, 0x200B4A);  // Fractional B Mode
#if (SW_VERSION == 205) // Vco1580 
WriteToMPLL_OM(3, 158);
#else
WriteToMPLL_OM(3, 159);
#endif
WriteToMPLL_OM(4, 0);

din = ReadFromMPLL_OM(3);
inf_buf[3] = din >> 16; inf_buf[4] = din >> 8; inf_buf[5] = din;
din = din >> 7;
if (din != 159) 
  return 1;
wc = 1000;
do {
  din = ReadFromMPLL_OM(0x12);
  wc--; 
} while (((din & 0x100) == 0) && (wc > 0) );   // wait Lock
inf_buf[6] = din >> 16; inf_buf[7] = din >> 8; inf_buf[8] = din;

if ((din & 0x100) == 0)  
  return 1;
//prog_status |= 0x4000;
WriteToMPLL_OM(0xf, 0x300);  //disable SDO pin
while (SPI[SSSR]&0x8); // BSY
SPI[SSCR1] = 0x0;     // off  
GPIO[GPIO_OUT_1] |= 0x00000020;  // set scs2 pin 
return 0;
}

uint32_t ReadFromMPLL(uint8_t adr)
{
volatile int* SPI = (volatile int *)SPI1_BASE; 
uint32_t res;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x2088;     // �������, CS2, CS aktiv high  
  SPI[SSDR] = ((adr&0x3F)<< 1)|0x80; SPI[SSDR] = 0x0; SPI[SSDR] = 0x0; SPI[SSDR] = 0x0; 
  while (SPI[SSSR]&0x8); // BSY
  res = SPI[SSDR]; 
  res = (res << 8) | (SPI[SSDR] & 0xff);
  res = (res << 8) | (SPI[SSDR] & 0xff);
  res = (res << 8) | (SPI[SSDR] & 0xff);
  return res & 0xffffff;
}

void WriteToMPLL(uint8_t adr, uint32_t dat) 
{
  volatile int* SPI = (volatile int *)SPI1_BASE;
  uint32_t data = ((dat&0xffffff) << 1) | ((adr&0x3F) << 25);
  uint8_t* data8 = (uint8_t*)&data;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR1] = 0x02488;     // ������ ��������, �������, CS2, CS aktiv high 
  for (int i = 0; i < 4; i++) {
    while ((SPI[SSSR]&0x1) == 0); // TFE
    SPI[SSDR] = data8[3-i];  
  }
}  

int init_MPLL(void) 
{
volatile int* SPI = (volatile int *)SPI1_BASE;
volatile int* GPIO = (volatile int *)GPIO_BASE;
uint32_t din, wc;
/*ADDRESS DEFAULTVALUE SETTING REGISTER FUNCTION
0x00    0x3E    2.56GHz         LO Frequency Tuning
0x01    0x84    DG = �4         Gain
0x02    0x80    0mV     Offset I-Channel
0x03    0x80    0mV     Offset Q-Channel
0x04    0x80    0dB     I/Q Gain Ratio
0x05    0x10    0�      I/Q Phase Balance
0x06    0x50    OFF     LO Port Matching Override
0x07    0x06    OFF     Temperature Correction Override
0x08    0x00    NORMAL  Operating Mode */
//uint8_t dmix[9] = {0x4d, 0x80, 0x80, 0x80, 0x80, 32, 0x50, 0x06, 0x00};  //1506-1590 MHz
//uint8_t dmix[9] = {0x4d, 0x80, 0x80, 0x80, 0x80, 16, 0x50, 0x06, 0x00};  //1506-1590 MHz

while (SPI[SSSR]&0x8); // BSY  
SPI[SSCR0] = 0x091F; // 32-bits, CPU_CLK / 20
SPI[SSCR1] = 0x2488;     // ������ ��������, �������, CS2, CS aktiv high
GPIO[GPIO_ALT_1]  |= 0x27;        // spi1 (cs2)
GPIO[GPIO_DIR_1] &= (~0x24);   // set "in" direction for scs2_1 & sclk_1 pins  
 
//while(1)
din = ReadFromMPLL(0); // chip_ID A7975
inf_buf[0] = din >> 16; inf_buf[1] = din >> 8; inf_buf[2] = din;
if (din != 0xA7975) {
  if (initMPLL_open_mode()) return 1;
} else {  
  WriteToMPLL(0, 0x000020);  // soft reset

  WriteToMPLL(2, 0x000001);  // Reference Divider = 1
  //WriteToMPLL(7, 0x00014d);  // PFD default
  WriteToMPLL(9, 0x547FFF);  // Charge Pump CP = 2.54mA, CP DN Offset = 420uA, Fractional Mode
  WriteToMPLL(0xB, 0x0FC0E1);  // PD UP and DN enabled, CSP On, Force CP disabled

  WriteToMPLL(5, 0x0f88);  // all subsystem enable
  WriteToMPLL(5, 0xE090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 11 = Max Gain, Divider Output Stage Gain Control = 1
  //WriteToMPLL(5, 0x8090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 00 = Min Gain, Divider Output Stage Gain Control = 1
  //WriteToMPLL(5, 0xA090);  // RF Divide Ratio = 1, RF Output Buffer Gain Control = 01 = 0 db, Divider Output Stage Gain Control = 1
  WriteToMPLL(5, 0x2A98);  // RF Buffer = SE, Manual RFO Mode = 1, RF Buffer Bias = 10, Spare Don�t Cares = 0010
  WriteToMPLL(5, 0x60A0);  // default
  WriteToMPLL(5, 0x1628);  // default
  WriteToMPLL(5, 0);  // close reg_5 write

  WriteToMPLL(0xA, 0x2046);  // AutoCal enabled, VSPI trigger enabled
  WriteToMPLL(0x6, 0x200B4A);  // Fractional B Mode
  #if (SW_VERSION == 205) // Vco1580 
  WriteToMPLL(3, 158);
  #else
  WriteToMPLL(3, 159);
  #endif
  WriteToMPLL(4, 0);

  din = ReadFromMPLL(3);
  inf_buf[3] = din >> 16; inf_buf[4] = din >> 8; inf_buf[5] = din;
  if (din != 159) 
    return 1;
  wc = 1000;
  do {
    din = ReadFromMPLL(0x12);
    wc--; 
  } while (((din & 0x2) == 0) && (wc > 0) );   // wait Lock
  inf_buf[6] = din >> 16; inf_buf[7] = din >> 8; inf_buf[8] = din;
  if ((din & 0x2) == 0)  
    return 1;
}

GPIO[GPIO_DIR_1] |= 0x20;   // set "out" direction for scs2_1 pin
GPIO[GPIO_ALT_1] &= (~0x20);   // off spi1 (cs2)

// --------------------------------------------------------- Mixer ---
SPI[SSCR0] = 0x090F; // 16-bits, CPU_CLK / 20
GPIO[GPIO_ALT_1] |= 0x18;        // spi1 (cs0, cs1)
//WriteToMIX(0x00+8, 0x8);  // sreset
WriteToMIX(0x80+8, 0x8);
//din = ReadFromMIX(0x81);
//inf_buf[6] = din;
din = ReadFromMIX(0x80);
//inf_buf[3] = din;
if (din != 0x3e) 
  return 1;
//while (1) 
//din = ReadFromMIX(0x00);

for (int i = 0; i < 9; i++) {
//  WriteToMIX(0x00+i, dmix[i]);  
  WriteToMIX(0x80+i, dmix[i]);  
}
//WriteToMIX(0x80+1, 0x93);  // -19 dB

for (int i = 0; i < 9; i++) {
  if (ReadFromMIX(0x80+i) != dmix[i]) 
    return 1;
}
return 0;
}  
//--------------------------------------------------------------------
//--------------------------------------------------------------------
uint32_t FlashReadSectorProtection(uint32_t ulDstAddr)
{
volatile int* SPI = (volatile int *)FLASH_SPI;

int i;
uint32_t ucStatus;

  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x4008;          // HOLDCS 
  SPI[SSDR] = 0x3c;     // Read Sector Protection Register
  // Transfer the address 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
  while (SPI[SSSR]&0x8); // BSY
//  while (SPI[SSSR]&0x4) (void)SPI[SSDR];  // read dummy
  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR];
  SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR];
  SPI[SSCR1] = 0x0408;          // clear HOLDCS
  ucStatus = dac_data[i-1];
  return ucStatus;
}

void read_flash_id(void)
{
volatile int* SPI = (volatile int *)FLASH_SPI;
//uint8_t answ[8];
int i;

  SPI[SSCR0] = 0x021f;         // 32-bit
  SPI[SSCR1] = 0x0008;
  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x9F;  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; //SPI[SSDR] = 0; 
  while (SPI[SSSR]&0x8); // BSY
  i = 0;
  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR]; 
 // while (SPI[SSSR]&0x8); // BSY  
 // SPI[SSDR] = 0x03;  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; //SPI[SSDR] = 0; SPI[SSDR] = 0;
 // while (SPI[SSSR]&0x8); // BSY
 // i = 0;
 // while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR]; 
  return;
}
//--------------------------------------------------------------------
//*****************************************************************************
//      param None
//      This function is to Read SST25VFxx Status Register:the bit field follows:
//      BIT  7    6   5   4   3,2   1     0 
//          SPRL SPM EPE WPP  SWP  WEL RDY/BSY
//      return the value of status
//*****************************************************************************
uint32_t FlashStatusRegRead(void)
{   
  uint32_t ucStatus;
  volatile int* SPI = (volatile int *)FLASH_SPI;
//  int i;
  
  while (SPI[SSSR]&0x8); // BSY  
//  SPI[SSCR0] = 0x021f;         // 32-bit
  SPI[SSCR0] = 0x020f;         // 16-bit
  SPI[SSCR1] = 0x0008;
  SPI[SSDR] = 0x05;  SPI[SSDR] = 0;  
//      SPI[SSDR] = 0;  SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
//  i = 0;
//  while (SPI[SSSR]&0x4) dac_data[i++] = SPI[SSDR]; 
  ucStatus = SPI[SSDR] << 8;
  ucStatus += SPI[SSDR]; 
//  ucStatus = ucStatus << 8; ucStatus += SPI[SSDR];
//  ucStatus = ucStatus << 8; ucStatus += SPI[SSDR];
  return ucStatus;    
}

//*****************************************************************************
//      param None
//      This function is to Wait for SST25VFxx is not busy
//      return None
//*****************************************************************************
void FlashWaitNotBusy(void)
{    
    //Wait the bit of busy is clear
    while((FlashStatusRegRead() & 0x01) == 0x01);
}

//*****************************************************************************
//      param None
//      This function is to enable the function 0f writing
//      return none
//*****************************************************************************
void FlashWriteEnable(void)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;
  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x0408;          // ������ ��������
  SPI[SSDR] = 0x06;   
  while (SPI[SSSR]&0x8); // BSY
}

//*****************************************************************************
//      param None
//      This function is to disable the function 0f writing
//      return none
//*****************************************************************************
void FlashWriteDisable(void)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;
  
  while (SPI[SSSR]&0x8); // BSY 
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x0408;          // ������ ��������
  SPI[SSDR] = 0x04;   
  while (SPI[SSSR]&0x8); // BSY
}

//*****************************************************************************
//      Erase all chip
//      param None
//      return none
//*****************************************************************************
void FlashChipErase(void)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Chip Erase
//  SPI[SSCR0] = 0x0207;   // 8-bit
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x60;   
//  while (SPI[SSSR]&0x8); // BSY  
  FlashWaitNotBusy();
}

//*****************************************************************************
//      Block Erase (4 Kbytes)
//      param ulDstAddr specifies the sector address which will be erased.
//      return none
//*****************************************************************************
void FlashSectorErase(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (4 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x20;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}

//*****************************************************************************
//      Erase a 32k Block
//      param ulDstAddr specifies the 32k Block address which will be erased.
//      return none
//*****************************************************************************
void FlashBlock32Erase(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (32 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x52;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}

//*****************************************************************************
//      brief Erase a 64k Block
//      param ulDstAddr specifies the 64k Block address which will be erased.
//      return none
//*****************************************************************************
void FlashBlock64Erase(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (64 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0xD8;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}
void FlashUnprotectSector(uint32_t ulDstAddr)
{
  volatile int* SPI = (volatile int *)FLASH_SPI;

  //Step 1 Set write enable
  FlashWriteEnable();
  //Step 2 Transfer the command Block Erase (64 Kbytes)
  SPI[SSCR0] = 0x021f;   // 32-bit, 
//  SPI[SSCR1] = 0x0408;          // ������ ��������
//  while (SPI[SSSR]&0x8); // BSY  
  SPI[SSDR] = 0x39;
  //Step 3 Transfer the address which will be erased
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 16); 
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 8);
  SPI[SSDR] = (unsigned char)(ulDstAddr >> 0);
//  while (SPI[SSSR]&0x8); // BSY  
  //Step 4  wait for the operation is over
  FlashWaitNotBusy();
}
//--------------------------------------------------------------------
void write_flash_image(void)
{
volatile int* SPI = (volatile int *)FLASH_SPI;
volatile int* AK = (volatile int *)AK_BASE_ADR;
volatile unsigned char* PTR = (unsigned char *)(0);
uint32_t adr = 0;
uint32_t image_size = 16384;
uint32_t sum = (unsigned char)(image_size >> 8) + (unsigned char)(image_size >> 0);
uint32_t rsum = 0, rw = 0;;
uint8_t bt;

  prog_status |= WR_FLASH_ERR;
  if (prog_cntrl & UPDATE_FLASH) PTR = (unsigned char *)(0x4000);
  prog_cntrl &= ~(WR_FLASH_IMAGE | UPDATE_FLASH);
  if ((AK[IMMIT_FRQ] != 0xA5A55A5A)) return;
  FlashUnprotectSector(0);
  FlashBlock32Erase(0);
  
  FlashWriteEnable();
  SPI[SSCR1] = 0x4408;          // ������ ��������, HOLDCS  
  SPI[SSDR] = 0x02;     // Write command
  // Transfer the address 
  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
  // Transfer count
  while ((SPI[SSSR]&0x10) == 0); // TFS
  SPI[SSDR] = (unsigned char)(image_size >> 0);
  SPI[SSDR] = (unsigned char)(image_size >> 8);
  SPI[SSDR] = 0; SPI[SSDR] = 0;
  // Transfer data 
  for (int i = 4; i < 256; i++) {
    while ((SPI[SSSR]&0x10) == 0); // TFS
    bt = PTR[adr++];
    sum += bt;
    SPI[SSDR] = bt;
  }  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR1] = 0x0408;          // clear HOLDCS
  FlashWaitNotBusy();
  
  for (int k = 1; k < image_size/256; k++) {
    FlashWriteEnable();
    SPI[SSCR1] = 0x4408;          // ������ ��������, HOLDCS  
    SPI[SSDR] = 0x02;     // Write command
    // Transfer the address 
    SPI[SSDR] = 0; SPI[SSDR] = k; SPI[SSDR] = 0;
    // Transfer data 
    for (int i = 0; i < 256; i++) {
      while ((SPI[SSSR]&0x10) == 0); // TFS
      bt = PTR[adr++];
      sum += bt;
      SPI[SSDR] = bt;
    }  
    while (SPI[SSSR]&0x8); // BSY
    SPI[SSCR1] = 0x0408;          // clear HOLDCS
    FlashWaitNotBusy();  
  } 
  
// ����������� ������
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x4008;          // HOLDCS 
  SPI[SSDR] = 0x03;     // read command
  // Transfer the address 
  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
  while (SPI[SSSR]&0x4) (void)SPI[SSDR];  // read dummy
  // read data 
  for (int i = 0; i < image_size/4; i++) {
      SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
      while (SPI[SSSR]&0x8); // BSY
      while (SPI[SSSR]&0x4) { 
        bt = SPI[SSDR];
        rsum += bt; 
        rw++; 
      }
  }  
  SPI[SSCR1] = 0x0408;          // clear HOLDCS
  if (sum == rsum) prog_status &= ~WR_FLASH_ERR;
}

uint32_t read_flash_image(uint32_t image_size)
{
volatile int* SPI = (volatile int *)FLASH_SPI;
uint32_t rsum = 0;
uint8_t bt;
 
  
  while (SPI[SSSR]&0x8); // BSY
  SPI[SSCR0] = 0x0207;         // 8-bit
  SPI[SSCR1] = 0x4008;          // HOLDCS 
  SPI[SSDR] = 0x03;     // read command
  // Transfer the address 
  SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
  while (SPI[SSSR]&0x8); // BSY
  while (SPI[SSSR]&0x4) (void)SPI[SSDR];  // read dummy
  // read data 
  for (int i = 0; i < image_size/4; i++) {
      SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0; SPI[SSDR] = 0;
      while (SPI[SSSR]&0x8); // BSY
      while (SPI[SSSR]&0x4) { 
        bt = SPI[SSDR];
        rsum += bt; 
      }
  }  
  SPI[SSCR1] = 0x0408;          // clear HOLDCS  
  return rsum;
}
//--------------------------------------------------------------------